//
//  ViewController.swift
//  SimpleTableView
//
//  Created by Andras Somlai on 2018. 05. 10..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        //Alsó csíkok eltűntetésére hack
        tableView.tableFooterView = UIView()
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 1) {
            return 3
        }
        else {
            return 10
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        debugPrint("CELL FOR ROW AT")
        var cell = tableView.dequeueReusableCell(withIdentifier: "cellId")
        if (cell == nil)  {
            cell = UITableViewCell(style: .default, reuseIdentifier: "cellId")
        }
        cell!.backgroundColor = UIColor.lightGray
        cell!.textLabel?.text = "sor: \(indexPath.row)--szekció: \(indexPath.section)"
        return cell!
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "szekció\(section)"
    }
 
    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return "Section Footer Section Footer Section Footer Section Footer Section Footer Section Footer Section Footer Section Footer Section Footer Section Footer"
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        debugPrint("\(indexPath)")
        tableView.cellForRow(at: indexPath)!.accessoryType = .checkmark
    }
}

