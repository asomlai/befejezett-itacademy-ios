//
//  Fruit.swift
//  15Feladatsor_6-10Megoldasok
//
//  Created by Andras Somlai on 2018. 05. 22..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

enum FoodType {
    case fruit
    case vegetable
}

class Food {
    var name : String
    var description : String
    var image : UIImage
    var quantity : Int
    var type : FoodType
    
    init(name: String, description: String, image: UIImage, quantity: Int, type: FoodType) {
        self.name = name
        self.description = description
        self.image = image
        self.quantity = quantity
        self.type = type
    }
}
