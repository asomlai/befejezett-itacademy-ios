//
//  FruitCell.swift
//  15Feladatsor_6-10Megoldasok
//
//  Created by Andras Somlai on 2018. 05. 22..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

protocol FruitCellDelegate {
    func fruitQuantityUpdated(fruit: Food)
}

class FruitCell: UITableViewCell {

    var delegate : FruitCellDelegate?
    
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var fruitImage: UIImageView!
    @IBOutlet weak var quantityStepper: UIStepper!
    
    var fruit : Food? {
        didSet {
            updateUIData()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func quantityChanged(_ sender: UIStepper) {
        if(fruit == nil) {
            return
        }
        
        quantityLabel.text = "\(Int(sender.value)) db"
        fruit?.quantity = Int(sender.value)
        delegate?.fruitQuantityUpdated(fruit: self.fruit!)
    }
    
    func updateUIData() {
        if(fruit == nil) {
            return
        }
        
        quantityLabel.text = "\(fruit!.quantity) db"
        quantityStepper.value = Double(fruit!.quantity)
        nameLabel.text = fruit!.name
        descriptionLabel.text = fruit!.description
        fruitImage.image = fruit!.image
    }
}
