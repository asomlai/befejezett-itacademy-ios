//
//  ViewController.swift
//  15Feladatsor_6-10Megoldasok
//
//  Created by Andras Somlai on 2018. 05. 22..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, FruitCellDelegate {
    @IBOutlet weak var sumViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dailyFruitQuantityLabel: UILabel!
    
    var fruitData : [Food] = []
    var vegetableData : [Food] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        dailyFruitQuantityLabel.text = "0db"
        sumViewHeightConstraint.constant = 0
        tableView.delegate = self
        tableView.dataSource = self
        
        let imageView = UIImageView(image: UIImage(named: "footer"))
        imageView.contentMode = .scaleAspectFit
        tableView.tableFooterView = imageView
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fruitQuantityUpdated(fruit: Food) {
        // Megoldás 1
        /*
        var sum = 0
        for fruit in fruitData {
            sum += fruit.quantity
        }
        
        for vegetable in vegetableData {
            sum += vegetable.quantity
        }
        dailyFruitQuantityLabel.text = "\(sum)db"
        */
        
        //Megoldás 2
        let foodSum = fruitData.map{$0.quantity}.reduce(0, +) + vegetableData.map{$0.quantity}.reduce(0, +)
        dailyFruitQuantityLabel.text = "\(foodSum)db"
        
        //Megoldás 1
        /*
        if(foodSum == 0) {
            sumViewHeightConstraint.constant = 0
        }
        else {
            sumViewHeightConstraint.constant = 50
        }
        */
        
        //Megoldás 2
        sumViewHeightConstraint.constant = (foodSum == 0) ? 0 : 50
        
        UIView.animate(withDuration: 0.5) {
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.tableFooterView!.frame = CGRect(x:0, y:0, width: tableView.frame.width, height: 75)
    }
    
    func setupData() {
        let imageNames = ["apple", "broccoli", "carrot", "cherries", "cucumber", "lemon", "orange", "pear", "pineapple", "pumpkin", "strawberry", "watermelon"]
        let foodTypes : [FoodType] = [.fruit, .vegetable, .vegetable, .fruit, .vegetable, .fruit, .fruit, .fruit, .fruit, .vegetable, .fruit, .fruit]
        let descriptions = ["Minions ipsum la bodaaa pepete poopayee butt voluptate tatata bala tu daa underweaaar.",
                            "Minions ipsum la bodaaa pepte tatata bala tu daa underweaaar.",
                            "Minions ipsum la bodaaaderweaaar.",
                            "e poopayee butt voluptate tatata bala tu daa underweaaar.",
                            "Minions ipsum la bodaaa pepete poopayee butt voluptate tatata bala tu daa underweaaar. Minions ipsum la bodaaa pepete poopayee butt voluptate tatata bala tu daa underweaaar.",
                            "Minions erweaaar.",
                            "Minions ipsum la bodaaa pepete poopayee bala tu daa underweaaar.",
                            "Minions ipsum la bodaaa pepete poopayee butt voluptate tatata bala tu daa underweaaar.",
                            "Minata bala tu daa underweaaar.",
                            "Miniotata bala tu daa underweaaar.",
                            "Minions ipsum la bodaaa pepete poopayee butt vonderweaaar.",
                            "bala tu daa underweaaar."]
        for (index, name) in imageNames.enumerated() {
            let image = UIImage(named: name)!
            let fruitDescription = descriptions[index]
            let foodType = foodTypes[index]
            
            let fruit = Food(name: name, description: fruitDescription, image: image, quantity: 0, type: foodType)
            
            if(foodType == .vegetable) {
                vegetableData.append(fruit)
            }
            else {
                fruitData.append(fruit)
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0) {
            return fruitData.count
        }
        else {
            return vegetableData.count
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "fruitCell", for: indexPath) as! FruitCell
        
        var actualFood : Food!
        if(indexPath.section == 0) {
            actualFood = fruitData[indexPath.row]
        }
        else {
            actualFood = vegetableData[indexPath.row]
        }
        
        cell.fruit = actualFood
        cell.selectionStyle = .none
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0: return "Gyümölcsök"
        case 1: return "Zöldségek"
        default: return nil
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

}

