//
//  ViewController.swift
//  StaticTableView
//
//  Created by Andras Somlai on 2018. 05. 10..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func segmentedControlValuieChanged(_ sender: UISegmentedControl) {
        debugPrint("van remény? - \(sender.titleForSegment(at: sender.selectedSegmentIndex) ?? "Nem tudjuk")")
    }
    
}

