//
//  ViewController.swift
//  TaskList
//
//  Created by Andras Somlai on 2018. 05. 10..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activeTaskView: ActiveTaskView!
    
    @IBOutlet weak var activeTaskViewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        activeTaskViewHeight.constant = 0
        NotificationCenter.default.addObserver(self, selector: #selector(updateUI), name: Notification.Name("DataCenterUpdated"), object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataCenter.sharedInstance.getAllTask().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "taskCell") as! TaskCell
        cell.task = DataCenter.sharedInstance.getAllTask()[indexPath.row]
        return cell
    }

    @objc func updateUI() {
        tableView.reloadData()
        guard let activeTask = DataCenter.sharedInstance.activeTask() else {
            activeTaskViewHeight.constant = 0
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
            return
        }
        
        activeTaskView.task = activeTask
        activeTaskViewHeight.constant = 100
        UIView.animate(withDuration: 0.2) {
            self.view.layoutIfNeeded()
        }
    }
}

