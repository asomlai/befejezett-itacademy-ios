//
//  Task.swift
//  TaskList
//
//  Created by Andras Somlai on 2018. 05. 10..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class Task {
    var identifier : Int = 0
    var name : String = ""
    private var _totalTime : Double = 0 //Ebben a lezárt sessionök össz idejét tárolom
    var totalTime : Double {    //Ennek segítségével a taszkkal eltötött össz időt tudom lekérni
        get {
            if(isActive) {
                let lastSpentTime = Date().timeIntervalSince1970 - lastStarted
                debugPrint("TaskName: \(name)---TIME: \(_totalTime + lastSpentTime)")
                return _totalTime + lastSpentTime
            }
            else {
                return _totalTime
            }
        }
    }
    var isActive : Bool = false {
        didSet {
            activationChanged()
        }
    }
    private var lastStarted : TimeInterval = 0 //Ebben tárolom, hogy mikor indítottuk el utoljára a taszkot. 0, ha a taszk inactive
    private var timer : Timer?
    
    private func activationChanged() {
        if(isActive == true) {
            lastStarted = Date().timeIntervalSince1970
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerFired(sender:)), userInfo: nil, repeats: true)
        }
        else {
            if(lastStarted != 0) {
                let lastSpentTime = Date().timeIntervalSince1970 - lastStarted
                lastStarted = 0
                _totalTime += lastSpentTime
                timer?.invalidate()
                timer = nil
            }
        }
    }
    
    @objc func timerFired(sender : Timer) {
        NotificationCenter.default.post(name: Notification.Name("TaskUpdated\(identifier)"), object:nil, userInfo:["taskId" : identifier])
    }
}
