//
//  DataCenter.swift
//  TaskList
//
//  Created by Andras Somlai on 2018. 05. 10..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class DataCenter: NSObject {
    static let sharedInstance: DataCenter = {
        let instance = DataCenter()
        return instance
    }()
    private var tasks : [Task] = []
    
    private override init() {
        let taskNames = [
        "Project setup",
        "Login screen",
        "Registration screen",
        "Network communication setup",
        "Login service",
        "Registration service",
        "Home screen UI",
        "Download Home screen data",
        "Home screen animations",
        "Loading indicator",
        "Error handling on Home screen",
        "Details screen design",
        "Implement details screen UI",
        "Details screen download data",
        "Details screen edit data",
        "Details screen delete",
        "Details screen network communication",
        ]
        
        for (index, taskName) in taskNames.enumerated() {
            let task = Task()
            task.name = taskName
            task.identifier = index
            tasks.append(task)
        }
    }
    
    func getAllTask() -> [Task] {
        return tasks
    }
    
    func getTaskWithId(identifier : Int) -> Task? {
        for task in tasks {
            if(task.identifier == identifier) {
                return task
            }
        }
        return nil
    }
    
    func activeTask() -> Task? {
        for task in tasks {
            if(task.isActive == true) {
                return task
            }
        }
        return nil
    }
    
    func toggleTaskActivation(_ task : Task) {
        let foundTask = getTaskWithId(identifier: task.identifier)
        foundTask!.isActive = !foundTask!.isActive
        dataCentarUpdated()
    }
    
    private func dataCentarUpdated() {
        NotificationCenter.default.post(name: Notification.Name("DataCenterUpdated"), object: nil)
    }
}
