//
//  ActiveTaskView.swift
//  TaskList
//
//  Created by Andras Somlai on 2018. 05. 10..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ActiveTaskView: UIView {
    @IBOutlet weak var lbl: UILabel!
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var timeSpentLabel: UILabel!
    @IBOutlet weak var startStopButton: UIButton!
    var task : Task? {
        didSet {
            NotificationCenter.default.removeObserver(self)
            if(task != nil) {
                NotificationCenter.default.addObserver(self, selector: #selector(taskUpdated(notification:)), name: Notification.Name("TaskUpdated\(task!.identifier)"), object: nil)
            }
            updateUI()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func startStop(sender : UIButton) {
        if(task != nil) {
            DataCenter.sharedInstance.toggleTaskActivation(task!)
        }
    }
    
    @objc func taskUpdated(notification: Notification) {
        let updatedTaskId = (notification.userInfo as! [String : Int])["taskId"]
        if(task!.identifier == updatedTaskId) {
            DispatchQueue.main.async {
                self.updateUI()
            }
        }
    }
    
    func updateUI() {
        if(task != nil) {
            taskNameLabel.text = task!.name
            startStopButton.setTitle(task!.isActive ? "Stop" : "Start", for: .normal)
            timeSpentLabel.text = String(task!.totalTime)
            NotificationCenter.default.addObserver(self, selector: #selector(taskUpdated(notification:)), name: Notification.Name("TaskUpdated"), object: nil)
            
        }
        else {
            taskNameLabel.text = "-"
            timeSpentLabel.text = "-"
            NotificationCenter.default.removeObserver(self)
        }
    }

}
