//
//  TaskCellTableViewCell.swift
//  TaskList
//
//  Created by Andras Somlai on 2018. 05. 10..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class TaskCell: UITableViewCell {
    @IBOutlet weak var taskNameLabel: UILabel!
    @IBOutlet weak var timeSpentLabel: UILabel!
    @IBOutlet weak var startStopButton: UIButton!
    var task : Task! {
        didSet {
            NotificationCenter.default.addObserver(self, selector: #selector(taskUpdated(notification:)), name: Notification.Name("TaskUpdated\(task.identifier)"), object: nil)
            updateUI()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func startStop(_ sender: UIButton) {
        
        DataCenter.sharedInstance.toggleTaskActivation(task)
    }
    
    private func updateUI() {
        taskNameLabel.text = task.name
        timeSpentLabel.text = String(task.totalTime)
        if(task.isActive) {
            contentView.backgroundColor = #colorLiteral(red: 0.5843137503, green: 0.8235294223, blue: 0.4196078479, alpha: 1)
            startStopButton.setTitle("Stop", for: .normal)
        }
        else {
            contentView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
            startStopButton.setTitle("Start", for: .normal)
        }
    }
    
    @objc func taskUpdated(notification: Notification) {
        let updatedTaskId = (notification.userInfo as! [String : Int])["taskId"]
        if(task.identifier == updatedTaskId) {
            DispatchQueue.main.async {
                self.updateUI()
            }
        }
    }
}
