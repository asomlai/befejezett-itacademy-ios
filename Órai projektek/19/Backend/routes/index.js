var express = require('express');
var router = express.Router();

module.exports = function (app) {

  app.get('/recruiting/attendees',
    function(req, res, next) {
      res.send([
       {
          "name":{
             "lastname":"Hatfield",
             "firstname":"Tommie"
          },
          "gender":"male",
          "age":65,
          "id":0
       },
       {
          "name":{
             "lastname":"Moon",
             "firstname":"Miriam"
          },
          "gender":"female",
          "age":14,
          "id":1
       },
       {
          "name":{
             "lastname":"Hanson",
             "firstname":"Daniel"
          },
          "gender":"male",
          "age":32,
          "id":2
       },
       {
          "name":{
             "lastname":"Kovács",
             "firstname":"Éva"
          },
          "gender":"female",
          "age":32,
          "id":3
       },
       {
          "name":{
             "lastname":"Vaughan",
             "firstname":"Rutledge"
          },
          "gender":"male",
          "age":18,
          "id":4
       },
       {
          "name":{
             "lastname":"Ádám",
             "firstname":"Júlia"
          },
          "gender":"female",
          "age":38,
          "id":5
       },
       {
          "name":{
             "lastname":"Nunez",
             "firstname":"Dunn"
          },
          "gender":"male",
          "age":21,
          "id":6
       },
       {
          "name":{
             "lastname":"Washington",
             "firstname":"Margo"
          },
          "gender":"female",
          "age":34,
          "id":7
       },
       {
          "name":{
             "lastname":"Mcgee",
             "firstname":"Christensen"
          },
          "gender":"male",
          "age":62,
          "id":8
       },
       {
          "name":{
             "lastname":"Álmos",
             "firstname":"Liliána"
          },
          "gender":"female",
          "age":40,
          "id":9
       },
       {
          "name":{
             "lastname":"Bird",
             "firstname":"Harrell"
          },
          "gender":"male",
          "age":24,
          "id":10
       },
       {
          "name":{
             "lastname":"Merritt",
             "firstname":"James"
          },
          "gender":"female",
          "age":61,
          "id":11
       },
       {
          "name":{
             "lastname":"Ördögh",
             "firstname":"Ákos"
          },
          "gender":"male",
          "age":47,
          "id":12
       },
       {
          "name":{
             "lastname":"Justice",
             "firstname":"Gale"
          },
          "gender":"female",
          "age":53,
          "id":13
       },
       {
          "name":{
             "lastname":"Pickett",
             "firstname":"Thelma"
          },
          "gender":"male",
          "age":54,
          "id":14
       },
       {
          "name":{
             "lastname":"Randolph",
             "firstname":"Melendez"
          },
          "gender":"female",
          "age":35,
          "id":15
       },
       {
          "name":{
             "lastname":"Úr",
             "firstname":"István"
          },
          "gender":"male",
          "age":21,
          "id":16
       },
       {
          "name":{
             "lastname":"Ross",
             "firstname":"Charity"
          },
          "gender":"female",
          "age":37,
          "id":17
       },
       {
          "name":{
             "lastname":"László",
             "firstname":"Péter"
          },
          "gender":"male",
          "age":65,
          "id":18
       },
       {
          "name":{
             "lastname":"Joyce",
             "firstname":"Lynn"
          },
          "gender":"female",
          "age":63,
          "id":19
       },
       {
          "name":{
             "lastname":"Odonnell",
             "firstname":"Robertson"
          },
          "gender":"male",
          "age":41,
          "id":20
       },
       {
          "name":{
             "lastname":"Whitehead",
             "firstname":"Herring"
          },
          "gender":"female",
          "age":58,
          "id":21
       },
       {
          "name":{
             "lastname":"Burgess",
             "firstname":"Finch"
          },
          "gender":"male",
          "age":58,
          "id":22
       },
       {
          "name":{
             "lastname":"Suarez",
             "firstname":"Victoria"
          },
          "gender":"female",
          "age":35,
          "id":23
       },
       {
          "name":{
             "lastname":"Riley",
             "firstname":"Sharpe"
          },
          "gender":"male",
          "age":59,
          "id":24
       },
       {
          "name":{
             "lastname":"Fischer",
             "firstname":"Cook"
          },
          "gender":"female",
          "age":17,
          "id":25
       },
       {
          "name":{
             "lastname":"Espinoza",
             "firstname":"Hale"
          },
          "gender":"male",
          "age":27,
          "id":26
       },
       {
          "name":{
             "lastname":"Guthrie",
             "firstname":"Hess"
          },
          "gender":"female",
          "age":63,
          "id":27
       },
       {
          "name":{
             "lastname":"Stewart",
             "firstname":"Estella"
          },
          "gender":"male",
          "age":65,
          "id":28
       },
       {
          "name":{
             "lastname":"Nixon",
             "firstname":"Sargent"
          },
          "gender":"female",
          "age":17,
          "id":29
       },
       {
          "name":{
             "lastname":"Nash",
             "firstname":"Kristine"
          },
          "gender":"male",
          "age":64,
          "id":30
       },
       {
          "name":{
             "lastname":"Houston",
             "firstname":"Schwartz"
          },
          "gender":"female",
          "age":15,
          "id":31
       },
       {
          "name":{
             "lastname":"Greene",
             "firstname":"Arlene"
          },
          "gender":"male",
          "age":46,
          "id":32
       },
       {
          "name":{
             "lastname":"Sampson",
             "firstname":"Beach"
          },
          "gender":"female",
          "age":27,
          "id":33
       },
       {
          "name":{
             "lastname":"Hooper",
             "firstname":"Ashley"
          },
          "gender":"male",
          "age":31,
          "id":34
       },
       {
          "name":{
             "lastname":"Reynolds",
             "firstname":"Ramos"
          },
          "gender":"female",
          "age":59,
          "id":35
       },
       {
          "name":{
             "lastname":"Gilliam",
             "firstname":"Cornelia"
          },
          "gender":"male",
          "age":57,
          "id":36
       },
       {
          "name":{
             "lastname":"Knowles",
             "firstname":"Tracey"
          },
          "gender":"female",
          "age":54,
          "id":37
       },
       {
          "name":{
             "lastname":"Travis",
             "firstname":"Hardy"
          },
          "gender":"male",
          "age":58,
          "id":38
       },
       {
          "name":{
             "lastname":"Hensley",
             "firstname":"Alexis"
          },
          "gender":"female",
          "age":28,
          "id":39
       },
       {
          "name":{
             "lastname":"Daugherty",
             "firstname":"Jo"
          },
          "gender":"male",
          "age":39,
          "id":40
       },
       {
          "name":{
             "lastname":"Slater",
             "firstname":"Patsy"
          },
          "gender":"female",
          "age":23,
          "id":41
       },
       {
          "name":{
             "lastname":"Blankenship",
             "firstname":"Savage"
          },
          "gender":"male",
          "age":35,
          "id":42
       },
       {
          "name":{
             "lastname":"Myers",
             "firstname":"Marshall"
          },
          "gender":"female",
          "age":64,
          "id":43
       },
       {
          "name":{
             "lastname":"Graham",
             "firstname":"Paulette"
          },
          "gender":"male",
          "age":28,
          "id":44
       },
       {
          "name":{
             "lastname":"Fletcher",
             "firstname":"Brigitte"
          },
          "gender":"female",
          "age":31,
          "id":45
       },
       {
          "name":{
             "lastname":"Kinney",
             "firstname":"Johnson"
          },
          "gender":"male",
          "age":59,
          "id":46
       },
       {
          "name":{
             "lastname":"Maynard",
             "firstname":"Warner"
          },
          "gender":"female",
          "age":65,
          "id":47
       },
       {
          "name":{
             "lastname":"Dale",
             "firstname":"Christy"
          },
          "gender":"male",
          "age":51,
          "id":48
       },
       {
          "name":{
             "lastname":"Willis",
             "firstname":"Dolly"
          },
          "gender":"female",
          "age":59,
          "id":49
       },
       {
          "name":{
             "lastname":"Foster",
             "firstname":"Rosario"
          },
          "gender":"male",
          "age":32,
          "id":50
       },
       {
          "name":{
             "lastname":"Buckley",
             "firstname":"Buckner"
          },
          "gender":"female",
          "age":33,
          "id":51
       },
       {
          "name":{
             "lastname":"Horn",
             "firstname":"Olson"
          },
          "gender":"male",
          "age":34,
          "id":52
       },
       {
          "name":{
             "lastname":"Langley",
             "firstname":"Jamie"
          },
          "gender":"female",
          "age":57,
          "id":53
       },
       {
          "name":{
             "lastname":"Winters",
             "firstname":"Morton"
          },
          "gender":"male",
          "age":34,
          "id":54
       },
       {
          "name":{
             "lastname":"Gillespie",
             "firstname":"Frieda"
          },
          "gender":"female",
          "age":65,
          "id":55
       },
       {
          "name":{
             "lastname":"Harper",
             "firstname":"Dee"
          },
          "gender":"male",
          "age":45,
          "id":56
       },
       {
          "name":{
             "lastname":"Guerra",
             "firstname":"Alisa"
          },
          "gender":"female",
          "age":37,
          "id":57
       },
       {
          "name":{
             "lastname":"Carey",
             "firstname":"Allyson"
          },
          "gender":"male",
          "age":52,
          "id":58
       },
       {
          "name":{
             "lastname":"Rodgers",
             "firstname":"Deanne"
          },
          "gender":"female",
          "age":39,
          "id":59
       },
       {
          "name":{
             "lastname":"Douglas",
             "firstname":"Ernestine"
          },
          "gender":"male",
          "age":43,
          "id":60
       },
       {
          "name":{
             "lastname":"Bowman",
             "firstname":"Wooten"
          },
          "gender":"female",
          "age":52,
          "id":61
       },
       {
          "name":{
             "lastname":"Banks",
             "firstname":"Welch"
          },
          "gender":"male",
          "age":57,
          "id":62
       },
       {
          "name":{
             "lastname":"Hendrix",
             "firstname":"Karen"
          },
          "gender":"female",
          "age":14,
          "id":63
       },
       {
          "name":{
             "lastname":"Harvey",
             "firstname":"Nellie"
          },
          "gender":"male",
          "age":31,
          "id":64
       },
       {
          "name":{
             "lastname":"Beck",
             "firstname":"Bonita"
          },
          "gender":"female",
          "age":60,
          "id":65
       },
       {
          "name":{
             "lastname":"Mccullough",
             "firstname":"Gray"
          },
          "gender":"male",
          "age":45,
          "id":66
       },
       {
          "name":{
             "lastname":"Holland",
             "firstname":"Thomas"
          },
          "gender":"female",
          "age":39,
          "id":67
       },
       {
          "name":{
             "lastname":"West",
             "firstname":"Talley"
          },
          "gender":"male",
          "age":31,
          "id":68
       },
       {
          "name":{
             "lastname":"Oconnor",
             "firstname":"Morse"
          },
          "gender":"female",
          "age":37,
          "id":69
       },
       {
          "name":{
             "lastname":"Black",
             "firstname":"Anne"
          },
          "gender":"male",
          "age":26,
          "id":70
       },
       {
          "name":{
             "lastname":"Woodard",
             "firstname":"Sandoval"
          },
          "gender":"female",
          "age":33,
          "id":71
       },
       {
          "name":{
             "lastname":"Hinton",
             "firstname":"Lelia"
          },
          "gender":"male",
          "age":58,
          "id":72
       },
       {
          "name":{
             "lastname":"Mason",
             "firstname":"Shelia"
          },
          "gender":"female",
          "age":48,
          "id":73
       },
       {
          "name":{
             "lastname":"Norton",
             "firstname":"Kerry"
          },
          "gender":"male",
          "age":47,
          "id":74
       },
       {
          "name":{
             "lastname":"Bond",
             "firstname":"Connie"
          },
          "gender":"female",
          "age":23,
          "id":75
       },
       {
          "name":{
             "lastname":"Decker",
             "firstname":"Randi"
          },
          "gender":"male",
          "age":47,
          "id":76
       },
       {
          "name":{
             "lastname":"Lynch",
             "firstname":"Noble"
          },
          "gender":"female",
          "age":32,
          "id":77
       },
       {
          "name":{
             "lastname":"Haynes",
             "firstname":"Petty"
          },
          "gender":"male",
          "age":23,
          "id":78
       },
       {
          "name":{
             "lastname":"Kerr",
             "firstname":"Montoya"
          },
          "gender":"female",
          "age":45,
          "id":79
       },
       {
          "name":{
             "lastname":"Levy",
             "firstname":"Fitzpatrick"
          },
          "gender":"male",
          "age":59,
          "id":80
       },
       {
          "name":{
             "lastname":"Dawson",
             "firstname":"Hubbard"
          },
          "gender":"female",
          "age":33,
          "id":81
       },
       {
          "name":{
             "lastname":"Good",
             "firstname":"Casandra"
          },
          "gender":"male",
          "age":30,
          "id":82
       },
       {
          "name":{
             "lastname":"Hansen",
             "firstname":"Aimee"
          },
          "gender":"female",
          "age":21,
          "id":83
       },
       {
          "name":{
             "lastname":"Talley",
             "firstname":"Ferguson"
          },
          "gender":"male",
          "age":46,
          "id":84
       }
    ]);
    }
  );

  app.post('/postRequest',
    function(req, res, next) {
      res.send(req.body);
    }
  );

  app.post('/login',
    function(req, res, next) {
      if (req.body.email == undefined || req.body.password == undefined) {
        res.sendError(400, "Please fill all fields")
        return
      }

      if (req.body.email != "aaaa@bbb.hu" || req.body.password != "aaaa") {
        res.sendError(404, "Please check your email and password.")
        return
      }

      res.sendStatus(200)

    }
  );

  app.post('/signUp',
    function(req, res, next) {
      if (req.body.email == undefined || req.body.password == undefined || req.body.rePassword == undefined) {
        res.sendError(400, "Please fill all fields")
        return
      }

      if (req.body.password != req.body.rePassword) {
        res.sendError(406, "Passwords do not match")
        return
      }

      res.sendStatus(200);
    }
  );

  app.get('/products',
    function(req, res, next) {
      res.send(
        [{
        "_id": {
          "oid": "5968dd23fc13ae04d9000001"
        },
        "product_name": "sildenafil citrate",
        "supplier": "Wisozk Inc",
        "quantity": 261,
        "unit_cost": "$10.47"
      }, {
        "_id": {
          "oid": "5968dd23fc13ae04d9000002"
        },
        "product_name": "Mountain Juniperus ashei",
        "supplier": "Keebler-Hilpert",
        "quantity": 292,
        "unit_cost": "$8.74"
      }, {
        "_id": {
          "oid": "5968dd23fc13ae04d9000003"
        },
        "product_name": "Dextromathorphan HBr",
        "supplier": "Schmitt-Weissnat",
        "quantity": 211,
        "unit_cost": "$20.53"
      }]);
    }
  );

  app.get('/people',
    function(req, res, next) {
      res.send(
        [{
        "id": 1,
        "first_name": "Jeanette",
        "last_name": "Penddreth",
        "email": "jpenddreth0@census.gov",
        "gender": "Female",
        "ip_address": "26.58.193.2"
      }, {
        "id": 2,
        "first_name": "Giavani",
        "last_name": "Frediani",
        "email": "gfrediani1@senate.gov",
        "gender": "Male",
        "ip_address": "229.179.4.212"
      }, {
        "id": 3,
        "first_name": "Noell",
        "last_name": "Bea",
        "email": "nbea2@imageshack.us",
        "gender": "Female",
        "ip_address": "180.66.162.255"
      }, {
        "id": 4,
        "first_name": "Willard",
        "last_name": "Valek",
        "email": "wvalek3@vk.com",
        "gender": "Male",
        "ip_address": "67.76.188.26"
      }]);
    }
  );

  app.get('/peopleWithHobbies',
    function(req, res, next) {
      res.send(
        [{
        "id": 1,
        "first_name": "Jeanette",
        "last_name": "Penddreth",
        "email": "jpenddreth0@census.gov",
        "gender": "Female",
        "ip_address": "26.58.193.2",
        "hobbies" : [
          {
            "id" : 1,
            "name" : "Running"
          }, {
            "id" : 2,
            "name" : "Biking"
          }
        ]
      }, {
        "id": 2,
        "first_name": "Giavani",
        "last_name": "Frediani",
        "email": "gfrediani1@senate.gov",
        "gender": "Male",
        "ip_address": "229.179.4.212",
        "hobbies" : [
          {
            "id" : 1,
            "name" : "Running"
          }, {
            "id" : 2,
            "name" : "Biking"
          }
        ]
      }, {
        "id": 3,
        "first_name": "Noell",
        "last_name": "Bea",
        "email": "nbea2@imageshack.us",
        "gender": "Female",
        "ip_address": "180.66.162.255",
        "hobbies" : [
          {
            "id" : 1,
            "name" : "Running"
          }, {
            "id" : 2,
            "name" : "Biking"
          }
        ]
      }, {
        "id": 4,
        "first_name": "Willard",
        "last_name": "Valek",
        "email": "wvalek3@vk.com",
        "gender": "Male",
        "ip_address": "67.76.188.26"
      }]);
    }
  );

  app.get('/clients',
    function(req, res, next) {
      if(req.get("token") != "itacademy") {
        res.sendError(404, "not logged in")
        return;
      }
      res.send({
        "clients": [
          {
            "id": "59761c23b30d971669fb42ff",
            "isActive": true,
            "age": 36,
            "name": "Dunlap Hubbard",
            "gender": "male",
            "company": "CEDWARD",
            "email": "dunlaphubbard@cedward.com",
            "phone": "+1 (890) 543-2508",
            "address": "169 Rutledge Street, Konterra, Northern Mariana Islands, 8551"
          },
          {
            "id": "59761c233d8d0f92a6b0570d",
            "isActive": true,
            "age": 24,
            "name": "Kirsten Sellers",
            "gender": "female",
            "company": "EMERGENT",
            "email": "kirstensellers@emergent.com",
            "phone": "+1 (831) 564-2190",
            "address": "886 Gallatin Place, Fannett, Arkansas, 4656"
          },
          {
            "id": "59761c23fcb6254b1a06dad5",
            "isActive": true,
            "age": 30,
            "name": "Acosta Robbins",
            "gender": "male",
            "company": "ORGANICA",
            "email": "acostarobbins@organica.com",
            "phone": "+1 (882) 441-3367",
            "address": "697 Linden Boulevard, Sattley, Idaho, 1035"
          }
        ]
      });
    }
  );

  app.get('/mapData',
    function(req, res, next) {
      // res.send(req.query)
      // return
      if(!(req.query.markersRequested == "true" || req.query.markersRequested == true)) {
        res.send({"message" : "No data to show"})
        return;
      }
      res.send({
        "markers": [
          {
            "name": "Rixos The Palm Dubai",
            "position": [25.1212, 55.1535],
          },
          {
            "name": "Shangri-La Hotel",
            "location": [25.2084, 55.2719]
          },
          {
            "name": "Grand Hyatt",
            "location": [25.2285, 55.3273]
          }
        ]
      });
    }
  );

}
