var common = require('../middleware/common');

module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define("user",
    {
      id : { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true, allowNull: false },
      creationDate : {type : DataTypes.BIGINT, allowNull: false, defaultValue: function() {return common.getNowAsTimestamp()} },
      deleted : {type : DataTypes.BOOLEAN, allowNull: false, defaultValue:false },

      email : {type : DataTypes.STRING, allowNull: false },
      name : {type : DataTypes.STRING, allowNull: true },
      password : {type : DataTypes.STRING, allowNull: false }
      //role : {type : DataTypes.ENUM, values : ['EMPLOYEE','EMPLOYER'], allowNull: false }
    },
    {
      charset: 'utf8',
      collate: 'utf8_unicode_ci'
    }
  );

  User.associate = function(models) {
    User.hasMany(models.device);
    //User.hasMany(models.project);
    //User.hasMany(models.userProjectConnection);
    //User.hasMany(models.userTaskConnection);
  }

  return User;
};
