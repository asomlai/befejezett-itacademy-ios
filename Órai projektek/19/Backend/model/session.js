var common = require('../middleware/common');

module.exports = function(sequelize, DataTypes) {
  var Session = sequelize.define("session",
    {
      id : { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true, unique: true, allowNull: false },
      creationDate : {type : DataTypes.BIGINT, allowNull: false, defaultValue: function() {return common.getNowAsTimestamp()} },
      deleted : {type : DataTypes.BOOLEAN, allowNull: false, defaultValue:false },

      sessionId : { type : DataTypes.STRING, allowNull: false },
      validTo : {type : DataTypes.BIGINT, allowNull: false }
    },
    {
      charset: 'utf8',
      collate: 'utf8_unicode_ci'
    }
  );

  Session.associate = function(models) {
  	Session.belongsTo(models.device, {foreignKey: {name:'deviceId', allowNull:true}})
  }

  return Session;
};
