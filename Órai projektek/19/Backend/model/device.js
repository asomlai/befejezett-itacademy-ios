var common = require('../middleware/common');

module.exports = function(sequelize, DataTypes) {
  var Device = sequelize.define("device",
    {
      id : { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true, unique: true, allowNull: false },
      creationDate : {type : DataTypes.BIGINT, allowNull: false, defaultValue: function() {return common.getNowAsTimestamp()} },
      deleted : {type : DataTypes.BOOLEAN, allowNull: false, defaultValue:false },

      hardwareId : { type : DataTypes.STRING, allowNull: false, unique: true},
      os : {type : DataTypes.ENUM, values : ['IOS','MAC', 'ANDROID', 'WINDOWS'], allowNull: false },
      osVersion : { type : DataTypes.STRING, allowNull: false },
    },
    {
      charset: 'utf8',
      collate: 'utf8_unicode_ci'
    }
  );

  Device.associate = function(models) {
  	Device.belongsTo(models.user, {foreignKey: {name:'userId', allowNull:false}});
  	Device.hasMany(models.session);
  }

  return Device;
};
