var fs        = require("fs");
var path      = require("path");
var Sequelize = require("sequelize");

var envConfig = require('../config/env');

var conString;

var db = {};

// database connection setup
// if (envConfig.env === 'development') {
//   console.log('Using development settings.');
//   conString = envConfig.connectionString;
// }else{
  // console.log('Using production settings.');
conString = "mysql://"+ envConfig.database.DB_USERNAME +":"+ envConfig.database.DB_PASSWORD +"@"+ envConfig.database.DB_HOSTNAME + ":" + envConfig.database.DB_PORT + "/" + envConfig.database.DB_NAME;
// }

// console.log(conString);

var sequelize = new Sequelize(conString,  {
    "dialect": "mysql",
    "logging": false,
    "define": {
        "timestamps": false,
    }
  });


fs.readdirSync(__dirname)
.filter(function(file) {
  return (file.indexOf(".") !== 0) && (file !== "init.js");
})
.forEach(function(file) {
  var model = sequelize.import(path.join(__dirname, file));
  db[model.name] = model;
});

Object.keys(db).forEach(function(modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;
console.log("Database initialized.");
module.exports = db;
