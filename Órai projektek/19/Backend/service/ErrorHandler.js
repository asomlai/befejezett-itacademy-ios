function  checkRequiredParameters(HEADERParameterNames, GETParameterNames, POSTParameterNames) {
	return function(req, res, next){
		var missingHEADERParams = [];
		var missingGETParams = [];
		var missingPOSTParams = [];

		for(var i = 0 ; i < HEADERParameterNames.length ; i++) {
			var parameterName = HEADERParameterNames[i];
			var parameter = req.get(parameterName);
			if (parameter == undefined) {
				missingHEADERParams.push(parameterName);
			}
		}

		//Handles pretty url parameters (for example: /task/12/delete)
		for(var i = 0 ; i < GETParameterNames.length ; i++) {
			var parameterName = GETParameterNames[i];
			var parameter = req.query[parameterName]// != undefined ? req.query[parameterName] : req.params[parameterName]
			if (parameter == undefined) {
				missingGETParams.push(parameterName);
			}
		}

		for(var i = 0 ; i < POSTParameterNames.length ; i++) {
			var parameterName = POSTParameterNames[i];
			var parameter = req.body[parameterName];
			if (parameter == undefined) {
				missingPOSTParams.push(parameterName);
			}
		}


		if (missingHEADERParams.length != 0 || missingGETParams.length != 0 || missingPOSTParams.length != 0) {
			return res.sendError(422, {"errorDescription" : "Missing Parameters", "header" : missingHEADERParams, "get" : missingGETParams, "post" : missingPOSTParams});
		}
		next();
	}
}


module.exports =
{
	'checkRequiredParameters' : checkRequiredParameters
};
