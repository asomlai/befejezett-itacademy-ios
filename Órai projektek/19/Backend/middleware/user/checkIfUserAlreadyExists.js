var common = require('../common');

function checkIfUserAlreadyExists() {
  var userModel = common.requireObjectRepository('userModel');
	return function(req, res, next) {
		var email = req.body.email;

    userModel.findOne({
			where: {
				email: email,
				deleted: false
			}
		}).then(user => {
      //Registered user found with the given email
      if(user != undefined) {
        res.sendError(404, "The given email address is already registered");
        return;
      }

      //User can register
      next();
		})
    .catch(err => {
			res.sendError(500, "DB error: "+err);
		})
	}
}

module.exports = checkIfUserAlreadyExists;
