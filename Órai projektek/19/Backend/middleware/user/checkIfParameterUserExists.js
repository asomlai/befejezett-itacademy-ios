var common = require('../common');

function checkIfParameterUserExists() {
  var userModel = common.requireObjectRepository('userModel');

	return function(req, res, next) {
    var email = req.body.userEmail;

    //Search user by id in database
    userModel.findOne({
				where: {
					"email": email,
					"deleted": false
				}
		}).then(user => {
      //If no user was found
			if (user == undefined) {
				res.sendError(404, "User not found with the given email");
			}
      //If a user was found with the given identifier, can countinue operation
			else {
        res.data.userToAddToProject = user;
				next();
        return;
			}
		}).catch(err => {
			return res.sendError(503, "DB error:"+err);
		});
	}
}

module.exports = checkIfParameterUserExists;
