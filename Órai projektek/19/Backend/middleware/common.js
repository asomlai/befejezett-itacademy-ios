var models = require("../model/init");
var moment = require("moment");

var objectRepository = {
	'userModel': models.user,
	'deviceModel': models.device,
	'sessionModel': models.session
	// 'projectModel': models.project,
	// 'taskModel': models.task,
	// 'recordModel': models.record,
	// 'userProjectConnectionModel': models.userProjectConnection,
	// 'userTaskConnectionModel': models.userTaskConnection
}

function requireObjectRepository(propertyName) {

	if (!objectRepository) {
		throw new TypeError(propertyName + ' No object Repo');
	}
	if (!objectRepository[propertyName]) {
		throw new TypeError(propertyName + ' No propname');
	}
	if (objectRepository && objectRepository[propertyName]) {
		return objectRepository[propertyName];
	}
	throw new TypeError(propertyName + ' required');
}

function serverBaseUrl(req) {
	var fullUrl = req.protocol + '://' + req.get('host') + '/';
	return fullUrl;
}

function getNowAsTimestamp() {
	return moment().unix();
}

module.exports.requireObjectRepository = requireObjectRepository;
module.exports.serverBaseUrl = serverBaseUrl;
module.exports.getNowAsTimestamp = getNowAsTimestamp;
