var common = require('../common');



function saveDeviceDataIfRequired() {
	var deviceModel = common.requireObjectRepository('deviceModel');

	return function(req, res, next) {
		var hardwareId = req.get("hardwareId");

		//Looking for existing device
		deviceModel.findOne({
			"where": {
				"hardwareId": hardwareId,
				"deleted" : false
			}
		}).then(device => {
			if (device) {
				//If device was found with the same userId
				if(device.userId == res.data.user.id && device.osVersion == req.get("osVersion")) {
					res.data.device = device;
					next();
					return;
				}
				//If device was previously registered with different user or the os version changed
				else {
					//update existing device
					deviceModel.update({
						"userId" : res.data.user.id,
						"osVersion" : req.get("osVersion")
					}, {
						fields: ['userId', "osVersion"],
						where: {
							"hardwareId": hardwareId
						}
					}).then(function() {
						next();
					})
				}
			}
			//Is device was not registered
			else {
				//Create Device object
				var newDevice =
				{
					hardwareId : hardwareId,
					userId : res.data.user.id,
					os : req.get("os"),
					osVersion : req.get("osVersion")
				}
				//Save new device
				deviceModel.create(newDevice).then(function(cratedDevice) {
					res.data.device = cratedDevice;
					next();
				})
			}
		}).catch(err => {
			res.sendError(503, "DB error: " + err);
		})
	}
}

module.exports = saveDeviceDataIfRequired;
