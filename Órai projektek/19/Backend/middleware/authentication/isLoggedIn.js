var common = require('../common');

function isLoggedIn() {
	var userModel = common.requireObjectRepository('userModel');
	var deviceModel = common.requireObjectRepository('deviceModel');
	var sessionModel = common.requireObjectRepository('sessionModel');

	return function(req, res, next){
		var lastValidSessionDate = common.getNowAsTimestamp();

		//Find the user who actually own the device with a valid sessionId
		userModel.findOne({
			"include": [{
				"model": deviceModel,
				"include": [{
					"model": sessionModel,
					"where": {
						"sessionId": req.get("sessionId"),
						"deleted": false,
						"validTo": {
							$gt: lastValidSessionDate
						}
					}
				}],
				where: {
					"hardwareId": req.get("hardwareId"),
					"deleted": false
				}
			}]
		}).then(user => {
			//If we found a user with the given conditioons, the identification was successful
			if (user) {
				res.send({"isLoggedIn" : true});
			}
			else {
				res.send({"isLoggedIn" : false});
			}

		}).catch(err => {
			return res.sendError(503, "DB error:"+err);
		});
	}
}

module.exports = isLoggedIn;
