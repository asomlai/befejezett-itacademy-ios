var common = require('../common');
var uuid = require('uuid/v4');
var env = require('../../config/env');



function createSession() {
	var deviceModel = common.requireObjectRepository('deviceModel');
	var sessionModel = common.requireObjectRepository('sessionModel');

	return function(req, res, next) {
		//Get already registered device
		deviceModel.findOne({
			where: {
				"hardwareId": req.get("hardwareId"),
				"deleted": false
			}
		}).then(device => {
			//If no existing device was found
			if(device == undefined) {
				res.sendError(500, "Cant create session because the given device was not registered previously")
				return;
			}

			//Create session object
			var sessionData = {
				"deviceId": device.id,
				"sessionId": uuid(),
				"validTo": common.getNowAsTimestamp()+env.sessionValidity
			};

			//Save session
			sessionModel.create(sessionData).then(session => {
				res.data.session = session;
				next();
			});
		}).catch(err => {
			res.sendError(503, "DB error:" + err);
		})
	}
}

module.exports = createSession;
