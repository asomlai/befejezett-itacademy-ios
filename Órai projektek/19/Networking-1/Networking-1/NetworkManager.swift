//
//  NetworkManager.swift
//  Networking-1
//
//  Created by Andras Somlai on 2018. 05. 24..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class NetworkManager: NSObject {

    static func getPeople(completion : @escaping (String?, Error?) -> Void) {
        let url = URL(string: "http://138.197.187.213/itacademy/people")
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            let httpResponse = response as! HTTPURLResponse
            
            if(error != nil || data == nil || httpResponse.statusCode != 200) {
                print("Hiba történt")
                completion(nil, error)
                return
            }
            
            completion(String(data: data!, encoding:.utf8), nil)
        }
        
        task.resume()
    }
    
    
    static func getAndCastPeople(completion : @escaping (Any?, Error?) -> Void) {
        let url = URL(string: "http://138.197.187.213/itacademy/people")
        
        let task = URLSession.shared.dataTask(with: url!) {(data, response, error) in
            let httpResponse = response as! HTTPURLResponse
            
            if(error != nil || data == nil || httpResponse.statusCode != 200) {
                print("Hiba történt")
                completion(nil, error)
                return
            }
            
            do {
                let castedObject = try JSONSerialization.jsonObject(with: data!, options: []) as? [[String: Any]]
                
                completion(castedObject, nil)
            }
            catch {
                completion(nil, nil)
            }
        }
        
        task.resume()
    }
    
    static func login(completion : @escaping ([String : Any]?, Bool, Error?) -> Void) {
        let requestUrl = URL(string: "http://138.197.187.213/itacademy/login")!
        var request = URLRequest(url: requestUrl)
        
        let postParams = "email=aaa@bbb.hu&password=aaaa"
        request.httpBody = postParams.data(using: .utf8)
        request.httpMethod = "POST"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            let httpResponse = response as! HTTPURLResponse
            
            if(error != nil || data == nil) {
                print("Hiba történt")
                completion(nil, false, error)
                return
            }
            
            do {
                let castedObject = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                completion(castedObject, httpResponse.statusCode == 200, nil)
            }
            catch {
                completion(nil, httpResponse.statusCode == 200, nil)
            }
        }
        task.resume()
    }
    
    static func getClients(completion : @escaping ([String : Any]?, Error?) -> Void) {
        let url = URL(string: "http://138.197.187.213/itacademy/clients")!
        var request = URLRequest(url: url)
        request.setValue("itacademy", forHTTPHeaderField: "token")
        
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            let httpResponse = response as! HTTPURLResponse
            
            if(error != nil || data == nil || httpResponse.statusCode != 200) {
                print("Hiba történt")
                completion(nil, error)
                return
            }
            
            do {
                let castedObject = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                completion(castedObject, nil)
            }
            catch {
                completion(nil, nil)
            }
        }
        
        task.resume()
    }
    
    static func getMapData(completion : @escaping ([String : Any]?, Error?) -> Void) {
        let url = URL(string: "http://138.197.187.213/itacademy/mapData?markersRequested=true")!
        let request = URLRequest(url: url)
        
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            let httpResponse = response as! HTTPURLResponse
            
            if(error != nil || data == nil || httpResponse.statusCode != 200) {
                print("Hiba történt")
                completion(nil, error)
                return
            }
            
            do {
                let castedObject = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any]
                completion(castedObject, nil)
            }
            catch {
                completion(nil, nil)
            }
        }
        
        task.resume()
    }
    
    
}
