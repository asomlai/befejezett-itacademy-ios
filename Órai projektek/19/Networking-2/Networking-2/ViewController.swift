//
//  ViewController.swift
//  Networking-2
//
//  Created by Andras Somlai on 2018. 05. 24..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        NetworkManager.getPeople { (peopleData, error) in
            //debugPrint(peopleData)
        }
 
        NetworkManager.login { (loginData, loginSuccess, error) in
            //debugPrint(loginData, loginSuccess)
        }

        NetworkManager.getClients { (clientData, error) in
            //debugPrint(clientData)
        }
        
        NetworkManager.getMapData { (mapData, error) in
            debugPrint(mapData)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

