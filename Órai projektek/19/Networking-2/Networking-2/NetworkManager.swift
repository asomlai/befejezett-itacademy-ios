//
//  NetworkManager.swift
//  Networking-2
//
//  Created by Andras Somlai on 2018. 05. 24..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {

    static func getPeople(completion : @escaping ([[String : Any]]?, Error?) -> Void) {
        
        Alamofire.request("http://138.197.187.213/itacademy/people").responseJSON { response in
            let statusCode = response.response?.statusCode
            if(statusCode == 200) {
                if let data = response.result.value as? [[String : Any]] {
                    completion(data, nil)
                }
                else {
                    completion(nil, nil)
                }
            }
            else {
                completion(nil, response.error)
            }
        }
        
    }
    

    static func login(completion : @escaping ([String : Any]?, Bool, Error?) -> Void) {
        let loginPartameters = ["email" : "aaaa@bbb.hu", "password" : "aaada"]
        
        Alamofire.request("http://138.197.187.213/itacademy/login",  method: .post, parameters: loginPartameters)
            .responseJSON { (response) in
            let statusCode = response.response?.statusCode
            if let data = response.result.value as? [String : Any]? {
                completion(data, statusCode == 200, nil)
            }
            else {
                completion(nil, statusCode == 200, nil)
            }
        }
    }
    
    static func getClients(completion : @escaping ([String : Any]?, Error?) -> Void) {
        let header = ["token" : "itacademy"]
        Alamofire.request("http://138.197.187.213/itacademy/clients", headers: header).responseJSON { response in
            let statusCode = response.response?.statusCode
            if(statusCode == 200) {
                if let data = response.result.value as? [String : Any] {
                    completion(data, nil)
                }
                else {
                    completion(nil, nil)
                }
            }
            else {
                completion(nil, response.error)
            }
        }
    }
    
    static func getMapData(completion : @escaping ([String : Any]?, Error?) -> Void) {
        let urlParam = ["markersRequested" : true]
        Alamofire.request("http://138.197.187.213/itacademy/mapData", parameters: urlParam, encoding: URLEncoding(destination: .queryString)).responseJSON { response in
            let statusCode = response.response?.statusCode
            if(statusCode == 200) {
                if let data = response.result.value as? [String : Any] {
                    completion(data, nil)
                }
                else {
                    completion(nil, nil)
                }
            }
            else {
                completion(nil, response.error)
            }
        }
    }
}
