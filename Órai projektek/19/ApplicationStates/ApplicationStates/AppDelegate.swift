//
//  AppDelegate.swift
//  ApplicationStates
//
//  Created by Andras Somlai on 2018. 05. 24..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        debugPrint("didFinishLaunchingWithOptions ---> Az alkalmazás elkezdett futni")
        // Override point for customization after application launch.
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        debugPrint("applicationWillResignActive ---> Az alkalmazás aktív állapotból inaktívba vált" )
        /*
         Állítsuk le időkritikus folyamatokat (pl.: játék esetén pause)!
         */
        
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        debugPrint("applicationDidEnterBackground ---> Az alkalmazás a háttérbe kerül, ahol egy rövid ideig még futhat, hogy elmenthesse az állapotát")
        /*
          Szabadítsuk fel az erőforrásokat és mentsük el az alkalmazás állapotát (lehet, hogy később kilövi a rendszer). Maximum 5 másodpercig futhat, ha ennél több idő kell, akkor Background Taskot kell indítani!
         */
        
        var taskId = UIBackgroundTaskInvalid
        taskId = application.beginBackgroundTask {
            print("A background task kifogyott az időből és leállt")
            application.endBackgroundTask(taskId)
        }
        if taskId == UIBackgroundTaskInvalid {
            print("Sikertelen background task indítás!")
            return
        }
        
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        debugPrint("applicationWillEnterForeground ---> Az alkalmazás először inaktív állapotba kerül")
        /*
        Állítsuk vissza az elmentett alkalmazás állapotot, töltsük be újra a szükséges adatokat, kapcsolódjunk a hálózathoz, stb.
         */
        
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        debugPrint("applicationDidBecomeActive ---> Az alkalmazás újra aktív állapotban van, fut az előtérben")
        /*
          Indítsuk el a leállított időkritikus folyamatokat (pl. játék esetén unpause). Vegyük figyelembe, hogy ez a metódus az alkalmazás első indításakor is meghívódik!
         */
        
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        //5mp adat forrása: https://developer.apple.com/documentation/uikit/uiapplicationdelegate/1623111-applicationwillterminate
        debugPrint("applicationWillTerminate ---> Az alkalmazás futása véget fog érni. 5 másodpercen belül le kell futnia")
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

