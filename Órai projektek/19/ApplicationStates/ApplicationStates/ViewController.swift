//
//  ViewController.swift
//  ApplicationStates
//
//  Created by Andras Somlai on 2018. 05. 24..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let coverView = UIView()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: NSNotification.Name.UIApplicationDidBecomeActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: NSNotification.Name.UIApplicationDidEnterBackground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willresignActive), name: NSNotification.Name.UIApplicationWillResignActive, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func willEnterForeground() {
        coverView.backgroundColor = UIColor.green
    }
    
    @objc func didBecomeActive() {
        coverView.removeFromSuperview()
    }

    @objc func willresignActive() {
        coverView.frame = view.bounds
        coverView.backgroundColor = UIColor.orange
        view.addSubview(coverView)
    }
    
    @objc func didEnterBackground() {
        coverView.backgroundColor = UIColor.red
    }

}

