//
//  ViewController.swift
//  scrollDemo
//
//  Created by Andras Somlai on 2018. 04. 26..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate {

    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let textField = UITextField()
        self.view.addSubview(textField)
        textField.backgroundColor = UIColor.lightGray
        textField.frame = CGRect(x:0, y:20, width:200, height:50)
        textField.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let numberOfPages = 3
        
        for i in stride(from: 0, to: numberOfPages, by: 1) {
            let subview = UIView()
            scrollView.addSubview(subview)
            subview.frame = CGRect(x:10 + (scrollView.frame.width*CGFloat(i)),
                                   y:10,
                                   width:scrollView.frame.size.width-20,
                                   height:scrollView.frame.size.height-20)
            subview.backgroundColor = UIColor.orange
        }
        
        //scrollView.contentSize = CGSize(width: 0, height: 0)
        scrollView.contentSize = CGSize(width: CGFloat(numberOfPages) * scrollView.frame.width, height: scrollView.frame.height)
        //scrollView.isPagingEnabled = true
        
        scrollView.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        debugPrint("scrollViewDidScroll")
    }

    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        debugPrint("scrollViewWillBeginDragging")
    }
    
    func scrollViewWillBeginDecelerating(_ scrollView: UIScrollView) {
        debugPrint("scrollViewWillBeginDecelerating")
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        debugPrint("scrollViewDidEndDecelerating")
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        debugPrint("scrollViewWillEndDragging")
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        debugPrint("scrollViewDidEndDragging")
    }

    
    
    //TextField delegates
    func textFieldDidBeginEditing(_ textField: UITextField) {
        debugPrint("textFieldDidBeginEditing")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        debugPrint("textFieldDidEndEditing")
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        debugPrint("textFieldShouldReturn")
        self.view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        debugPrint("shouldChangeCharactersIn")
        if(string.lowercased() == "r") {
            return true
        }
        
        return false
    }
}

