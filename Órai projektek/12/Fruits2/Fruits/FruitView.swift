//
//  FruitView.swift
//  Fruits
//
//  Created by Andras Somlai on 2018. 04. 29..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class FruitView: UIView, UIScrollViewDelegate {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var likeScrollView: UIScrollView!
    @IBOutlet weak var oftenScrollView: UIScrollView!
    
    private let texts = ["naponta", "hetente", "kéthetente", "havonta", "negyedévente", "félévente", "évente"]
    var parentViewController : ViewController!
    
    @IBAction func addComment(_ sender: UIButton) {
        parentViewController.openCommentViewController()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupLikeScrollView()
        setupOftenScrollView()
        self.backgroundColor = UIColor(red: 46.0/255.0,
                                       green: 204.0/255.0,
                                       blue: 113.0/255.0,
                                       alpha: 1.0)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateOftenScrollViewFrames()
    }
    
    func setupLikeScrollView() {
        let rateButtonImageNames = ["like", "neutral", "dislike"]
        for (index, imageName) in rateButtonImageNames.enumerated() {
            let rateButton = UIButton()
            let buttonImage = UIImage(named: imageName)
            rateButton.setImage(buttonImage, for: .normal)
            likeScrollView.addSubview(rateButton)
            rateButton.frame = CGRect(x:0,
                                      y:CGFloat(index)*likeScrollView.frame.height,
                                      width:likeScrollView.frame.width,
                                      height:likeScrollView.frame.height)
            rateButton.isUserInteractionEnabled = false
        }
        likeScrollView.isPagingEnabled = true
        likeScrollView.delegate = self
        likeScrollView.contentSize = CGSize(width:likeScrollView.frame.width,
                                            height:CGFloat(rateButtonImageNames.count)*likeScrollView.frame.width)
    }
    
    func setupOftenScrollView() {
        
        for (index, actualText) in texts.enumerated() {
            let label = UILabel()
            label.text = actualText
            label.tag = index
            label.font = UIFont(name: "Arial-BoldMT", size: 22)!
            
            oftenScrollView.addSubview(label)
        }
        oftenScrollView.isPagingEnabled = true
    }
    
    func updateOftenScrollViewFrames() {
        for label in oftenScrollView.subviews {
            label.frame = CGRect(x:CGFloat(label.tag)*oftenScrollView.frame.width,
                                 y:0,
                                 width:oftenScrollView.frame.width,
                                 height:oftenScrollView.frame.height)
        }
        
        oftenScrollView.contentSize = CGSize(width:CGFloat(texts.count) * oftenScrollView.frame.width,
                                        height:oftenScrollView.frame.height)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        //#2ecc71
        let page = Int(scrollView.contentOffset.y / scrollView.frame.height)
        switch page {
        case 0: self.backgroundColor = UIColor(red: 46.0/255.0,
                                               green: 204.0/255.0,
                                               blue: 113.0/255.0,
                                               alpha: 1.0)
        case 1: self.backgroundColor =  UIColor(red: 241.0/255.0,
                                                green: 196.0/255.0,
                                                blue: 15.0/255.0,
                                                alpha: 1.0)
        case 2: self.backgroundColor =  UIColor(red: 231.0/255.0,
                                                green: 76.0/255.0,
                                                blue: 60.0/255.0,
                                                alpha: 1.0)
        default: self.backgroundColor = UIColor.white
        }
    }
    
}
