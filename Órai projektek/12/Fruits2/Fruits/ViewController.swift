//
//  ViewController.swift
//  Fruits
//
//  Created by Andras Somlai on 2018. 04. 29..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {
    let imageNames : [String] = ["apple", "broccoli", "carrot", "cherries", "lemon", "orange", "pineapple", "strawberry", "watermelon"]

    @IBOutlet weak var scrollView: UIScrollView!
    var isJustLoaded : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for (index, imageName) in imageNames.enumerated() {
            let fruitPage = Bundle.main.loadNibNamed("FruitView", owner: self, options: nil)![0] as! FruitView
            fruitPage.tag = index
            fruitPage.imageView.image = UIImage(named: imageName)
            fruitPage.parentViewController = self
            scrollView.addSubview(fruitPage)
        }
        
        scrollView.backgroundColor = UIColor.red
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for subview in scrollView.subviews {
            subview.frame = CGRect(x:CGFloat(subview.tag)*scrollView.frame.width,
                                   y:0,
                                   width:scrollView.frame.width,
                                   height:scrollView.frame.height)
        }
        
        scrollView.contentSize = CGSize(width:CGFloat(imageNames.count) * scrollView.frame.width, height:scrollView.frame.height)
        
        if(isJustLoaded == true) {
            isJustLoaded = false
            let lastPage = UserDefaults.standard.integer(forKey: "savedPage")
            if(lastPage != 0) {
                scrollView.contentOffset = CGPoint(x:CGFloat(lastPage)*scrollView.frame.size.width, y:0)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openCommentViewController() {
        debugPrint("OpenCommentViewController")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let commentVC = storyboard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        self.present(commentVC, animated: true) {}
    }

    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        UserDefaults.standard.set(page, forKey: "savedPage")
    }

}

