//
//  ViewController.swift
//  Fruits
//
//  Created by Andras Somlai on 2018. 04. 29..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let imageNames : [String] = ["apple", "broccoli", "carrot", "cherries", "lemon", "orange", "pineapple", "strawberry", "watermelon"]

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for (index, imageName) in imageNames.enumerated() {
            let fruitImage = UIImage(named: imageName)!
            let imageView = UIImageView()
            imageView.image = fruitImage
            imageView.frame = CGRect(x:CGFloat(index) * scrollView.frame.width, y:0, width:scrollView.frame.width, height:scrollView.frame.height)
            
            scrollView.addSubview(imageView)
        }
        scrollView.contentSize = CGSize(width:CGFloat(imageNames.count) * scrollView.frame.width, height:scrollView.frame.height)
        
        scrollView.isPagingEnabled = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

