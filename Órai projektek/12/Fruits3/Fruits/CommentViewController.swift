//
//  CommentViewController.swift
//  Fruits
//
//  Created by Andras Somlai on 2018. 04. 29..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class CommentViewController: UIViewController {
    var dataModel : FruitViewModel!
    @IBOutlet var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text = dataModel.comment
        // Do any additional setup after loading the view.
    }
    
    @IBAction func back(_ sender: UIButton) {
        dataModel.comment = textView.text
        dismiss(animated: true, completion: nil)
    }
    
}
