//
//  Enums.swift
//  Fruits
//
//  Created by Andras Somlai on 2018. 04. 29..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

enum Fruit : String {
    case apple = "apple"
    case broccoli = "broccoli"
    case carrot = "carrot"
    case cherries = "cherries"
    case lemon = "lemon"
    case orange = "orange"
    case pineapple = "pineapple"
    case strawberry = "strawberry"
    case watermelon = "watermelon"
    case unknown = "unknown"
    
    static let allValues : [Fruit] = [apple, broccoli, carrot, cherries, lemon, orange, pineapple, strawberry, watermelon]
}


enum Rating : String {
    case like = "like"
    case neutral = "neutral"
    case dislike = "dislike"
    
    static let allValues : [Rating] = [.like, .neutral, .dislike]
}

enum Often : String {
    case day = "naponta"
    case week = "hetente"
    case twoWeek = "kéthetente"
    case month = "havonta"
    case quarterYear = "negyedévente"
    case halfYear = "félévente"
    case year = "évente"
    
    static let allValues  : [Often] = [.day, .week, .twoWeek, .month, .quarterYear, .halfYear, .year]
}
