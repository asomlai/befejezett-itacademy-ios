//
//  ViewController.swift
//  Fruits
//
//  Created by Andras Somlai on 2018. 04. 29..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {
    @IBOutlet weak var scrollView: UIScrollView!
    var isJustLoaded : Bool = true
    var dataManager = DataManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataManager.load()
        
        for (index, dataModel) in dataManager.dataArray.enumerated() {
            //UI setup
            let fruitPage = Bundle.main.loadNibNamed("FruitView", owner: self, options: nil)![0] as! FruitView
            fruitPage.tag = index
            fruitPage.parentViewController = self
            scrollView.addSubview(fruitPage)
            
            //Data handling & setup
            fruitPage.dataModel = dataModel
        }
        
        scrollView.backgroundColor = UIColor.red
        scrollView.isPagingEnabled = true
        scrollView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        dataManager.save()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for subview in scrollView.subviews {
            subview.frame = CGRect(x:CGFloat(subview.tag)*scrollView.frame.width,
                                   y:0,
                                   width:scrollView.frame.width,
                                   height:scrollView.frame.height)
        }
        
        scrollView.contentSize = CGSize(width:CGFloat(Fruit.allValues.count) * scrollView.frame.width, height:scrollView.frame.height)
        
        if(isJustLoaded == true) {
            isJustLoaded = false
            let lastPage = UserDefaults.standard.integer(forKey: "savedPage")
            if(lastPage != 0) {
                scrollView.contentOffset = CGPoint(x:CGFloat(lastPage)*scrollView.frame.size.width, y:0)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func openCommentViewController(page : Int) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let commentVC = storyboard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        commentVC.dataModel = dataManager.dataArray[page]
        self.present(commentVC, animated: true) {  }
 
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let page = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
        UserDefaults.standard.set(page, forKey: "savedPage")
    }
}

