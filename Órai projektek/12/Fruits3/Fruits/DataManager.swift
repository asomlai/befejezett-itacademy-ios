//
//  DataManager.swift
//  Fruits
//
//  Created by Andras Somlai on 2018. 04. 30..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class DataManager: NSObject {
    var dataArray : [FruitViewModel] = []
    
    func load() {
        if(UserDefaults.standard.array(forKey: "savedFruitData") == nil) {
            //setup array first time
            for fruit in Fruit.allValues {
                let dataModel = FruitViewModel()
                dataModel.fruit = fruit
                dataArray.append(dataModel)
            }
        }
        else {
            //load from defaults
            let loadedArray = UserDefaults.standard.array(forKey: "savedFruitData") as! [[String : String]]
            for dict in loadedArray {
                let loadedModel = FruitViewModel(dictionary: dict)
                dataArray.append(loadedModel)
            }
        }
    }
    
    func save() {
        var arrayToSave : [[String : String]] = []
        for data in dataArray {
            let dict = data.getAsDictionary()
            arrayToSave.append(dict)
        }
        
        UserDefaults.standard.set(arrayToSave, forKey: "savedFruitData")
    }
}
