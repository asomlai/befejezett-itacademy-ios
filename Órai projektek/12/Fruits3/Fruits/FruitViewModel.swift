//
//  FruitViewModel.swift
//  Fruits
//
//  Created by Andras Somlai on 2018. 04. 29..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class FruitViewModel {
    var rating : Rating = .like
    var often : Often = .year
    var fruit : Fruit = .unknown
    var comment : String = ""
    
    init() {}
    
    init(dictionary : [String : String]) {
        rating = Rating(rawValue: dictionary["rating"]!)!
        often = Often(rawValue: dictionary["often"]!)!
        fruit = Fruit(rawValue: dictionary["fruit"]!)!
        comment = dictionary["comment"]!
    }
    
    func getAsDictionary() -> [String : String] {
        var dict : [String : String] = [:]
        dict["rating"] = rating.rawValue
        dict["often"] = often.rawValue
        dict["fruit"] = fruit.rawValue
        dict["comment"] = comment
        
        return dict
    }
}
