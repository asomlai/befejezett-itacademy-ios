//: Playground - noun: a place where people can play

/*
 Sziasztok, az első alkalommal felmerülő kérdésekre mind igyekszek választ adni ebben a kis projektben.
 A többsoros kommentelés a /**/ karakterek közé írva lehetséges. A hosszabb magyarázatokat így írom le, a rövideket csak a standard // karakterekkel
 
 A debugPrint-ek mindenhol csak a kiírást szolgálják. A lényegi rész mindig a () zárójelek között van.
 */

import UIKit


debugPrint("EGYSZERŰ TÍPUSOK, VÁLTOZÓK LÉTREHOZÁSA ÉS HASZNÁLATA, RAJTUK VÉGEZHETŐ MŰVELETEK")

//Változók létrehozása egyszerűen
var name = "John Appleseed"
var age = 34
var weight = 75.4
var happy = true

//Változók típusának kiírása (csak személtetés céljából)
debugPrint(type(of: name))
debugPrint(type(of: age))
debugPrint(type(of: weight))
debugPrint(type(of: happy))

/*
 A fordító felismeri, hogy milyen típusú érték van az = jel jobb oldalán, és automatikusan ennek megfelelő típusúként kezeli a változókat. Ennek megfelelően így alakulnak a változók típusai:
 name: String
 age: Int (Integer rövidítése)
 weight: Double
 happy: Bool (Boolean rövidítése)
 
 Ezek egyszerű esetek voltak, itt a fordító meg tudta állapítani, hogy az adott változó milyen típusú
 Van azonban olyan eset, amikor szükséges, hogy mi adjuk meg, hogy milyen típusú legyen a változónk
*/

//Float típusú változó létrehozása. Ez is tört szám (azaz nem egész), mint a Double
var exactHeight : Float = 75.4

//exactHeight típusának kiírása
debugPrint(type(of: exactHeight))

/*
 Ha tört számot adunk értékül egy változónak típus megadása nélkül, akkor az alapból Double lesz
Ha azt akarjuk, hogy Float legyen, akkor meg kell adjuk a típust
Float és Double típusok közti különbség: A Double több tizedesjegy pontossággal tudja ábrázolni a számokat.
Gyakorlatban ez azt jelenti, hogy ha egy Double típusú változó 15 tizedesjegy pontossággal tárol számokat, a Float pedig "csak" 6 tizedesjeggyel
 Az Apple ajánlása szerint inkább Double-t ajánlott használni
 Bővebb info:
 https://teamtreehouse.com/community/what-is-the-difference-between-a-float-and-a-double
 https://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/TheBasics.html#//apple_ref/doc/uid/TP40014097-CH5-ID321
*/



debugPrint("---------------------------------------------------")
debugPrint("CASTOLÁS, FELTÉTELES KIFEJEZÉSEK, LOGIKAI MŰVELETEK")

/*
 A logikai műveletkben nem csak Bool típusú változók szerepelhetnek, viszont az eredményük mindig true/false lesz.
 Például ha egy számot összehasonlítunk egy másikkal, akkor 2 szám az operandus.
 */

var numberOfDogs : Int = 13 //Nem kötelező megadni, hogy Int típusú a változó, de meg lehet adni
var numberOfCats = 27 //Ez is Int típusú változó. Itt nem adtam meg típust, hagytam hogy a fordító találja ki.

var averageWeightOfDogs = 34.5
var averageWeightOfCats = 4.6

var dogsLikeCats = true
var catsLikeDogs = false
var areAnimalsFree = true

//Miután az alap változókat felvettem, ezekből további értékeket számolok ki
var sumWeightOfDogs = Double(numberOfDogs) * averageWeightOfDogs
var sumWeightOfCats = Double(numberOfCats) * averageWeightOfCats

/*
 Miért Double(numberOfDogs), és nem numberOfDogs van a szorzás bal oldalán?
 MERT:
 A Swift nem teszi lehetővé, hogy két különböző típusú számmal műveletet végezzek, ezért meg kell adnom a fordítónak, hogy az Int típusú numberOfDogs változót értelmezze Double-ként. A fordító errort jelezne, ha ezt nem tenném.
 Ezt castolásnak hívják. Átcastoltam Doublelé az Int típusú numberOfDogs-ot.
 Ez bizonyos esetekben adatvesztést is okozhat. Pl Ha Double-t castolok Intté
 */
debugPrint("numberOfDogs mint Int típusú változó: ", numberOfDogs) //nem castoltam
debugPrint("numberOfDogs mint Double típusú változó: ", Double(numberOfDogs)) //castoltam adatvesztés nélkül
debugPrint("averageWeightOfDogs, mint Int típusú változó", Int(averageWeightOfDogs)) //castoltam, és emiatt adatot is vesztettem
debugPrint("averageWeightOfDogs, mint Double típusú változó", averageWeightOfDogs) //nem castoltam

//Az if szintaktikája:
/*
 if(logikai kifejezés) {
    Programkód, ami csak akkor fut le, ha a logikai kifejezés igaz
 }
 */

//Mivel catsLikeDogs és dogsLikeCats Bool típusú változók, önálló logikai értékkel, ezért használhatók önmbagukban feltételként.
if(catsLikeDogs) {
    debugPrint("A macskák szeretik a kutyákat")
}

if(dogsLikeCats) {
    debugPrint("A kutyák szeretik a macskákat")
}

if(dogsLikeCats && !catsLikeDogs) {
    debugPrint("A kutyák szeretik a macskákat és a macskák nem szeretik a kutyákat")
}

//numberOfCats és numberOfDogs önmagukban számok, az viszont, hogy az egyik szám nagyobb, mint a másik már egy logikai kifejezés. Vagy igaz, vagy hamis, tehát lehet az if feltétele
if(numberOfCats > numberOfDogs) {
    debugPrint("A macskák többen vannak, mint a kutyák")
}

//==    --->Egyenlőség vizsgálat.Igaz, ha a két érték azonos
//!=    --->Különbözőség vizsgálat. igaz, ha a két érték különböző
//<     --->Kisebb. igaz, ha a bal oldali operandus kisebb, mint a jobb
//>     --->Nagyobb. igaz, ha a bal oldali operandus nagyobb, mint a jobb
//<=    --->Kisebb-egyenlo. igaz, ha a bal oldali operandus kisebb-egyenlő, mint a jobb
//>=    --->Nagyobb-egyenlő. igaz, ha a bal oldali operandus nagyobb-egyenlő, mint a jobb

/*Összetettebb logikai kifejezés, ami két fő részből áll:
 (sumWeightOfCats < sumWeightOfDogs) : a macskák össztömege kisebb a kutyákénál -> ez true
 !catsLikeDogs : a macskák nem szeretik a kutyákat -> ez true
 Mivel a két kifejezés között (AND) && van, ezért a teljes kifejezés csak akkor igaz, ha a mindkét állítás igaz. (első ÉS második állítás)
 A kifejezés tehát igaz, belépünk az if törzsébe.
 */
if ((sumWeightOfCats < sumWeightOfDogs) && !catsLikeDogs) {
    debugPrint("A macskák hiába nem szeretik a kutyákat, nem fogják bántani őket, mert sokkal kisebbek")
}

//Az if-ek egymásba is ágyazhatók. Tetszőlegesen sokszor
if (areAnimalsFree) {
    debugPrint("Az állatok szabadok")
    if(numberOfDogs < numberOfCats) {
        debugPrint("Több szabadon élő macska van, mint kutya")
    }
}

//Ha komplexebb esetet vizsgálunk, akkor kerülnek képbe az else if, és else ágak is.
//A debugPrint üzenetek leírják, hogy melyik ágba mikor lép be a program
if (areAnimalsFree && (averageWeightOfCats > averageWeightOfDogs)) {
    debugPrint("Az állatok szabadon élnek és a macskák átlagosan nehezebbek, mint a kutyák")
}
else if(areAnimalsFree && (averageWeightOfCats < averageWeightOfDogs)) {
    debugPrint("Az állatok szabadon élnek és a kutyák átlagosan nehezebbek, mint a macskák")
}
else {
    debugPrint("Az állatok nem élnek szabadon, és/vagy azonos az átlagos testsúlyuk")
}

/*
 Egyszerű magyarázat:
 
 if(Ha egy adott feltétel teljesül) {
    történjen ez
 }
 else if(Ha az első feltétel nem teljesült, és ez a feltétel teljesül) {
    történjen ez
 }
 else if(Ha az első feltételek nem teljesültek, és ez a feltétel teljesül) {
    történjen ez
 }
 else {
    itt nincs feltétel. Ha egyik korébbi feltétel se teljesül, ez a programrész fut le
 }
 */

//if-else if-else esetén mindig CSAK az első olyan kódrészlet fut le, ahol a zárójelbe () írt feltétel teljesül
if(!areAnimalsFree) {
    debugPrint("Annyit tudunk biztosan, hogy az állatok nem szabadon élnek")
}
else if(dogsLikeCats) {
    debugPrint("Annyit tudunk biztosan, hogy az állatok szabadon élnek, és a kutyák szeretik a macskákat")
}
else if(!catsLikeDogs) {
    debugPrint("Amit tudunk: az állatok szabadok, a kutyák nem szeretik a macskákat, és a macskák sem szeretik a kutyákat")
}
else {
    debugPrint("Amit tudunk: az állatok szabadok, a kutyák nem szeretik a macskákat, de a macskák szeretik a kutyákat")
}



debugPrint("---------------------------------------------------")
debugPrint("TÖMB - ARRAY")

//Szintaktikailag itt az a fontos, hogy szögletes zárójelek közé legyenek betéve az értékek vesszővel elválsztva
//Az elemekre az indexeikkel hivatkozunk (hányadik a sorban)
//Az elemek sorrendje számít
//A tömb első eleme van a NULLADIK indexen

//Integereket tartalmazó tömb létrehozása
var numbers = [7,34,64,4,3,74,7876674]

//Bool értékeket tartalmazó tömb
var booleans = [true, false, true, true]

//n-edik elem lekérdezése, ahol 0 <= n <= tömbméret-1
debugPrint(numbers[0])
//debugPrint(numbers[7]) 7 elemű a tömb, nullától indexelve a legutolsó elem indexe 6. Ez így hibás. Ezt a hibát TÚLINDEXELÉSnek hívják
debugPrint(booleans[1])

//n-edik elem módosítása
debugPrint("numbers tömb 6. indexű elem módosítása előtt", numbers)
numbers[6] = 9
debugPrint("numbers tömb 6. indexű elem módosítása után", numbers)

//Új elem hozzáadása a tömbhöz:
numbers.append(56)
booleans.append(false)

//Tömb méretének lekérdezése
debugPrint(numbers.count)
debugPrint(booleans.count)

//Annak vizsgálata, hogy tartalmaz-e a tömb egy bizonyos elemet:
debugPrint(numbers.contains(57))
debugPrint(booleans.contains(false))

//Elem eltávolítása a tömbből
debugPrint("numbers tömb 2. indexű elem eltávolítása előtt", numbers)
numbers.remove(at: 2)
debugPrint("numbers tömb 2. indexű elem eltávolítása után", numbers)

//FONTOS!
//Mi van akkor, ha üres tömböt szeretnénk létrehozni, hogy később elemeket adjunk hozzá?
//A tömböt létre kell hozni üresen, és meg kell határozni, hogy milyen elemek lesznek benne.

//Magyarázat:
//[Int] ---> ugyanolyan típus jelölés, mint a Double, vagy Bool. Ez egy tömböt jelöl, amiben Int típusú elemek vannak
//= [] ---> ez egy ugyanolyan értékadás, mint a többi esetben. Az érték itt egy üres tömb, amit [] jelöl
var emptyNumbersArray : [Int] = []


debugPrint("---------------------------------------------------")
debugPrint("SZÓTÁR - DICTIONARY")
//Szintaktikailag itt az a fontos, hogy szögletes számok közé legyenek betéve a kulcs-érték párok. A kulcs és érték közé kettőspontot kell tenni, a párok közé vesszőt.
//Az elemek sorrendje nem számít
//Az elemekre a kulcsaikkal hivatkozunk

//String kulcsokat és String értékeket tartalmazó dictionary
var currencyNames = ["HUF" : "Hungarian Forint", "USD" : "USA Dollar", "EUR" : "Euro", "CNY" : "Chinese YuaN"]

//Int kulcsokat és Double érétkeket tartalmazó szótár
var eurHufExchangeRates = [2015 : 287.2, 2016 : 291.6, 2017 : 304.8]

//Érék lekérése kulcs alapján
//A szögletes zárójelbe a kulcsot írjuk, aminek típusa attól függ, hogy hogy hoztuk létre a dictionaryt
debugPrint(currencyNames["HUF"])
debugPrint(eurHufExchangeRates[2016])

//A kulcs, ami alapján keresek benne lehet egy változóban is, így az alábbi érték lekérések is helyesek
var hufCurrencyShortName = "HUF"
debugPrint(currencyNames[hufCurrencyShortName])

var year = 2016
debugPrint(eurHufExchangeRates[year])

//Adott kucshoz tartozó érték módosítása
debugPrint("currencyNames módosítás előtt: ", currencyNames)
currencyNames["CNY"] = "Chinese Yuan"
debugPrint("currencyNames módosítás után: ", currencyNames)

debugPrint("eurHufExchangeRates módosítás előtt: ", eurHufExchangeRates)
eurHufExchangeRates[year] = 296.1
debugPrint("eurHufExchangeRates módosítás után: ", eurHufExchangeRates)

//Új elem hozzáadása
//ÉRDEKES: Ha nézitek a logot, a BND nem az utolsó elemeként lesz kiírva a currencyNames-nek. A soorend a szótáraknál nem számít!
currencyNames["BND"] = "Brunei Dollar"
debugPrint("currencyNames új elem hozzáadása után:", currencyNames)

//Szótár méretének lekérdezése
debugPrint(currencyNames.count)
debugPrint(eurHufExchangeRates.count)

//Annak vizsgálata, hogy tartalmaz-e a tömb egy bizonyos kulcsú:
currencyNames["BND"] != nil
eurHufExchangeRates[2018] != nil
//MEGJEGYZÉS a nil-hez kapcsolódóan: a nil a nullpointert jelenti. Röviden: semmit. Ha egy változó értéke nil, akkor az azt jelenti, hogy nincs értéke. A eurHufExchangeRates[2018]-nak nincs értéke, tehát a 2018-as kulcshoz nem tartozik érték a eurHufExchangeRates dictionaryban

//Elem eltávolítása a szótárból
currencyNames.removeValue(forKey: "BND")
debugPrint("currencyNames BND kulcsú elem eltávolítása után:", currencyNames)

//FONTOS!
//Mi van akkor, ha üres szótárat szeretnénk létrehozni, hogy később elemeket adjunk hozzá?
//A szótárat létre kell hozni üresen, és meg kell határozni, hogy milyen típusú kulcsok és értékek lesznek benne.

//Magyarázat:
//[Int : Double] ---> ugyanolyan típus jelölés, mint a Double, vagy Bool. Ez egy tömböt jelöl, amiben Int típusú kulcsok jelölnek Double típusú értékeket
//= [:] ---> ez egy ugyanolyan értékadás, mint a többi esetben. Az érték itt egy üres szótár, amit [:] jelöl
var emptyDictionary : [Int : Double] = [:]


debugPrint("---------------------------------------------------")
debugPrint("CIKLUSOK")

//A ciklusok az ismétlődő (azonos vagy hasonló) tevékenységek megvalósítására szolgál
//For ciklius működésének vizuális bemutatása:
//https://www.youtube.com/watch?v=1jA9mPPoXu0

debugPrint("FOR ciklus:")
debugPrint("Töröld ki a // karaktereket a ciklusok törzseiben, hogy lásd konzolon a futásuk eredményét")
//Ebben a ciklusban a loopIndex egy Integer típusú változó lesz, amit csak a ciklus (loop) törzsében érünk el. A ciklus törzse az a kódrészlet, ami a {} kapcsos zárójelek között van
for loopIndex in stride(from: 0, to: 10, by: 1) {
    //debugPrint(loopIndex)
}

/*
 Magyarázat a szintaktikához:
 for futóIndex in stride(from: futóIndex kezdőértéke, to: futóindex végértéke + 1, by: a futóIndex értéke ennyivel változik minden ciklusban) {
    For ciklus törzse. Programkód, ami folyamatosan ismétlődve fut. A futóIndexet csak a {} kapcsos zárójel közötti programkódban használhatjuk
 }
*/

//Pár egyszerű for ciklus:

//loopIndex típusa: Int
//Hányszor fut le a ciklus törzsében lévő kód: 10
//loopIndex értékei első, második, harmadik stb... körben: 0,1,2,3,4,5,6,7,8,9
for loopIndex in stride(from: 0, to: 10, by: 1) {
    //debugPrint(loopIndex)
}

//loopIndex típusa: Int
//Hányszor fut le a ciklus törzsében lévő kód: 6
//loopIndex értékei első, második, harmadik stb... körben: 5,4,3,2,1,0
for loopIndex in stride(from: 5, to: -1, by: -1) {
    //debugPrint(loopIndex)
}

//loopIndex típusa: Int
//Hányszor fut le a ciklus törzsében lévő kód: 2
//loopIndex értékei első, második, harmadik stb... körben: 1,3
for loopIndex in stride(from: 1, to: 5, by: 2) {
    //debugPrint(loopIndex)
}


//loopIndex típusa: Double ---> Mivel 0.5-öt ad hozzá minden körben, ezért a loopIndex nem lehet egész szám. A fordító ezt észleli, és automatikusan Double típust használ
//Hányszor fut le a ciklus törzsében lévő kód: 8
//loopIndex értékei első, második, harmadik stb... körben: 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5
for loopIndex in stride(from: 1, to: 5, by: 0.5) {
    //debugPrint(loopIndex)
}

/*
 A
 for loopIndex in stride(from:..., to:..., by:...) {}
 helyett van lehetőségünk a
 for loopIndex in stride(from:..., through:..., by:...) {}
 használatára is. Ez annyiban különbözik az első ciklustól, hogy magába foglalja azt a számot, amit a through:-hot írsz.
 Erre is mutatok két példát
 */

//loopIndex típusa: Int
//Hányszor fut le a ciklus törzsében lévő kód: 6
//loopIndex értékei első, második, harmadik stb... körben: 0,1,2,3,4,5
for loopIndex in stride(from: 0, through: 5, by: 1) {
    //debugPrint(loopIndex)
}

//loopIndex típusa: Double ---> Mivel 0.5-öt ad hozzá minden körben, ezért a loopIndex nem lehet egész szám. A fordító ezt észleli, és automatikusan Double típust használ
//Hányszor fut le a ciklus törzsében lévő kód: 9
//loopIndex értékei első, második, harmadik stb... körben: 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 4.5, 5.0
for loopIndex in stride(from: 1, through: 5, by: 0.5) {
    //debugPrint(loopIndex)
}

debugPrint("WHILE ciklus:")
debugPrint("Töröld ki a // karaktereket a ciklusok törzseiben, hogy lásd konzolon a futásuk eredményét")

//Ez a ciklus addig fut, amíg index változó értéke kisebb, mint 10. A ciklus törzsében (a {} kapcsos zárójel közötti részben) minden körben növelem eggyel index értékét
//Emiatt a feltétel előbb-utóbb teljesülni fog. Ha a ciklus törzsében nem szerepelne az "index = index + 1" kódrészlet, akkor index értéke mindig 0 lenne, és így végtelen sokszor futna a ciklus.
//A program ilyenkor kifagy, nem fut tovább.
var index = 0
while(index < 10) {
    //debugPrint(index)
    index = index + 1
}

/*
 Magyarázat a szintaktikához:
 while(feltétel teljesül) {
    While ciklus törzse. Programkód, ami folyamatosan ismétlődve fut.
 }
 
 Az előző példában létrehoztam az index típusú változót a cikluson kívül, és azt használtam a feltétel megfogalmazásához.
 Mivel az index változó a nem ciklus törzsében lett, ezért elérhető azon kívül is.
 */

debugPrint("Az index változó éréke: \(index)") //Ez is helyes módja egy változó értékének konzolra írására
debugPrint("Az index változó éréke:", index) //Ez is helyes módja egy változó értékének konzolra írására. Konzolon nézd meg, mi a különbség a kettő között

/*Látjátok, hogy index változó értéke a program ezen pontján 10. A while ciklus addig növelgette 1-gyel az értékét, amíg 10 nem lett.
A while ciklus törzsében nincs kilogolva (azaz konzolra írva) az index, amikor 10 az értéke. Ennek az az oka, hogy amikor az index értéke 9 volt, akkor belépett a ciklus törzsébe, kiírta az értékét, és megnövelte 10-re. De amikor index értéke 10 volt, akkor már nem teljesült a ciklusfeltétel (index < 10), ezért nem lépett be, és nem írta ki a konzolra. De index értéke ekkor már 10 volt, és ciklus után is annyi maradt
 */

while (index < 10) {
    debugPrint("Ez a kódrészlet nem fog lefutni. Ez a log nem fog látszani. MIVEL index értéke 10, és 10 nem kisebb, mint 10")
}

index = 0 //index változót korábban létrehoztam. itt csak módosítom az értékét

//Ez a ciklus addig fog futni, amíg index értéke egyenlő nem lesz öttel
while (index != 5) {
    //debugPrint(index) //Ha itt van a debugPrintes sor, akkor 0-4-ig lesznek kiírva a számok
    index = index + 1
    //debugPrint(index) //Ha itt van a debugPrintes sor, akkor 1-5-ig lesznek kiírva a számok
}

debugPrint("ÓRAI FELADATOK MEGOLDÁSA")

/*
 ************************************************************************************
 1. FELADAT:
 Van egy folyosó, ahol ajtók vannak egymás után. Van olyan ajtó, ami nyitva van, és van olyan, ami be van csukva. Az ajtók nyitittságát egy Bool értékeket tartalmazó tömb tárolja.
 Menj végig a tömbön, és amelyik ajtó nyitva volt, azt csukd be, amelyik csuka volt, azt nyisd ki.
 
 Feladat programozóként megfogalmazva:
 Hozz létre egy Bool értékeket tartalmazó tömböt (röviden Bool tömböt), és az összes elem értékét negáld
 ************************************************************************************
 */

//1. megoldás:

//létrehozom a Bool tömböt, és feltöltötm tetszőleges számú értékkel
var doors1 = [true, false, true, true, true, false]

//doorIndex értékét változtatam 0-tól a doors1 tömb elemszáma -1-ig, egyesével növelve
for doorIndex in stride(from: 0, to: doors1.count, by: 1) {
    //Ebben a kódrészletben a doorIndex értéke változni fog attól függően, hogy hányadszor fut le.
    //doorIndex értékei:  0,l,2,3,4,5 -> 6x fog lefutni a ciklus törzse
    
    //a "doors1[doorIndex]" kódrészlettel lekérdezem a doors1 tömb doorIndex-edik elemét.
    if(doors1[doorIndex] == true) { //Ha az adott elem igaz (tehár az ajtó nyitva vol), akkor
        doors1[doorIndex] = false   //beállítom hamisra (azaz becsukom az ajtót)
    }
    else {                          //Egyébként (ha nem igaz, akkor biztosan hamis)
         doors1[doorIndex] = true   //beállítom igazra (azaz kinyitom az ajtót)
    }
}
debugPrint("Az ajtók miután végigmentünk rajtuk: \(doors1)")

//Ha a feladatot ciklus nélkül akarnánk megoldani, így nézne ki a megoldás:

//ROSSZ MEGOLDÁS ELEJE:
if(doors1[0] == true) {
    doors1[0] = false
}
else {
    doors1[0] = true
}

if(doors1[1] == true) {
    doors1[1] = false
}
else {
    doors1[1] = true
}

if(doors1[2] == true) {
    doors1[2] = false
}
else {
    doors1[2] = true
}

if(doors1[3] == true) {
    doors1[3] = false
}
else {
    doors1[3] = true
}

if(doors1[4] == true) {
    doors1[4] = false
}
else {
    doors1[4] = true
}

if(doors1[5] == true) {
    doors1[5] = false
}
else {
    doors1[5] = true
}
debugPrint("Az ajtók miután a rossz megoldással is végigmentünk rajtuk: \(doors1)")
//ROSSZ MEGOLDÁS VÉGE
/*
 MAGYARÁZAT:
 Ha megnézitek, pontosan ugyanazt a programkódot írtam le hatszor:
 
 if(doors1[szám] == true) {
    doors1[szám] = false
 }
 else {
    doors1[szám] = true
 }
 
 Azon kvül, hogy sokkal többet kellett gépelnem, ez a megoldás onnantól kezdve nem működik, hogy új elemeket veszek fel, vagy távolítok el a tömbből
 A for ciklus is pontosan ezt a kódrészletet futtatja, csak annyiszor, ahány elemű a tömb, és a [szám] helyére mindig változó értéket tesz.
 */

//2. megoldás:

var doors2 = [true, false, true, true, true, false]

//ugyanúgy lépked a ciklus, mint előbb, csak a törzse különbözik
for doorIndex in stride(from: 0, to: doors2.count, by: 1) {
    //Az adott indexen lévő elemet egyenlővé teszem önmaga negáltjával.
    // !true = false, !false = true
    doors2[doorIndex] = !doors2[doorIndex]
}

/*
 ************************************************************************************
 2. FELADAT:
 Hozz létre egy számokat tartalmazó tömböt, majd egy ciklus segítségével tedd be fordított sorrendben az elemeit egy másik tömbbe
 ************************************************************************************
 */

var numbersArray = [1,2,3,4,5,6,7,8,9]  //Létrehoztam a számokat tartalmazó tömböt
var invertedNumbersArray : [Int] = []    //Létrehoztam egy üres tömböt, aminél megadtam, hogy milyen típusú elemek lesznek benne. Ez egy üres tömb, amibe csak Int típusú számokat tehetek

//1. Megoldás

//A doorIndex értéke a először tömbMéret-1 lesz. A tömb utolsó elemének indexe mindig tömbméret-1, mivel a tömb 0-tól indexelődik.
//Hogy jegyezd meg: 1 elemű tömbnek 1 a tömbmérete, de az utolsó elemének indexe 0, azaz (tömbméret-1)
//elindulunk a numbersArray utolsó elemétől, és haladunk az első eleméig.
//Először az utolsó elemet adjuk hozzá az invertedNumbersArray-hoz, majd az utolsó előttit, és így tovább.
for doorIndex in stride(from: numbersArray.count-1, to: -1, by: -1) {
    invertedNumbersArray.append(numbersArray[doorIndex]) //invertedNumbersArray-hot mindig hozzáadom a numbersArray doorIndex-edik elemét.
}

debugPrint("Eredeti tömb:\(numbersArray)")
debugPrint("Megfordított tömb:\(invertedNumbersArray)")

//2. Megoldás
var numbersArray2 = [1,2,3,4,5,6,7,8,9]
var invertedNumbersArray2 : [Int] = []

//Annyi a különbség az előző megoldáshoz képest, hogy itt a stride másik verzióját használtam: stride(from:.., through:..., by:...)
//Nézzétek meg, hogy mi a különbség az indexelés között
//A through-os for a to-ssal szemben annyiban különbözik, hogy lefut akkor is, amikor a futóindex (ez esetben a doorIndex) egyenlő lesz a through-nál megadott paraméterrel
for doorIndex in stride(from: numbersArray2.count-1, through: 0, by: -1) {
    invertedNumbersArray2.append(numbersArray2[doorIndex]) //invertedNumbersArray-hot mindig hozzáadom a numbersArray doorIndex-edik elemét.
}


/*
 ************************************************************************************
 3. FELADAT:
 Fibonacci sor: 1,1,2,3,5,8,13,21,34, stb... minden eleme megegyezik az előző két elem összegével
 Az elemek sorszámát vegyük nullától. Tehát a 1. elem:1, 1. elem:1, 2. elem:2, és így tovább. Mintha tömbként kezelnénk.
 Hozz létre egy változót, ami megadja, hogy a fibonacci sor hányadik elemét írja ki a program

 ************************************************************************************
 */

//1. Megoldás
var indexOfFibonacci = 6 //Ez határozza meg, hogy a fibonacci sor negyedik elemére vagyok kíváncsi. Ha változtatom az értékét, annak is változnia kell, hogy hányadik elemet adja vissza a program

//A fibonacci sor tagjait két két értékből számoljuk: az előző kettőből. Ehhez szükséges, hogy megadjuk az első kettőnek az értékét
//A previousElement a sorozat 0. eleme, a nextElement pedig a sorozat 1. eleme
var previousElement = 1
var nextElement = 1

//elindítok egy iterációt, ami annyiszor fog lefutni, amennyi az indexOfFibonacci értéke.
for loopIndex in stride(from: 0, to: indexOfFibonacci, by: 1) {
    //Erre az elágazásra azért van szükség, hogy felváltva növelje a previousElement és nextElement értékét. Ha ez nem lenne, akkor az egyik érték mindig 1 maradna.
    //így viszont mindkét szám növekszik, és a fibonacci sorozatnak megfelelően változik az értékük
    //Kommenteljétek ki a debugprintes sorokat, hogy lássátok az értékek változását
    debugPrint("A ciklus \(loopIndex). futásának elején previousElement értéke: \(previousElement), nextElement értéke: \(nextElement)")
    if(loopIndex%2==0) {
        nextElement = previousElement + nextElement
    }
    else {
        previousElement = previousElement + nextElement
    }
    debugPrint("A ciklus \(loopIndex). futásának végén previousElement értéke: \(previousElement), nextElement értéke: \(nextElement)")
}

//A ciklus után a previousElementnek és a nextElementnek is lesz értéke. Ezekközül minket csak az egyik érték érdekel. De melyik?
//Egyik lehetséges megoldás, hogy megvizsgálod, hogy indexOfFibonacci páros-e. Mivel felváltva növeled a változók értékeit, ezért indexOfFibonacci-tól függően
//vagy nextElement, vagy previousElement értéke lesz a keresett szám.
if(indexOfFibonacci % 2 == 0) {
    debugPrint("A Fibonacci sorozat \(indexOfFibonacci). eleme: \(nextElement)")
}
else {
    debugPrint("A Fibonacci sorozat \(indexOfFibonacci). eleme: \(previousElement)")
}

//Másik lehetséges megoldás, hogy megfigyeled, hogy a két érték közül mindig a kisebbik lesz a számodra megfelelő.
//a min(A, B) értéke A és B számok közül a kisebb lesz
//a max(A, B) értéke A és B számok közül a nagyobb lesz
debugPrint("A Fibonacci sorozat \(indexOfFibonacci). eleme: \(min(previousElement, nextElement))")

//2. Megoldás
/*
 Ebben a megoldásban nem kér változót használok, amiben nyilvántartom a 2 ismert elemet, hanem egy tömböt, amiben a teljes sorozat megtalálható lesz.
 A tömböt ciklus segítségével feltöltöm adatokkal, amíg szükséges.
 A tömb létrehozásakor meg kell adnom a sor első két elemét, hogy tudja számolni a program a harmadikat, és onnantól az összes többit.
 A tömb új elemeit az tömb előző két elemének összege alkotja majd
 */
var fibonacciArray = [1,1] //Létrehozok egy tömböt, amibe beteszem a Fibonacci sorozat első két elemét
var indexOfFibonacci2 = 10 //Ugyanaz a szerepe, mint az előző megoldásnál

//Fontos, hogy a loopIndex nem 0-tól, hanem 1-től indul!
//Ez azért fontos, mert ha nullától indulna, akkor a program elején a tömb -1-edik indexű elemére hivatkoznék, ami hibás. Egy tömb első eleme a 0. indexen van. Egy ilyen hiba crasht okoz a programban!
//indexOfFibonacci2-szor fog lefutni a ciklus (kivéve, ha annak értéke 0, vagy 1, mert akkor egyszer se).
//Ezen futások alatt feltöltöm értékkel fibonacciArray tömböt
for loopIndex in stride(from: 1, to: indexOfFibonacci2, by: 1) {
    
    //A tömbhöz hozzáadandó új érték úgy adódik, hogy összeadom a fibonacciArray loopIndex-1-edik indexű és loopIndex-edik indexű elemét.
    var newFibonacciNumber = fibonacciArray[loopIndex-1] + fibonacciArray[loopIndex]
    fibonacciArray.append(newFibonacciNumber) //Miután kiszámoltam az új értéket, hozzáadom a tömbhöz
}
//Mivel a feladat csak 1 elemet kér, ezért kiveszem a tömbből a szükséges elemet: fibonacciArray[indexOfFibonacci2]
debugPrint("A Fibonacci sorozat \(indexOfFibonacci2). eleme: \(fibonacciArray[indexOfFibonacci2])")
