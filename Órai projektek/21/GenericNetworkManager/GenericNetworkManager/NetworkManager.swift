//
//  NetworkManager.swift
//  GenericNetworkManager
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import Foundation
import Alamofire

enum NetworkManagerRequest : String {
    case getPeople = "/people"
    case getClients = "/clients"
    case login = "/login"
}

class NetworkManager {
    private static let baseUrl = "http://138.197.187.213/itacademy"
    
    //MARK: - App-specific Network functions
    static func getPeople(completion : @escaping (_ data : [[String : Any]]?, _ error: Error?) -> Void) {
        let urlString = baseUrl + NetworkManagerRequest.getPeople.rawValue
        sendRequest(to: urlString, method: .get, parameters: nil, headers: nil, completion: completion)
    }
    
    static func getClients(completion : @escaping (_ data : [String : Any]?, _ error: Error?) -> Void) {
        let urlString = baseUrl + NetworkManagerRequest.getClients.rawValue
        let header = ["token" : "itacademy"]
        sendRequest(to: urlString, method: .get, parameters: nil, headers: header, completion: completion)
    }
    
    static func login(email: String, password: String, completion : @escaping (_ data : [String : Any]?, _ error: Error?) -> Void) {
        let urlString = baseUrl + NetworkManagerRequest.login.rawValue
        let parameters = ["email": email, "password": password]
        let headers = ["hardwareId" : UIDevice.current.identifierForVendor!.uuidString,
                      "os" : "IOS",
                      "osVersion" : UIDevice.current.systemVersion]
        sendRequest(to: urlString, method: .post, parameters: parameters, headers: headers, completion: completion)
    }
    
    
    //MARK: - General Networking
    private static func sendRequest<T>(
        to urlString : String,
        completion : @escaping (_ data : T?, _ error : Error?) -> Void) {
        sendRequest(to: urlString, method: .get, parameters: nil, headers:nil, completion: completion)
    }
    
    private static func sendRequest<T>(
        to urlString : String,
        method : HTTPMethod,
        parameters : [String: Any]?,
        headers : [String: String]?,
        completion : @escaping (_ data : T?, _ error : Error?) -> Void) {
        
        Alamofire.request(urlString, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            let statusCode = response.response?.statusCode
            if(statusCode == 200) {
                if let data = response.result.value as? T {
                    completion(data, nil)
                }
                else {
                    completion(nil, nil)
                }
            }
            else {
                completion(nil, response.error)
            }
        }
    }
    
}

