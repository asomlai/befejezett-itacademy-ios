//
//  Calculator.swift
//  Generics
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

enum MathematicalError : Error, LocalizedError {
    case zeroDivideError
    
    public var errorDescription: String? {
        switch self {
        case .zeroDivideError: return "Can't divide by zero"
        }
    }
}

struct Calculator {
    static func substract<T: Numeric>(a : T, b: T, c : String) -> T {
        print(c)
        return a-b
    }
    
    static func add<T: Numeric>(a : T, b: T) -> T {
        return a+b
    }
    
    static func multiply<T : Numeric>(a : T, b: T) -> T {
        return a*b
    }
    
    static func divide<T: BinaryInteger>(a : T, b: T) throws -> T {
        if(b == 0) {
            throw MathematicalError.zeroDivideError
        }
        return a/b
    }
    
    static func divide<T: FloatingPoint>(a : T, b: T) throws -> T {
        if(b == 0) {
            throw MathematicalError.zeroDivideError
        }
        return a/b
    }
}
