//
//  ViewController.swift
//  Generics
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let number : Double = 1
        
//        debugPrint(Calculator.add(a: 1, b: Float(0.1)))
//
//        debugPrint(Calculator.add(a: 1, b: Float(0.1)))
//        debugPrint(Calculator.add(a: 1, b: Float(0.1)))
//        debugPrint(Calculator.add(a: 1, b: Float(0.1)))
//        debugPrint(Calculator.add(a: 1, b: Float(0.1)))
        
        do {
            try debugPrint(Calculator.divide(a: 1, b: Float(0.0)))
        }
        catch MathematicalError.zeroDivideError {
            debugPrint("MathematicalError.zeroDivideError")
        }
        catch {
            debugPrint("unexpected error")
        }
        defer {
            debugPrint("defer")
        }
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    
    

}

