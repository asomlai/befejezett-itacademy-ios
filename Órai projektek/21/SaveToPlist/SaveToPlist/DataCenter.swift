//
//  DataCenter.swift
//  SaveToPlist
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

enum RequiredDataError: Error, LocalizedError {
    case requiredDataIsNil
    var errorDescription: String? {
        return "Error: A required variable is nil"
    }
}

class DataCenter: NSObject {
    static let sharedInstance: DataCenter = {
        let instance = DataCenter()
        return instance
    }()
    private override init() { super.init() }
    
    private(set) var humans : [Human] = []
    
    func getHumans(completion : @escaping (Bool) -> Void) {
        NetworkManager.getPeople { (jsonData, error) in
            let decoder = JSONDecoder()
            do {
                guard let json = jsonData else { throw RequiredDataError.requiredDataIsNil }
                self.humans = try decoder.decode([Human].self, from: json)
                debugPrint(self.humans)
                completion(true)
            }
            catch {
                print(error.localizedDescription)
                completion(false)
            }
            defer {
                debugPrint("Data processing finished")
            }
        }
        
    }
    
    func filePath(with fileName : String) -> URL {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filePath = documentsPath.appending("/\(fileName)")
        debugPrint("File Path: \(filePath)")
        return URL.init(fileURLWithPath: filePath)
    }
    
    //MARK: - Plist
    func saveToPlist() {
        let encoder = PropertyListEncoder()
        
        do {
            let data = try encoder.encode(humans)
            try data.write(to: filePath(with: "MyPlist.plist"))
        } catch {
            print("Error encoding notes, \(error)")
        }
    }
    
    func loadFromPlist() {
        humans.removeAll()
        
        let decoder = PropertyListDecoder()
        if let data = try? Data(contentsOf: filePath(with: "MyPlist.plist")) {
            do {
                humans = try decoder.decode([Human].self, from: data)
            } catch {
                print("Error decoding notes, \(error)")
            }
        }
        return
    }
    
    //MARK: - Defaults
    func saveToDefaults() {
        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: humans)
        UserDefaults.standard.set(encodedData, forKey: "humans")
    }
    
    func loadFromDefaults() {
        humans.removeAll()
    
        guard let loadedData = UserDefaults.standard.object(forKey: "humans") as? Data else {
            return
        }
        
        
        if let loadedHumans = NSKeyedUnarchiver.unarchiveObject(with: loadedData) as? [Human] {
            humans = loadedHumans
        }
    }
    
    //MARK: - JSON file
    func saveToJSON() {
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(humans)
            let jsonString = String(data: jsonData, encoding: .utf8)!
            try jsonString.write(to: filePath(with: "MyJSON.json"), atomically: false, encoding: .utf8)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func loadFromJSON() {
        humans.removeAll()
        do {
            let fileData = try Data(contentsOf: filePath(with: "MyJSON.json"))//try String(contentsOf: filePath(with: "MyJSON.json"), encoding: .utf8)
            let decoder = JSONDecoder()
            self.humans = try decoder.decode([Human].self, from: fileData)
        }
        catch {
            print(error.localizedDescription)
        }
    }
}
