//
//  FileSystemHandler.swift
//  SaveToPlist
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class FileSystemHandler: NSObject {
    static func createFolder(name: String) {
        let documentsPath = NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let logsPath = documentsPath.appendingPathComponent(name)
        print("create folder at path: \(logsPath)")
        do {
            try FileManager.default.createDirectory(atPath: logsPath!.path, withIntermediateDirectories: true, attributes: nil)
        } catch let error as NSError {
            NSLog("Unable to create directory \(error.debugDescription)")
        }

    }
    
    static func getDocumentsDirectoryContent() -> [String] {
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsURL, includingPropertiesForKeys: nil)
            return fileURLs.map{$0.absoluteString}
        } catch {
            print("Error while enumerating files \(documentsURL.path): \(error.localizedDescription)")
            return []
        }
    }
    
}
