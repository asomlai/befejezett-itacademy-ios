//
//  ViewController.swift
//  SaveToPlist
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var folderNameTextField: UITextField!
    @IBOutlet weak var folderContentsTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        folderNameTextField.delegate = self
        DataCenter.sharedInstance.getHumans { (success) in
            if(success) {
                //DataCenter.sharedInstance.saveToPlist()
                //DataCenter.sharedInstance.loadFromPlist()
                
                //DataCenter.sharedInstance.saveToDefaults()
                //DataCenter.sharedInstance.loadFromDefaults()
                
                DataCenter.sharedInstance.saveToJSON()
                DataCenter.sharedInstance.loadFromJSON()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func createFolder(_ sender: UIButton) {
        FileSystemHandler.createFolder(name: folderNameTextField.text!)
    }
    
    @IBAction func update(_ sender: UIButton) {
        folderContentsTextView.text = ""
        for string in FileSystemHandler.getDocumentsDirectoryContent() {
            folderContentsTextView.text = folderContentsTextView.text + "\(string) \n\n"
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}

