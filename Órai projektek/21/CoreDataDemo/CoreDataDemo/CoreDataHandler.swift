//
//  CoreDataHandler.swift
//  CoreDataDemo
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit
import CoreData

class CoreDataHandler: NSObject {

    private static func getContext() -> NSManagedObjectContext {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
    
    static func save(firstName: String, lastName: String, id: Int64, email: String, ipAddress: String, gender: String) -> Bool {
        let context = getContext()
        let entity = NSEntityDescription.entity(forEntityName: "Human", in: context)
        let managedObject = NSManagedObject(entity: entity!, insertInto: context)
        managedObject.setValue(firstName, forKey: "firstName")
        managedObject.setValue(lastName, forKey: "lastName")
        managedObject.setValue(email, forKey: "email")
        managedObject.setValue(gender, forKey: "gender")
        managedObject.setValue(id, forKey: "id")
        managedObject.setValue(ipAddress, forKey: "ipAddress")
        
        do {
            try context.save()
            return true
        }
        catch {
            return false
        }
    }
    
    static func fetchHumans() -> [Human]? {
        let context = getContext()
        do {
            return try context.fetch(Human.fetchRequest())
        }
        catch {
            return nil
        }
    }
}
