//
//  Human+CoreDataProperties.swift
//  CoreDataDemo
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. ITAcademy. All rights reserved.
//
//

import Foundation
import CoreData


extension Human {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Human> {
        return NSFetchRequest<Human>(entityName: "Human")
    }

    @NSManaged public var id: Int64
    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var email: String?
    @NSManaged public var ipAddress: String?
    @NSManaged public var gender: String?

}
