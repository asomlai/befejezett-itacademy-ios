//
//  ViewController.swift
//  CoreDataDemo
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

//Forrás: https://www.youtube.com/watch?v=cL68k-2yINY
//További részek
//2: https://www.youtube.com/watch?v=3b8P44XdwkQ
//3: https://www.youtube.com/watch?v=Xnqk9nVeU1E
class ViewController: UIViewController {

    var humans : [Human]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if( CoreDataHandler.save(firstName: "Lajos",
                             lastName: "Henrik",
                             id: 12,
                             email: "Henrik@gmail.com",
                             ipAddress: "123.223.123.1",
                             gender: "Male")) {
            humans = CoreDataHandler.fetchHumans()
            debugPrint(humans?.count)
        }
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

