//
//  Extensions.swift
//  ExtensionDemo
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

extension Int {

    var cgFloat : CGFloat {
        return CGFloat(self)
    }
    
    var double : Double {
        return Double(self)
    }
    
    var string : String {
        return String(self)
    }
    
    
    
    func prettyPrint() {
        var stringNumber = ""
        switch self {
        case 0: stringNumber = "nulla"
        case 1: stringNumber = "egy"
        case 2: stringNumber = "kettő"
        case 3: stringNumber = "három"
        case 4: stringNumber = "négy"
        default:  stringNumber = "nagyobb, mint 4, vagy negatív"
        }
        debugPrint(stringNumber)
    }
}

extension UIScrollView {
    var currentPage : Int {
        return Int(0.5 + (contentOffset.x / frame.size.width))
    }
    
    func scrollToNextPage(animated: Bool) {
        scroll(to: currentPage+1, animated: animated)
    }
    
    func scrollToPreviousPage(animated: Bool) {
        scroll(to: currentPage-1, animated: animated)
    }
    
    func scroll(to page : Int, animated: Bool) {
        if((page < 0) || (contentSize.width < (page+1).cgFloat * frame.width)) {
            return
        }
        setContentOffset(CGPoint(x:page.cgFloat * frame.width, y:0), animated: animated)
    }
}

extension UIView {
    var width : CGFloat {
        return frame.width
    }
    var height : CGFloat {
        return frame.height
    }
    
    
}

extension UIDevice {
    var iPhoneX: Bool {
        return UIScreen.main.nativeBounds.height == 2436
    }
    var iPhone: Bool {
        return UIDevice.current.userInterfaceIdiom == .phone
    }
    enum ScreenType: String {
        case iPhone4_4S = "iPhone 4 or iPhone 4S"
        case iPhones_5_5s_5c_SE = "iPhone 5, iPhone 5s, iPhone 5c or iPhone SE"
        case iPhones_6_6s_7_8 = "iPhone 6, iPhone 6S, iPhone 7 or iPhone 8"
        case iPhones_6Plus_6sPlus_7Plus_8Plus = "iPhone 6 Plus, iPhone 6S Plus, iPhone 7 Plus or iPhone 8 Plus"
        case iPhoneX = "iPhone X"
        case unknown
    }
    var screenType: ScreenType {
        switch UIScreen.main.nativeBounds.height {
        case 960:
            return .iPhone4_4S
        case 1136:
            return .iPhones_5_5s_5c_SE
        case 1334:
            return .iPhones_6_6s_7_8
        case 1920, 2208:
            return .iPhones_6Plus_6sPlus_7Plus_8Plus
        case 2436:
            return .iPhoneX
        default:
            return .unknown
        }
    }
}
