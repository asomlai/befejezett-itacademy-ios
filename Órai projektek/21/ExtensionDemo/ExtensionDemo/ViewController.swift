//
//  ViewController.swift
//  ExtensionDemo
//
//  Created by Andras Somlai on 2018. 05. 31..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewWillAppear(_ animated: Bool) {
        
        var number : Int = 32
        number.cgFloat
        
        super.viewWillAppear(animated)
        for i in 0...10 {
            let label = UILabel()
            label.text = i.string
            label.frame = CGRect(x:i.cgFloat * scrollView.width + 10, y:0, width:scrollView.width-20, height:scrollView.height)
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
            label.textAlignment = .center
            scrollView.addSubview(label)
        }
        scrollView.contentSize = CGSize(width: scrollView.frame.width * 11,
                                        height: scrollView.frame.height)
        scrollView.isPagingEnabled = true
    }
    @IBAction func previous(_ sender: UIButton) {
        scrollView.scrollToPreviousPage(animated: true)
    }
    
    @IBAction func next(_ sender: UIButton) {
        scrollView.scrollToNextPage(animated: true)
    }
    
    func device() {
        switch UIDevice.current.screenType {
        case .iPhone4_4S: break
        case .iPhones_5_5s_5c_SE: break
        case .iPhoneX: break
        default: break
            
        }
    }
}

