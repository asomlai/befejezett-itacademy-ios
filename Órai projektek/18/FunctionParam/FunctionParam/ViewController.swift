//
//  ViewController.swift
//  FunctionParam
//
//  Created by Andras Somlai on 2018. 05. 22..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var operand1Field: UITextField!
    @IBOutlet weak var operand2Field: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        operand1Field.delegate = self
        operand2Field.delegate = self
        picker.selectRow(0, inComponent: 0, animated: false)
        calculateResult()
    }
    
    //MARK: - Calculation
    func calculateResult() {
        guard let operand1 = Double(operand1Field.text!) else {noResult();return}
        guard let operand2 = Double(operand2Field.text!) else {
            noResult()
            return
        }
        
        let operation = getOperationBy(row: picker.selectedRow(inComponent: 0))!
        
        let result = doOperation(operand1: operand1, operand2: operand2, operation: operation)
        resultLabel.text = "\(result)"
    }
    
    func doOperation(operand1: Double, operand2: Double, operation: (Double, Double) -> Double) -> Double {
        return operation(operand1, operand2)
    }
    
    func noResult() {
        resultLabel.text = "?"
    }
    
    //MARK: - Operations
    func add(a : Double, b: Double) -> Double {
        return a+b
    }
    
    func substract(a : Double, b: Double) -> Double {
        return a-b
    }
    
    func multiply(a : Double, b: Double) -> Double {
        return a*b
    }
    
    func divide(a : Double, b: Double) -> Double {
        return a/b
    }
    
    //MARK: - UIPickerView Delegate & Datasource
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 4
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return getOperationString(by: row)
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        calculateResult()
    }
    
    
    //MARK: - UITextFieldDelegate
    func textFieldDidEndEditing(_ textField: UITextField) {
        calculateResult()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: - Helpers
    func getOperationBy(row: Int) -> ((Double, Double) -> Double)? {
        switch row {
        case 0: return add
        case 1: return substract
        case 2: return multiply
        case 3: return divide
        default: return nil
        }
    }
    
    func getOperationString(by row: Int) -> String? {
        switch row {
        case 0: return "+"
        case 1: return "-"
        case 2: return "*"
        case 3: return "/"
        default: return nil
        }
    }

}

