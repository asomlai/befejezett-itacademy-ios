//
//  ViewController.swift
//  AnimatedViewControllerChange
//
//  Created by Andras Somlai on 2018. 05. 22..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var buttonBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonBottomConstraint.constant = -50
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        buttonBottomConstraint.constant = 0
        UIView.animate(withDuration: 3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options: .allowUserInteraction, animations: {
            self.view.layoutIfNeeded()
        }) { (done) in
            //Nothing to do here
        }
    }

    @IBAction func showNextVC(_ sender: UIButton) {
        buttonBottomConstraint.constant = -50
        UIView.animate(withDuration: 3, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.8, options: .allowUserInteraction, animations: {
            self.view.layoutIfNeeded()
        }) { (done) in
            self.performSegue(withIdentifier: "showSecond", sender: nil)
        }
        
    }
    
}

