//
//  ViewController.swift
//  AnimationFunctionParam
//
//  Created by Andras Somlai on 2018. 05. 22..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

//FUN: https://stackoverflow.com/questions/36576500/differrence-between-closure-and-function-as-argument-in-swift
class ViewController: UIViewController {

    @IBOutlet weak var centerYConstraint: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func animate(_ sender: UIButton) {
        centerYConstraint.constant = sender.tag % 2 == 0 ? 100 : 0
        sender.tag += 1
        
        //Megoldás 1
        /*
        UIView.animate(withDuration: 1, animations: {
            self.view.layoutIfNeeded()
        }) { (done) in
            let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        */
        
        //Megoldás 2
        UIView.animate(withDuration: 1, animations: animations, completion: doAfterAnimation)
    }
    
    func animations() {
        view.layoutIfNeeded()
    }
    
    func doAfterAnimation(success: Bool) {
        if(!success) {
            return
        }
        let alert = UIAlertController(title: "Alert", message: "Message", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func showAlert(_ sender: UIButton) {
        let alert = UIAlertController(title: "Válassz", message: "mit csináljak?", preferredStyle: UIAlertControllerStyle.alert)
        
        let changeBackgroundColorAction1 = UIAlertAction(title: "change to white",style: .default, handler: changeBackgoround)
        
        let changeBackgroundColorAction3 = UIAlertAction(title: "ddd", style: .default) { (action) in
            self.view.backgroundColor = action.style == .destructive ? UIColor.red : UIColor.white
        }
        
        let changeBackgroundColorAction2 = UIAlertAction(title: "change to red",style: .destructive, handler: changeBackgoround)
        let noAction = UIAlertAction(title: "nothing",style: .cancel, handler: showSadAlert)
        
        alert.addAction(changeBackgroundColorAction1)
        alert.addAction(changeBackgroundColorAction2)
        alert.addAction(noAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func changeBackgoround(action : UIAlertAction) {
        view.backgroundColor = action.style == .destructive ? UIColor.red : UIColor.white
    }
    
    func showSadAlert(action : UIAlertAction) {
        let alert = UIAlertController(title: ":(", message: nil, preferredStyle: UIAlertControllerStyle.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}

