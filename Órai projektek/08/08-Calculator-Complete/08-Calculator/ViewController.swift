//
//  ViewController.swift
//  08-Calculator
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var addition1: UITextField!      //összeadás jobb oldali textbox
    @IBOutlet weak var addition2: UITextField!      //összeadás, bal oldali textbox
    @IBOutlet weak var additionResultLabel: UILabel!//Összadás eredmény label
    
    @IBOutlet weak var substraction1: UITextField!
    @IBOutlet weak var substraction2: UITextField!
    @IBOutlet weak var substractionResultLabel: UILabel!
    
    @IBOutlet weak var multiplication1: UITextField!
    @IBOutlet weak var multiplication2: UITextField!
    @IBOutlet weak var multiplicationResultLabel: UILabel!
    
    @IBOutlet weak var division1: UITextField!
    @IBOutlet weak var division2: UITextField!
    @IBOutlet weak var divisionResultLabel: UILabel!
    
    @IBOutlet weak var modulo1: UITextField!
    @IBOutlet weak var modulo2: UITextField!
    @IBOutlet weak var moduloResultLabel: UILabel!
    
    //Ez a függvény híódik meg amikor betölt a képernyő
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Háttérre tappolás érzékeléséhez kapcsolódó programkód
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
        
        additionResultLabel.text = "-"
        substractionResultLabel.text = "-"
        multiplicationResultLabel.text = "-"
        divisionResultLabel.text = "-"
        moduloResultLabel.text = "-"
    }

    //Ez a függvény hívódik meg a Calculate gomb megnyomására
    @IBAction func calculateButtonPressed(_ sender: UIButton) {
        //addition
        //a castToNumberIfPossible függvény segítségével a szöveges típusból Double típust csinálok.
        //castToNumberIfPossible visszatérési értéke lehet nil akkor, ha a paraméter string nem helyes számokat tartalmaz
        let add1 = Calculator.castToDoubleIfPossible(doubleStringNumber: addition1.text)
        let add2 = Calculator.castToDoubleIfPossible(doubleStringNumber: addition2.text)
        //Ha mindkét operandus valós szám, és mindkettőnek van értéke, akkor végezhető el a művelet
        if(add1 != nil && add2 != nil) {
            //resultba bekerül a számítás eredménye
            let result = Calculator.addition(a: add1!, b: add2!)
            //kiírom a felhasználói felületre az eredményt
            additionResultLabel.text = String(result)
        }
        //Ha helytelen az input
        else {
            //A felhasználói felület kapcsolódó részét ennek megfelelően módosítom
            additionResultLabel.text = "?"  //eredmény labelbe ? karaktert
            addition1.text = nil            //operanduson textfieldjeit pedig ürítem
            addition2.text = nil
        }
        
        //substraction
        let sub1 = Calculator.castToDoubleIfPossible(doubleStringNumber: substraction1.text)
        let sub2 = Calculator.castToDoubleIfPossible(doubleStringNumber: substraction2.text)
        if(sub1 != nil && sub2 != nil) {
            let result = Calculator.substraction(a: sub1!, b: sub2!)
            substractionResultLabel.text = String(result)
        }
        else {
            substractionResultLabel.text = "?"
            substraction1.text = nil
            substraction2.text = nil
        }
        
        //multiplication
        let mult1 = Calculator.castToDoubleIfPossible(doubleStringNumber: multiplication1.text)
        let mult2 = Calculator.castToDoubleIfPossible(doubleStringNumber: multiplication2.text)
        if(mult1 != nil && mult2 != nil) {
            let result = Calculator.multiplication(a: mult1!, b: mult2!)
            multiplicationResultLabel.text = String(result)
        }
        else {
            multiplicationResultLabel.text = "?"
            multiplication1.text = nil
            multiplication2.text = nil
        }
        
        //division
        let div1 = Calculator.castToDoubleIfPossible(doubleStringNumber: division1.text)
        let div2 = Calculator.castToDoubleIfPossible(doubleStringNumber: division2.text)
        if(div1 != nil && div2 != nil) {
            let result = Calculator.division(a: div1!, b: div2!)
            divisionResultLabel.text = String(result)
        }
        else {
            divisionResultLabel.text = "?"
            division1.text = nil
            division2.text = nil
        }
        
        //modulo:
        let mod1 = Calculator.castToIntIfPossible(intStringNumber: modulo1.text)
        let mod2 = Calculator.castToIntIfPossible(intStringNumber: modulo2.text)
        if(mod1 != nil && mod2 != nil) {
            let result = Calculator.modulo(a: mod1!, b: mod2!)
            moduloResultLabel.text = String(result)
            modulo1.text = String(mod1!)
            modulo2.text = String(mod2!)
        }
        else {
            moduloResultLabel.text = "?"
            modulo1.text = nil
            modulo2.text = nil
        }
        
    }
    @IBAction func clear(_ sender: UIButton) {
        for subview in view.subviews {
            if let textField = subview as? UITextField {
                textField.text = nil
            }
        }
        additionResultLabel.text = "-"
        substractionResultLabel.text = "-"
        multiplicationResultLabel.text = "-"
        divisionResultLabel.text = "-"
        moduloResultLabel.text = "-"
    }
    
    //Ez a függvény hívódik meg, ha a háttérre tappoltok
    @objc func hideKeyboard() {
        self.view.endEditing(true) //Billentyűzet eltüntetése
    }
}

