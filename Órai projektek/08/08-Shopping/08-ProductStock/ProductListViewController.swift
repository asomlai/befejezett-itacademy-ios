//
//  ProductListViewController.swift
//  08-ProductStock
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ProductListViewController: UIViewController {
    @IBOutlet weak var quantitySelectorSlider: UISlider!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var productListTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ÍGY TUDJÁTOK BEÁLLÍTANI, HOGY A SLIDER.VALUE-NAK MI LEGYEN A MINIMUM ÉS MAXIMUM ÉRTÉKE
        quantitySelectorSlider.minimumValue = 0
        quantitySelectorSlider.maximumValue = 1
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Ez a függvény hívódik meg, amikor mozog a csúszka
    @IBAction func quantitySelected(_ sender: UISlider) {
        debugPrint(sender.value)
        debugPrint(quantitySelectorSlider.value)
    }
}
