//
//  ViewController.swift
//  08-ProductStock
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet weak var totalValueLabel: UILabel!
    @IBOutlet weak var numberOfProductsLabel: UILabel!
    
    //Ez a függvény csak akkor hívodik meg, amikor a képernyő betöltődött
    //Nem hívódik meg, amikor a back gomb hatására láthatóvá válik
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //Ez a függvény mindig meghívódik mielőtt a képernyő látható lesz
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Ez a függvény hívódik meg, amikor a valutát választott a felhasználó
    @IBAction func currencySelected(_ sender: UISegmentedControl) {
        debugPrint(sender.selectedSegmentIndex)
    }
    
}

