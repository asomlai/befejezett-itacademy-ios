//
//  AddProductViewController.swift
//  08-ProductStock
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class AddProductViewController: UIViewController {
    
    @IBOutlet weak var productNameInputField: UITextField!  //termék neve text input property
    @IBOutlet weak var productPriceTextField: UITextField!  //termék ára text input property
    @IBOutlet weak var currencySelector: UISegmentedControl!//valut kiválasztó property
    @IBOutlet weak var quantiyFeedbackLabel: UILabel!       //Mennyiség label property
    @IBOutlet weak var quantityStepper: UIStepper!          //A +/- kontroll propertyje
    @IBOutlet weak var canBuySwitchOutlet: UISwitch!        //18 alatt meg lehet-e venni a terméket switch property
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Ez a függvény hívódik meg, ha valutát választ a user
    @IBAction func currencySelected(_ sender: UISegmentedControl) {
        //Ez a két kimenet ugyanaz lesz. A sender paraméter azt jelenti, hogy melyik UI elem hívta meg a függvényt
        debugPrint(sender.selectedSegmentIndex)
        debugPrint(currencySelector.selectedSegmentIndex)
    }
    
    //Ez a függvény hívódik meg, amikor a +/- konrollt használja a user
    @IBAction func quantityChanged(_ sender: UIStepper) {
        debugPrint(sender.value)
        debugPrint(quantityStepper.value)
    }
    
    //Ez a függvény hívódik meg, ha a switc-et megnyomta a user
    @IBAction func canBuyProductSwitchValueChanged(_ sender: UISwitch) {
        //Ez a két kimenet ugyanaz lesz. A sender paraméter azt jelenti, hogy melyik UI elem hívta meg a függvényt
        debugPrint(sender.isOn)
        debugPrint(canBuySwitchOutlet.isOn)
    }
    
    //Ez a függvény hívódikmeg, ha a Termék hozzáadása gombra nyom a user
    @IBAction func addProductButtonPressed(_ sender: UIButton) {
    }

}
