//
//  Stock.swift
//  08-ProductStock
//
//  Created by Andras Somlai on 2018. 04. 12..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import Foundation

class Stock {
    //Singleton
    static let sharedInstance: Stock = {
        let instance = Stock()
        return instance
    }()
    
    //ez a property privát, hogy ne adhassak hozzá bármilyen itemet
    private var items : [StockItem] = []

    //Új item hozzáadása
    func add(item : StockItem) {
        //Csak akkor tudok a stockhoz új itemet hozzáadni, ha annak minden peopertyje meg van adva
        if(item.allDetailsProvided()) {
            items.append(item)
        }
    }
    
    //Itemek lekérdezése
    func getAllItem() -> [StockItem]  {
        return items
    }
    
    //Hozzáadott termékek számának lekérdezése
    func totalNumberOfProducts() -> Int {
        var count = 0
        //végigiterálok az itemeken, és a quantityjükkel növelem az össz termék számot
        for item in items {
            count += item.quantity
        }
        return count
    }
    
    //Termékek össz értékének lekérdezése
    func totalPriceOfProducts(currency : Currency) -> Double {
        var totalPrice = 0.0
        
        //Iterációval
        for item in items {
            //Egy item össz értéke = price * mennyiség
            totalPrice += Double(item.quantity) * item.getPrice(in: currency)
        }
        return totalPrice
    }
}
