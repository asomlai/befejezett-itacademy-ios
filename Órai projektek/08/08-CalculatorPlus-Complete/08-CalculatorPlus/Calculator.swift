//
//  Calculator.swift
//  08-CalculatorPlus
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class Calculator {
    //Ebben a propertyben fogom tárolni a korábban elvégzett műveleteket
    var history : [Operation] = []
    
    //Kiszámolja a paraméterként kapott operation eredményét.
    //Modulo esetében is Double a visszatérési érték, de garantálnotok kell, hogy egész szám lesz
    //ha a shouldSaveResult paraméter értéke true, akkor elmenti az opeartiont a memóriájába, egyébként csak kiszámolja az opeartion eredményét
    func calculateOperationResult(operation: Operation, shouldSaveResult : Bool) -> Double? {
        //Az operationType határozza meg, hogy melyik provát függvény segítségével számítom ki az eredményt
        let operationType = operation.operationType
        
        //Rövidebb kódért kiszerveztem változóba az operandusokat
        let a = operation.operand1
        let b = operation.operand2
        
        //Ebbe fogom tárolni az eredményt
        var result : Double?
        
        //Itt döntöm el, hogy reult-ot melyik függvénnyel számoljam
        switch operationType {
        case .addition: result = addition(a: a, b: b)
        case .substraction: result = substraction(a: a, b: b)
        case .multiplication:result =  multiplication(a: a, b: b)
        case .division: result = division(a: a, b: b)
        case .modulo: result = modulo(a: a, b: b)
        case .unknown: break
        }
        
        //Ha az operationType .unknown volt, akkor a result maradt nil. Nem mentem a műveletet, mert értelmetlen. Visszatérek nilel.
        if(result == nil) {
            return nil
        }
        //ha resultnak van értéke, azzal térek vissza
        else {
            //A paraméter függvényében mentem a műveletet
            if(shouldSaveResult) {
                history.append(operation)
            }
            return result
        }
    }
    
    //Visszaadja, hogy a string paraméter milyen típusú művelet elvégzését jelenti
    //példa: "1+8" -> összeadás, "3%32,23" -> modulo
    //Ha helyztelen a bemenet unknow-val tér vissza. Helytelen bemenet, ha nem két szám és 1 operandus van a string paraméterben
    //példa helytelen bemenetre: "3,14 %   2.43 % 34", "3 +-   2", "3+4+4"
    static func getOperationType(from operationString : String?) -> OperationType {
        //Ha nincs paraméter, ismeretlen az operátor
        if(operationString == nil) {
            return .unknown
        }
        
        //Számolom, hogy hány értelmes operátort találok a stringben. Csak 1 lehet!
        var numberOfFoundOperators = 0
        //Ezzel a változóval térek majd vissza. Alapból .unknown az értéke
        var lastFoundOperationType : OperationType = .unknown
        
        //Végigiterálok az operationTypeokon.
        //emiatt az iteráció miatt hoztam létre a validOperations tömböt az OperationType enumnál
        for operationType in OperationType.validOperations {
            
            //Ha a string tartalmazza az operátor karaktert
            if(operationString!.contains(operationType.rawValue)) {
                //Hányszor fordul elő az adott operator karakter a stingben
                //"3+3+3".components(separatedBy: operationType.rawValue).count értéke 3 lesz, de 3-1 db plusz opertáor van a stringben
                numberOfFoundOperators += operationString!.components(separatedBy: operationType.rawValue).count - 1
                
                //A visszatérési értékként szolgáló változóba beteszem a megtalált operátort
                lastFoundOperationType = operationType
                
            }
        }
        
        //Ha nincs operátor megadva a paramétzer stringbe, vagy 1-nél több operátor van benne megadva, akkor nem értelmezzük a műveletet
        //ismeretlen típusúnak értéküljök a műveletet
        if(numberOfFoundOperators == 0 || numberOfFoundOperators > 1) {
            return .unknown
        }
        //Ha a numberOfFoundOperators értéke 1, akkor biztosan pontosan 1 értelmes művelettípust találtunk, ezzel visszatérek
        else {
            return lastFoundOperationType
        }
    }
    
    //Számításokat végző műveletek
    private func addition(a : Double, b : Double) -> Double {
        return a+b
    }
    
    private func substraction(a : Double, b : Double) -> Double {
        return a-b
    }
    
    private func multiplication(a : Double, b : Double) -> Double {
        return a*b
    }
    
    private func division(a : Double, b : Double) -> Double {
        return a/b
    }
    
    //A visszatérési értéke bár Double típusú, mégis garantálnod kell, hogy egész szám lesz
    private func modulo(a : Double, b : Double) -> Double {
        return Double(Calculator.castToIntCorrectly(a)%Calculator.castToIntCorrectly(b))
    }
    
    //Visszaadja az operationString String típusú változóban lévő operandusokat
    //Csak akkor műkdösik, ha az opeartionTypenak megfelelő az operationString paraméter
    //Ha nem értelmezhető az operationStting, nil-lel tér vissza
    //nem értelmezhető input például: "defs%deded", "1%d", "1- ?:)"
    //Tudja helyesen kezelni a spaceket. Tehát az alábbiak helyes bemenetnek számítanak:
    //"1.34 + 2.4", "1     +     2", "45/ 432    "
    //Tudja helyesen kezelni a , és . karaktereket is, tehát: "3.13", és "3,14" számokat is értelmezni kell
    static func getOperands(from operationString: String?,  operationType : OperationType) -> (operand1 : Double, operand2 : Double)? {
        
        //ha a paraméter nil, nincsenek operandusok
        if(operationString == nil) {
            return nil
        }
        
        //A pramaéter stringből eltávolítom az összes space karaktert
        let spacesRemoved = operationString!.replacingOccurrences(of: " ", with: "")
        
        //A paraméter stringet splittelem az operátor mentén
        let separated = spacesRemoved.components(separatedBy: operationType.rawValue)
        
        //Csak akkor dolgozok tovább, ha a splittelt szöveg 2 részből áll
        if(separated.count == 2) {
            //Castolom double típusra a szövegrészleteket
            let firstOperand = castToDoubleIfPossible(doubleStringNumber: separated[0])
            let secondOperand = castToDoubleIfPossible(doubleStringNumber: separated[1])
            //Ha mindkét számmá castolás sikerült, akkor rendben van a paraméter. Visszatérekj az operanduaokkal
            if(firstOperand != nil && secondOperand != nil) {
                return (firstOperand!, secondOperand!)
            }
            //Ha valamelyik castolás nem sikerült, nillel térek vissza, mert valami rossz input van a műveletben
            else {
                return nil
            }
        }
        //ha nem 2 részre bontja a spléittlés a stringet, nillel térek vissza
        else {
            return nil
        }
    }
    
    
    //Visszaadja a doubleStringNumber String tíopusú változót Double típusban.
    //Ha nem értelmezhetpő számként a paraméter, nil-lel tér vissza
    //Tudja helyesen kezelni a , és . karaktereket is, tehát: "3.13", és "3,14" is helyes bemenet kell legyen
    static func castToDoubleIfPossible(doubleStringNumber : String?) -> Double? {
        //ha nincs paraméter, nillel térek vissza
        if (doubleStringNumber == nil) {
            return nil
        }
        //Ha van paraméter, a benne lévő vesszőket pontra cserélem, mert csak így tudja számkémnt értelmezni a stringet a swift
        let doubleString = doubleStringNumber!.replacingOccurrences(of: ",", with: ".")
        
        //Visszatérek a Double értékkel. Ez a visszatérési érték nil lesz, ha a doubleString nem értelmes számot tartalmaz. Pl: "1a12"
        return Double(doubleString)
    }
    
    //Visszaadja a doubleNumber Double tíopusú változót Int típusban.
    //A matematika szabályai szerint kerekít. (4.5)->(5), (-4.5)->(-5)
    //Segítség: Int(2.1) = 2, Int(2.999999) = 2
    static func castToIntCorrectly(_ doubleNumber : Double) -> Int {
        //Ha hozzáadok/kivopnok 0.5-öt a számból, és veszem az egész részét, úgy biztosítva vana matematilailag helyes kerekítés. Hiszen 1.5 + 0.5 egész része 2 lesz, de 1.49+0.5 1.99 lesz, aminek egész része 1
        
        //Pozitív számoknál hozzáadok 0.5-öt
        if(doubleNumber > 0) {
            return Int(doubleNumber + 0.5)
        }
        //Negatív számoknál kivonok 0.5-öt
        else {
            return Int(doubleNumber - 0.5)
        }
        
    }
    
    
}
