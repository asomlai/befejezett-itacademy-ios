//
//  ViewController.swift
//  08-CalculatorPlus
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Storyboardból bekötött propertyk
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var historyTextView: UITextView!
    let calculator = Calculator()
    
    //Ez a függvény híódik meg amikor betölt a képernyő
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Háttérre tappolás érzékeléséhez kapcsolódó programkód
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    //Ez a függvény hívódik meg a Calculate gomb megnyomására
    @IBAction func calculateButtonPressed(_ sender: UIButton) {
        //Először lekérdem a művelet típusát
        let operationType = Calculator.getOperationType(from: inputTextField.text)
        
        //Ha a művelet tíopusa ismeretlen, nem tudjuk elvégezni a számítást
        if(operationType == .unknown) {
            resultLabel.text = "?"
            return
        }
        
        //A művelet típusának ismeretében lekérem az operandusokat
        let operands = Calculator.getOperands(from: inputTextField.text, operationType: operationType)
        
        //Ha az opreandusok értéke nil, akkor nem tudom elvégezni a műveletet
        if(operands == nil) {
            resultLabel.text = "?"
            return
        }
        
        //létrehozok egy új operation objektumot
        let operation = Operation(operands: operands!, operationType: operationType, calculator: self.calculator)
        
        //resulba kiszámolom az eredményét
        let result = self.calculator.calculateOperationResult(operation: operation, shouldSaveResult: true)
        
        //kiírom az eredményt a felhasználói felületre
        resultLabel.text = String(result!)
    }
    
    //Ez a függvény hívódik meg, amikor az update history gombra nyomott a user
    @IBAction func updateHistoryButtonPressed(_ sender: UIButton) {
        //a histroyTextbe fogom gyűjteni a megjelenítendő sorokat
        var historyText = ""
        
        //végigiterálok a calculatorral elvégzett műveleteken
        for operation in self.calculator.history {
            //Kiszámolom a művelet eredményét ügyelve arra, hogy a calculator ne mentse ezeket a számításokat
            let operationResult = self.calculator.calculateOperationResult(operation: operation, shouldSaveResult: false)
            
            //hozzáfűzöm a megjeleníteni kívánt sorokhoz a műveletet, és eredményt
            historyText += "\(operation.getAsString()) = \(operationResult!)\n"
        }
        
        //A felhasználói felületen visszajelzést adok a usernek
        historyTextView.text = historyText
    }
    
    //Ez a függvény hívódik meg, amikor a clear history gombra nyomott a user
    @IBAction func clearHistoryButtonPressed(_ sender: UIButton) {
        //A felhasználói felületet frissítem
        historyTextView.text = ""
        
        //A celculatorból eltávolítom az összes eredményt
        calculator.history.removeAll()
    }
    
    //Ez a függvény hívódik meg, ha a háttérre tappoltok
    @objc func hideKeyboard() {
        self.view.endEditing(true) //Billentyűzet elrejtése
    }


}

