//
//  ViewController.swift
//  08-CalculatorPlus
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Storyboardból bekötött propertyk
    @IBOutlet weak var inputTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var historyTextView: UITextView!
    let calculator = Calculator()
    
    //Ez a függvény híódik meg amikor betölt a képernyő
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Háttérre tappolás érzékeléséhez kapcsolódó programkód. Ezzel a kódrészlettel nincs dolgotok
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
    }
    
    //Ez a függvény hívódik meg a Calculate gomb megnyomására
    @IBAction func calculateButtonPressed(_ sender: UIButton) {
        debugPrint("calculateButtonPressed")
    }
    
    //Ez a függvény hívódik meg, amikor az update history gombra nyomott a user
    @IBAction func updateHistoryButtonPressed(_ sender: UIButton) {
        debugPrint("updateHistoryButtonPressed")
    }
    
    //Ez a függvény hívódik meg, ha a háttérre tappoltok. Nem kell foglalkozz vele
    @objc func hideKeyboard() {
        self.view.endEditing(true) //Billentyűzet elrejtése
    }


}

