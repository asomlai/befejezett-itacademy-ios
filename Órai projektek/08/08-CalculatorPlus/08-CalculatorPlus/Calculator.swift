//
//  Calculator.swift
//  08-CalculatorPlus
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class Calculator {
    //Ebben a propertyben fogom tárolni a korábban elvégzett műveleteket
    var history : [Operation] = []
    
    //Kiszámolja a paraméterként kapott operation eredményét.
    //Modulo esetében is Double a visszatérési érték, de garantálnotok kell, hogy egész szám lesz
    //ha a shouldSaveResult paraméter értéke true, akkor elmenti az opeartiont a memóriájába, egyébként csak kiszámolja az opeartion eredményét
    func calculateOperationResult(operation: Operation, shouldSaveResult : Bool) -> Double? {
        return nil
    }
    
    //Visszaadja, hogy a string paraméter milyen típusú művelet elvégzését jelenti
    //példa: "1+8" -> összeadás, "3%32,23" -> modulo
    //Ha helyztelen a bemenet unknow-val tér vissza. Helytelen bemenet, ha nem két szám és 1 operandus van a string paraméterben
    //példa helytelen bemenetre: "3,14 %   2.43 % 34", "3 +-   2", "3+4+4"
    static func getOperationType(from operationString : String?) -> OperationType {
        return .unknown
    }
    
    //Számításokat végző műveletek
    private func addition(a : Double, b : Double) -> Double {
        return 0
    }
    
    private func substraction(a : Double, b : Double) -> Double {
        return 0
    }
    
    private func multiplication(a : Double, b : Double) -> Double {
        return 0
    }
    
    private func division(a : Double, b : Double) -> Double {
        return 0
    }
    
    //A visszatérési értéke bár Double típusú, mégis garantálnod kell, hogy egész szám lesz!
    private func modulo(a : Double, b : Double) -> Double {
        return 0
    }
    
    //Visszaadja az operationString String típusú változóban lévő operandusokat
    //Csak akkor műkdösik, ha az opeartionTypenak megfelelő az operationString paraméter
    //Ha nem értelmezhető az operationStting, nil-lel tér vissza
    //nem értelmezhető input például: "defs%deded", "1%d", "1- ?:)"
    //Tudja helyesen kezelni a spaceket. Tehát az alábbiak helyes bemenetnek számítanak:
    //"1.34 + 2.4", "1     +     2", "45/ 432    "
    //Tudja helyesen kezelni a , és . karaktereket is, tehát: "3.13", és "3,14" számokat is értelmezni kell
    static func getOperands(from operationString: String?,  operationType : OperationType) -> (operand1 : Double, operand2 : Double)? {
        return nil
    }
    
    
    //Visszaadja a doubleStringNumber String tíopusú változót Double típusban.
    //Ha nem értelmezhetpő számként a paraméter, nil-lel tér vissza
    //Tudja helyesen kezelni a , és . karaktereket is, tehát: "3.13", és "3,14" is helyes bemenet kell legyen
    static func castToDoubleIfPossible(doubleStringNumber : String?) -> Double? {
        return nil
    }
    
    //Visszaadja a doubleNumber Double tíopusú változót Int típusban.
    //A matematika szabályai szerint kerekít. (4.5)->(5), (-4.5)->(-5)
    //Segítség: Int(2.1) = 2, Int(2.999999) = 2
    static func castToIntCorrectly(_ doubleNumber : Double) -> Int {
        return 0
    }
    
    
}
