//
//  ViewController.swift
//  08-Calculator
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var addition1: UITextField!      //összeadás jobb oldali textbox
    @IBOutlet weak var addition2: UITextField!      //összeadás, bal oldali textbox
    @IBOutlet weak var additionResultLabel: UILabel!//Összadás eredmény label
    
    @IBOutlet weak var substraction1: UITextField!
    @IBOutlet weak var substraction2: UITextField!
    @IBOutlet weak var substractionResultLabel: UILabel!
    
    @IBOutlet weak var multiplication1: UITextField!
    @IBOutlet weak var multiplication2: UITextField!
    @IBOutlet weak var multiplicationResultLabel: UILabel!
    
    @IBOutlet weak var division1: UITextField!
    @IBOutlet weak var division2: UITextField!
    @IBOutlet weak var divisionResultLabel: UILabel!
    
    @IBOutlet weak var modulo1: UITextField!
    @IBOutlet weak var modulo2: UITextField!
    @IBOutlet weak var moduloResultLabel: UILabel!
    
    //Ez a függvény híódik meg amikor betölt a képernyő
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Háttérre tappolás érzékeléséhez kapcsolódó programkód
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
        
        additionResultLabel.text = "-"
        substractionResultLabel.text = "-"
        multiplicationResultLabel.text = "-"
        divisionResultLabel.text = "-"
        moduloResultLabel.text = "-"
    }

    //Ez a függvény hívódik meg a Calculate gomb megnyomására
    @IBAction func calculateButtonPressed(_ sender: UIButton) {
        //Így éritek el a textfieldekbe beírt szöveget
        debugPrint(addition1.text)
        debugPrint(substraction1.text)
        //stb...
    }
    
    //Ez a függvény hívódik meg, ha a háttérre tappoltok
    @objc func hideKeyboard() {
        self.view.endEditing(true) //Billentyűzet eltüntetése
    }
    
}

