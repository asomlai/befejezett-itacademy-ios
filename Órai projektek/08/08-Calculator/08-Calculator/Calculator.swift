//
//  Calculator.swift
//  08-Calculator
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class Calculator: NSObject {

    //return a + b
    static func addition(a : Double, b : Double) -> Double {
        return 0
    }
    
    //return a - b
    static func substraction(a : Double, b : Double) -> Double {
        return 0
    }
    
    //return a * b
    static func multiplication(a : Double, b : Double) -> Double {
        return 0
    }
    
    //return a / b
    static func division(a : Double, b : Double) -> Double {
        return 0
    }
    
    //return a % b
    static func modulo(a : Int, b : Int) -> Int {
        return 0
    }
    
    //Visszaadja a doubleStringNumber String tíopusú változót Double típusban.
    //Ha nem értelmezhetpő számként a paraméter, nil-lel tér vissza
    //Tudja helyesen kezelni a , és . karaktereket is, tehát: "3.13", és "3,14" is helyes bemenet kell legyen
    static func castToDoubleIfPossible(doubleStringNumber : String?) -> Double? {
        return nil
    }
    
    //Visszaadja a intStringNumber String tíopusú változót Int típusban.
    //Ha nem értelmezhetpő számként a paraméter, nil-lel tér vissza
    //Tudja helyesen kezelni azt az esetet is, ha double számot a paraméter. Ekkor a matematika szabályai szerint kerekít
    //Tehát pl ha a bemenet "3.14", a kimement 3. Ha a bemenet 3.76, akkor a kimenet 4
    //Tudja helyesen kezelni a , és . karaktereket is, tehát: "3.13", és "3,14" is helyes bemenet kell legyen
    //Segítség: Int(2.1) = 2, Int(2.999999) = 2
    static func castToIntIfPossible(intStringNumber : String?) -> Int? {
        return nil
    }
}
