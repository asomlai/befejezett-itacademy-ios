//
//  DetailsViewController.swift
//  TableViewCellDelegate
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    var textToShow : String!
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = textToShow
    }
}
