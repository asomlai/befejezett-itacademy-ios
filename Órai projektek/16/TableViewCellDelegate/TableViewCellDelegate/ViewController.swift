//
//  ViewController.swift
//  TableViewCellDelegate
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UITableViewController, CustomTableViewCellDelegate {
    var ratingText : String = "?"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        ratingText = "?"
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var customCell = tableView.dequeueReusableCell(withIdentifier: "customCell") as? CustomTableViewCell
        if(customCell == nil) {
            customCell = CustomTableViewCell(style: .default, reuseIdentifier: "customCell")
        }
        customCell!.delegate = self
        return customCell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        debugPrint("Cell tapped at row:\(indexPath.row) in section:\(indexPath.section)")
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "showDetails", sender: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }

    
    func customCellLikePressed(cell: CustomTableViewCell) {
        ratingText = ":)"
        performSegue(withIdentifier: "showDetails", sender: nil)
    }
    
    func customCellNeutralPressed(cell: CustomTableViewCell) {
        ratingText = ":|"
        performSegue(withIdentifier: "showDetails", sender: nil)
    }
    
    func customCellDislikePressed(cell: CustomTableViewCell) {
        ratingText = ":("
        performSegue(withIdentifier: "showDetails", sender: nil)
    }
 
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailsVC = segue.destination as? DetailsViewController else {
            return
        }
        detailsVC.textToShow = ratingText
    }

}

