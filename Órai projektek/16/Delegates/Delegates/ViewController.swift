//
//  ViewController.swift
//  Delegates
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController, SecondViewControllerDelegate {
    @IBOutlet weak var label: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func showNext(_ sender: UIButton) {
        performSegue(withIdentifier: "showSecondVC", sender: nil)
    }
    
    func textChanged(newText: String) {
        label.text = newText
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Alább több megoldás van, amik ugyanazt a feladatot látják el
        //Csak a szemléltetés miatt van megírva többször, a lenti megoldásokközül csak egyet kell megtartani
        
        //If-let megoldás
        if let secondVC = segue.destination as? SecondViewController {
            secondVC.delegate = self
            secondVC.initialText = label.text
        }

        //Guard megoldás
        guard let secondViewController = segue.destination as? SecondViewController else {
            return
        }
        secondViewController.initialText = label.text
        secondViewController.delegate = self

        //Standard if-1
        if(segue.destination is SecondViewController) {
            let secondVC = segue.destination as! SecondViewController
            secondVC.delegate = self
            secondVC.initialText = label.text
        }
        
        //Standard if-2
        let secondVC = segue.destination as? SecondViewController
        if(secondVC != nil) {
            secondVC!.delegate = self
            secondVC!.initialText = label.text
        }
        
        //Egybébkémnt If nélkül is működik
        let secondVController = segue.destination as? SecondViewController
        secondVController?.delegate = self
        secondVController?.initialText = label.text
        
        
    }

}

