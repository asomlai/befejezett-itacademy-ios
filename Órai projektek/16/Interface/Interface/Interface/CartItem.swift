//
//  CartItemInterface.swift
//  Interface
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

protocol CartItem {
    var price : Double { get }
    var type : CartItemType { get }
    var name : String { get }
    
    func prettyPrint()
}
