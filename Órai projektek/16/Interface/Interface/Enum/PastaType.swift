//
//  PastaType.swift
//  Interface
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

enum PastaType : String {
    case penne = "Penne"
    case fusili = "Fusili"
    case ravioli = "Ravioli"
    case farfalle = "Farfalle"
    case spaghetti = "Spaghetti"
}
