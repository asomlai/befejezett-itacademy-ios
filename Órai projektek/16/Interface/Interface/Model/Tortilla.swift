
//
//  Tortilla.swift
//  Interface
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class Tortilla: Food {
    var isHot : Bool = false
    
    override init() {
        super.init()
        type = .tortilla
    }
    
    override init(name: String, price: Double) {
        super.init(name: name, price: price)
        type = .tortilla
    }
    
    override func prettyPrint() {
        print("Tortilla name: \(name), price:\(price), isHot:\(isHot)")
    }
}
