//
//  Drink.swift
//  Interface
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class Drink: CartItem {
    var price : Double
    var name : String
    var type : CartItemType = .drink
    
    var alcoholic : Bool = false
    var producerName : String = "Coca Cola"
    
    init(price: Double, name: String) {
        self.price = price
        self.name = name
    }
    
    convenience init(price: Double, name: String, alcoholic: Bool, producerName: String) {
        self.init(price: price, name: name)
        self.alcoholic = alcoholic
        self.producerName = producerName
    }
    
    func prettyPrint() {
        print("Drink name: \(name), price:\(price), isAlcoholic:\(alcoholic), producer: \(producerName)")
    }
}
