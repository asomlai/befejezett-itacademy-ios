//
//  Food.swift
//  Interface
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class Food: CartItem {
    var type : CartItemType = .food
    var price : Double
    var name : String
    
    init() {
        self.name = "not set"
        self.price = 0
    }
    
    init(name: String, price: Double) {
        self.name = name
        self.price = price
    }
    
    func prettyPrint() {
        print("Food name: \(name), price:\(price)")
    }
}
