//
//  main.swift
//  05
//
//  Created by Andras Somlai on 2018. 04. 03..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Foundation

///*
// Anyagok Diához kezdete
// */
//
////Enum a kávéméret egyszerűbb definiálásához
//enum CoffeeSize : Int {
//    case small
//    case large
//}
//
//
//class CoffeeMaker { //Osztály definiálása class kulcsszóval
//    var producerName : String //Gyártó property
//    var waterTankSize : Double //víztartály mérete
//    var color : String //kávégép színe
//    var powerConsumption : Double //Áramfogyasztás
//
//    //Paraméter nélküli inicializáló függvény
//    init() {
//        producerName = "CoffeMaker CO" //Ezek default értékek
//        waterTankSize = 1.5
//        color = "black"
//        powerConsumption = 220.0
//    }
//
//    //Inicializáló függvény
//    init(factoryName : String) {
//        producerName = factoryName //Propertyk beállítása
//        waterTankSize = 1.5 //Ezek default értékek
//        color = "black"
//        powerConsumption = 220.0
//    }
//
//    //Több paraméteres init függvény
//    init(factoryName : String, waterTank : Double, machineColor : String, consumptionOfPower : Double) {
//        producerName = factoryName //Propertyk beállítása
//        waterTankSize = waterTank
//        color = machineColor
//        powerConsumption = consumptionOfPower
//    }
//
//    //CoffeeSize egy enum típus .small és .big értékekkel
//    //Kávé létrehozása függvény implementálása a CoffeeMaker osztályba
//    func makeCoffee(power : Power, water : Water, capsule : Capsule, size : CoffeeSize) -> Coffee? {
//        if (power.availablePower < powerConsumption) {
//            debugPrint("Nincs elegendő energia a kávéfőzéshez")
//            return nil
//        }
//
//        if (water.availableQuantity < 0.1) {
//            debugPrint("Nincs elegendő víz a kávéfőzéshez")
//            return nil
//        }
//
//        //Kávé példányosítása Coffee inicializáló függvényével
//        let coffee = Coffee(machine: self, water: water, capsule: capsule)
//
//        //Mivel az inicializáló függvényben nem tettem lehetővé a méret megadását
//        //Ezért külön kell beállítoanom a példány propertyjét
//        switch size {
//            case .large: coffee.quantity = 0.4
//            case .small: coffee.quantity = 0.1
//        }
//
//        water.availableQuantity = water.availableQuantity - 100
//
//        //Visszatér a függvény a frissen létrehozott kávéval
//        return coffee
//    }
//}
//
//class Capsule {
//    var taste : String = "Caramel" //Default érték így is megadható egy propertynek
//    var color : String = "brown"    //Ha van default érték, nem szükséges init függvény
//    var producer : String = "CapsuleMaker CO"
//    var caffiene : Double = 5.2
//    var isUsed : Bool = false
//}
//
//class Water {
//    var quality : Int = 7   //víz minőslége 10-es skálán
//    var availableQuantity : Double = 2000.0 //Elérhető vízmennyiség literben
//}
//
//class Power {
//    var availablePower : Double = 2200000.0 //felhasználható energia
//    var currentPowerBill : Double = 0 //Aktuális energia számla
//}
//
//class Coffee {
//    var taste : String? = nil
//    var quantity : Double? = nil
//    var quality : Double? = nil
//    var caffeine : Double? = nil
//
//    init(machine : CoffeeMaker, water : Water, capsule : Capsule) {
//        taste = capsule.taste //Az objektum taste propertyjét beállítom a paraméter taste propertyje szerint
//
//        quality = Double(water.quality) //Az objektum quality propertyjét beállítom a paraméter quality propertyje szerint.
//                                        //Mivel más típusú a két property, castolnom is kell
//
//        caffeine = capsule.caffiene//Az objektum caffiene propertyjét beállítom a paraméter caffiene propertyje szerint
//
//        //!! A quantityt nem állítja be az init fgv !!
//        //Ezt a létrehozott példányon állítja be a CoffeeMaker
//    }
//}
//
////Víz, és energia példányosítása paraméterek nélküli init függvénnyel
//let water = Water()
//let power = Power()
//
////CoffeeMaker példányosítása. myCoffeeMaker lesz az új CoffeeMaker típusú változó
////Az init függvények közül a paramétereset használtam
//let myCoffeeMaker = CoffeeMaker(factoryName : "CoffeMaker CO", waterTank : 1.7, machineColor : "red", consumptionOfPower : 220)
//
////példányosítom Capsule-t a paraméterek nélküli init függvényével
//let myCapsule = Capsule()
////beállítom/módosítom a példény propertyjeit
//myCapsule.taste = "vanilla"
//myCapsule.caffiene = 8.2
//
////Létrehozok új
//let myCoffee = myCoffeeMaker.makeCoffee(power: power, water: water, capsule: myCapsule, size: .large)
//if(myCoffee == nil) {
//    debugPrint("Nem sikerült lefőzni a kávét")
//}
//else {
//    debugPrint("Sikerült lefőzni a kávét")
//    debugPrint(myCoffee!.caffeine!) //MyCoffee biztos létezik, ezért nyugodtan mehet mögé a "!"
//    debugPrint(myCoffee!.quality!)  //A propertyk is opcionálisak, de úgy írtuk meg a kódot, hogy kapnak értéket
//    debugPrint(myCoffee!.quantity!) //Ezért a propertyk mögé is mehet a "!"
//    debugPrint(myCoffee!.taste!)
//}

/*
 Anyagok Diához VÉGE
 */


/*
 Feladatmegoldások kezdete
 */


//Enum a kávéméret egyszerűbb definiálásához
enum CoffeeSize : Int {
    case small
    case large
}

class Capsule {
    var taste : String = "Caramel" //Default érték így is megadható egy propertynek
    var color : String = "brown"    //Ha van default érték, nem szükséges init függvény
    var producer : String = "CapsuleMaker CO"
    var caffiene : Double = 5.2
    var isUsed : Bool = false
}

class Water {
    var quality : Int = 7   //víz minőslége 10-es skálán
}

class Power {
    var availablePower : Double = 90000.0 //felhasználható energia
    var currentPowerBill : Double = 0 //Aktuális energia számla
}

class Coffee {
    var taste : String? = nil
    var quantity : Double? = nil
    var quality : Double? = nil
    var caffeine : Double? = nil
    
    init(machine : CoffeeMaker, water : Water, capsule : Capsule) {
        taste = capsule.taste //Az objektum taste propertyjét beállítom a paraméter taste propertyje szerint
        
        quality = Double(water.quality) //Az objektum quality propertyjét beállítom a paraméter quality propertyje szerint.
        //Mivel más típusú a két property, castolnom is kell
        
        caffeine = capsule.caffiene//Az objektum caffiene propertyjét beállítom a paraméter caffiene propertyje szerint
        
        //!! A quantityt nem állítja be az init fgv !!
        //Ezt a létrehozott példányon állítja be a CoffeeMaker
    }
}

class Home {
    var coffeeMaker : CoffeeMaker? //A lakásban lévő kávégép. Lehet nil az értéke, mert nem biztos, hogy van
    var water : Water = Water() //A lakásban ez a víz érhető el
    var power : Power = Power() //A lakásban ez az energia van
    var availableWater : Double = 200 //Ennyi elérhető liter víz van egy lakásban alapból.
    
    /*
     Amikor létrehozok egy példánmyt a Home osztályból, akkor abban alapból nem lesz kávégép, viszont lesz benne víz, meg áram
     */
    
    // A Home osztályba írjátok meg a coffeeParty függvényt, ami addig főzi a kávét, amíg van elég víz és áram a lakásban. Amint valamelyik kifogyott, írja ki a konzolra, hogy mi fogyott el
    func coffeeParty() {
        if(coffeeMaker == nil) {
            debugPrint("Kávégép nélkül nem lehet kávét főzni")
            return
        }
        var numberOfCookedCoffees = 0
        while (true) {
            let coffeeCapsule = Capsule() //Feltétlezem, hogy végtelen számú kávékapszula áll rendelkezésre
            let coffee = coffeeMaker?.makeCoffee(power: self.power, capsule: coffeeCapsule, size: .large)
            
            
            //Nem sikerült a kávé lefőzése
            //Két dolog okozhatja: kifogyott a kávégépből a víz, vagy elfogyott az áram
            if(coffee == nil) {
                //Ha kevesebb áramom van, mint ami 1 kávé elkészítéséhez kell
                if(power.availablePower < coffeeMaker!.powerConsumption) {
                    //A makeCoffee függvény kilogolja a hibát, így itt nekünk már nem kell
                    break //kilépek a ciklusből
                }
                //Ha nem sikerült a kávé lefőzése, és a lakásban sincs már víz, akkor nem újratölteni kell a gépet, hanem teléjesen kifogytunk a vízből
                //Az availableWater értékét lenullázhatjuk, mert az utolsó csepp vízig mindenből kávét főzünk
                else if(availableWater == 0) {
                    //Azt nem tudja a kávégép, hogy a lakásban mennyi víz van, ezért itt kel logolni.
                    //A makeCoffee függvény azt írja majd csak ki, hogy a víztartályban nincs elég víz kávéfőzéshez
                    debugPrint("Elfogyott a lakásból a víz")
                    break //kilépek a ciklusból
                }
                //Ha van elég áram, és van még víz a lakásban, akkor biztosan a gépből fogyott ki a víz
                else {
                    let waterInCoffeeMaker = coffeeMaker!.waterLevel //Ennyi víz van a kávéfőzőben (nem elég 1 kávé elkszítéséhez)
                    let requiredWaterToFullFillTank = coffeeMaker!.waterTankSize - waterInCoffeeMaker //ennyi víz kell a kávégépbe, hogy tele legyen töltve
                    let filledWater = min(requiredWaterToFullFillTank, availableWater)  //Ha kevesebb víz van, mint amennyi ahhoz szükséges, hogy teletöltsük a gépet, akkor minden vizet beletöltök
                                                                                        //Egyébként pontosan annyit töltök bele, hogy tele legyen a tartály
                    coffeeMaker!.fillWaterTank(filledWaterQuantity: filledWater, filledWater: self.water) //belettöltöm a kávégépbe a vizet
                    availableWater -= filledWater //A lakásban lévő elérhető víz mennyisége csökkent a betöltött víz mennyiségével
                    debugPrint("feltöltöttem a kávégépet \(filledWater) liter vízzel")
                }
            }
            numberOfCookedCoffees += 1
            print("Lefőzött kávék száma:\(numberOfCookedCoffees). Még ennyi víz van a lakásban: \(availableWater). Még ennyi áram van a lakásban:\(self.power.availablePower). Ennyi víz a kávégépben:\(coffeeMaker!.waterLevel)")
        }
        
    }
}

debugPrint("----------------------------------------")
debugPrint("Feladatmegoldáshoz kapcsolódó logok eleje")
let myFlat = Home() //létrehoztam egy Hone osztályú változót. Ez lesz a lakás, ahol a kávéfőzés történni fog

myFlat.coffeeParty() //Ezt fogja kiírni: "Kávégép nélkül nem lehet kávét főzni"

//Mivel a lakásnak alapból nem tartozéka a kávéfőző (initializer nem hoz létre, nincs default értéke sem)
//Ezért létrehozok egy kávéfőző objektumot is
let myCoffeeMaker = CoffeeMaker(factoryName : "Krups", waterTank : 1.7, machineColor : "orange", consumptionOfPower : 220)

//Beállítom a lakás coffeeMaker tagváltozóját a frissen létrehozott objektum szerint
//"Beteszem a lakásba a kávéfőzőt"
myFlat.coffeeMaker = myCoffeeMaker

//A myFlat változó nélkül logikusan nem tzdok kávét főzni
//Ezért a paramétereket a myFlat propertyjeiből veszem

myCoffeeMaker.makeCoffee(power: myFlat.power, capsule: Capsule(), size: .large)

myFlat.coffeeParty()


debugPrint("Feladatmegoldáshoz kapcsolódó logok vége")
debugPrint("----------------------------------------")


/*
 Feladatmegoldások vége
 */

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//!!!Ez a pár kódsor nem kapcsolódik szorosan a megoldáshoz. Itt csak példányosítgatok, és a properyket módosítom, és írom ki.
//Ez a rész lehet a segítségetekre, ha szintaxis-beli probléméátok adódna
//
//Víz, és energia példányosítása paraméterek nélküli init függvénnyel
let water = Water()
let power = Power()

//CoffeeMaker példányosítása. myCoffeeMaker lesz az új CoffeeMaker típusú változó
//Az init függvények közül a paramétereset használtam
let myRedCoffeeMaker = CoffeeMaker(factoryName : "CoffeMaker CO", waterTank : 1.7, machineColor : "red", consumptionOfPower : 220)

//példányosítom Capsule-t a paraméterek nélküli init függvényével
let myCapsule = Capsule()
//beállítom/módosítom a példény propertyjeit
myCapsule.taste = "vanilla"
myCapsule.caffiene = 8.2

//Létrehozok új
let myCoffee = myRedCoffeeMaker.makeCoffee(power: power, capsule: myCapsule, size: .large)
if(myCoffee == nil) {
    debugPrint("Nem sikerült lefőzni a kávét")
}
else {
    debugPrint("Sikerült lefőzni a kávét")
    debugPrint(myCoffee!.caffeine!) //MyCoffee biztos létezik, ezért nyugodtan mehet mögé a "!"
    debugPrint(myCoffee!.quality!)  //A propertyk is opcionálisak, de úgy írtuk meg a kódot, hogy kapnak értéket
    debugPrint(myCoffee!.quantity!) //Ezért a propertyk mögé is mehet a "!"
    debugPrint(myCoffee!.taste!)
}

//
//NEM A FELADATMEGOLDÁSHOZ KAPCSOLÓDÓ RÉSZ VÉGE
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

