//
//  CoffeMaker.swift
//  05
//
//  Created by Andras Somlai on 2018. 04. 05..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Cocoa

class CoffeeMaker { //Osztály definiálása class kulcsszóval
    var producerName : String //Gyártó property
    var waterTankSize : Double //víztartály mérete
    var color : String //kávégép színe
    var powerConsumption : Double //Áramfogyasztás
    var waterLevel : Double = 0 //Mennyi víz van jelenleg a kávéfőzőben
    var water : Water? //ez a víz van beletöltve a kávéfőzőbe. Opcionális, mert lehet, hogy nincs víz a gépben
    
    //Paraméter nélküli inicializáló függvény/konstruktor
    init() {
        producerName = "CoffeMaker CO" //Ezek default értékek
        waterTankSize = 1.5
        color = "black"
        powerConsumption = 220.0
    }
    
    //Inicializáló függvény/Konstruktor
    init(factoryName : String) {
        producerName = factoryName //Propertyk beállítása
        waterTankSize = 1.5 //Ezek default értékek
        color = "black"
        powerConsumption = 220.0
    }
    
    //Több paraméteres init függvény/konstruktpr
    init(factoryName : String, waterTank : Double, machineColor : String, consumptionOfPower : Double) {
        producerName = factoryName //Propertyk beállítása
        waterTankSize = waterTank
        color = machineColor
        powerConsumption = consumptionOfPower
    }
    
    //CoffeeSize egy enum típus .small és .big értékekkel
    //Kávé létrehozása függvény implementálása a CoffeeMaker osztályba
    func makeCoffee(power : Power, capsule : Capsule, size : CoffeeSize) -> Coffee? {
        if (power.availablePower < powerConsumption) {
            debugPrint("Nincs elegendő energia a kávéfőzéshez")
            return nil
        }
        
        if(water == nil) {
            debugPrint("Nincs víz a kávéfőzőben")
            return nil
        }
        
        if ((size == .large && waterLevel < 0.4) || (size == .small || waterLevel < 0.1)) {
            debugPrint("Nem sikerült kávét főzni. Nincs elég víz a tartályban. Töltsd újra!")
            return nil
        }
        
        if (capsule.isUsed == true) {
            debugPrint("Használt kapszulából nem lehet kávét főzni")
            return nil
        }
        
        //Kávé példányosítása Coffee inicializáló függvényével
        let coffee = Coffee(machine: self, water: water!, capsule: capsule)
        
        //Mivel az inicializáló függvényben nem tettem lehetővé a méret megadását
        //Ezért külön kell beállítoanom a példány propertyjét
        switch size {
        case .large: coffee.quantity = 0.4 //4 dl-es a nagy kávé
        case .small: coffee.quantity = 0.1 //1 dl-es a kis kávé
        }
        
        waterLevel -= coffee.quantity!
        
        power.availablePower -= powerConsumption
        power.currentPowerBill += powerConsumption
        
        //Ha a waterLevel 0, akkor nincs víz a kávéfőzőben
        //A self.water-t ennek megfelelően nil-re kell állítani
        //Azért így vizsgálom, mert a Double típus számábrázolási pontatlanságaiból eredően lehet, hogy logikusan 0 lenne az érték, de a változóban valami elhanyagolhatóan kicsi
        //de nullától eltérő érték van. Pl:  0.000000000000443
        if(waterLevel < 0.000001) {
            self.water = nil
        }
        
        //A kapszula objektumot használtra állítjuk
        capsule.isUsed = true
        
        //Visszatér a függvény a frissen létrehozott kávéval
        return coffee
    }
    
    //víztartály teletöltése
    func fillWaterTank(water : Water) {
        //A water objektumba is betesszük az új viz objektumot.
        //Ennek akkor van szerepe, ha más minőségű vízzel töltjük meg a kávégépet.
        self.water = water
        
        //Teletöltjük a waterTanket
        waterLevel = waterTankSize
    }
    
    //víztartály feltöltése filledWater liter vízzel
    func fillWaterTank(filledWaterQuantity : Double, filledWater : Water) {
        //Ha túl sok vizet öntünk a kávégépbe, akkor kifolyik belőle a víz. Ezt jelezzük
        if(filledWaterQuantity + waterLevel > waterTankSize) {
            debugPrint("Kifolyt a víz")
        }
        
        //betöltjük a vizet a tartályba, ügyelve arra, hogy a waterLevel értéke nem lehet nagyovv waterTankSizenál
        waterLevel = min(waterTankSize, filledWaterQuantity + waterLevel)
        
        //A water objektumba is betesszük az új viz objektumot.
        //Ennek akkor van szerepe, ha más minőségű vízzel töltjük meg a kávégépet.
        water = filledWater
    }
}

