//
//  main.swift
//  05
//
//  Created by Andras Somlai on 2018. 04. 03..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import Foundation

///*
// Anyagok Diához kezdete
// */
//
////Enum a kávéméret egyszerűbb definiálásához
//enum CoffeeSize : Int {
//    case small
//    case large
//}
//
//
//class CoffeeMaker { //Osztály definiálása class kulcsszóval
//    var producerName : String //Gyártó property
//    var waterTankSize : Double //víztartály mérete
//    var color : String //kávégép színe
//    var powerConsumption : Double //Áramfogyasztás
//
//    //Paraméter nélküli inicializáló függvény
//    init() {
//        producerName = "CoffeMaker CO" //Ezek default értékek
//        waterTankSize = 1.5
//        color = "black"
//        powerConsumption = 220.0
//    }
//
//    //Inicializáló függvény
//    init(factoryName : String) {
//        producerName = factoryName //Propertyk beállítása
//        waterTankSize = 1.5 //Ezek default értékek
//        color = "black"
//        powerConsumption = 220.0
//    }
//
//    //Több paraméteres init függvény
//    init(factoryName : String, waterTank : Double, machineColor : String, consumptionOfPower : Double) {
//        producerName = factoryName //Propertyk beállítása
//        waterTankSize = waterTank
//        color = machineColor
//        powerConsumption = consumptionOfPower
//    }
//
//    //CoffeeSize egy enum típus .small és .big értékekkel
//    //Kávé létrehozása függvény implementálása a CoffeeMaker osztályba
//    func makeCoffee(power : Power, water : Water, capsule : Capsule, size : CoffeeSize) -> Coffee? {
//        if (power.availablePower < powerConsumption) {
//            debugPrint("Nincs elegendő energia a kávéfőzéshez")
//            return nil
//        }
//
//        if (water.availableQuantity < 0.1) {
//            debugPrint("Nincs elegendő víz a kávéfőzéshez")
//            return nil
//        }
//
//        //Kávé példányosítása Coffee inicializáló függvényével
//        let coffee = Coffee(machine: self, water: water, capsule: capsule)
//
//        //Mivel az inicializáló függvényben nem tettem lehetővé a méret megadását
//        //Ezért külön kell beállítoanom a példány propertyjét
//        switch size {
//            case .large: coffee.quantity = 0.4
//            case .small: coffee.quantity = 0.1
//        }
//
//        water.availableQuantity = water.availableQuantity - 100
//
//        //Visszatér a függvény a frissen létrehozott kávéval
//        return coffee
//    }
//}
//
//class Capsule {
//    var taste : String = "Caramel" //Default érték így is megadható egy propertynek
//    var color : String = "brown"    //Ha van default érték, nem szükséges init függvény
//    var producer : String = "CapsuleMaker CO"
//    var caffiene : Double = 5.2
//    var isUsed : Bool = false
//}
//
//class Water {
//    var quality : Int = 7   //víz minőslége 10-es skálán
//    var availableQuantity : Double = 2000.0 //Elérhető vízmennyiség literben
//}
//
//class Power {
//    var availablePower : Double = 2200000.0 //felhasználható energia
//    var currentPowerBill : Double = 0 //Aktuális energia számla
//}
//
//class Coffee {
//    var taste : String? = nil
//    var quantity : Double? = nil
//    var quality : Double? = nil
//    var caffeine : Double? = nil
//
//    init(machine : CoffeeMaker, water : Water, capsule : Capsule) {
//        taste = capsule.taste //Az objektum taste propertyjét beállítom a paraméter taste propertyje szerint
//
//        quality = Double(water.quality) //Az objektum quality propertyjét beállítom a paraméter quality propertyje szerint.
//                                        //Mivel más típusú a két property, castolnom is kell
//
//        caffeine = capsule.caffiene//Az objektum caffiene propertyjét beállítom a paraméter caffiene propertyje szerint
//
//        //!! A quantityt nem állítja be az init fgv !!
//        //Ezt a létrehozott példányon állítja be a CoffeeMaker
//    }
//}
//
////Víz, és energia példányosítása paraméterek nélküli init függvénnyel
//let water = Water()
//let power = Power()
//
////CoffeeMaker példányosítása. myCoffeeMaker lesz az új CoffeeMaker típusú változó
////Az init függvények közül a paramétereset használtam
//let myCoffeeMaker = CoffeeMaker(factoryName : "CoffeMaker CO", waterTank : 1.7, machineColor : "red", consumptionOfPower : 220)
//
////példányosítom Capsule-t a paraméterek nélküli init függvényével
//let myCapsule = Capsule()
////beállítom/módosítom a példény propertyjeit
//myCapsule.taste = "vanilla"
//myCapsule.caffiene = 8.2
//
////Létrehozok új
//let myCoffee = myCoffeeMaker.makeCoffee(power: power, water: water, capsule: myCapsule, size: .large)
//if(myCoffee == nil) {
//    debugPrint("Nem sikerült lefőzni a kávét")
//}
//else {
//    debugPrint("Sikerült lefőzni a kávét")
//    debugPrint(myCoffee!.caffeine!) //MyCoffee biztos létezik, ezért nyugodtan mehet mögé a "!"
//    debugPrint(myCoffee!.quality!)  //A propertyk is opcionálisak, de úgy írtuk meg a kódot, hogy kapnak értéket
//    debugPrint(myCoffee!.quantity!) //Ezért a propertyk mögé is mehet a "!"
//    debugPrint(myCoffee!.taste!)
//}

/*
 Anyagok Diához VÉGE
 */


/*
 Feladatmegoldások kezdete
 */

class CoffeMaker {
    //property list
    var producer : String
    var waterTankSize : Double
    var color : String
    var powerConsumption : Double
    
    //initialitzers
    init() {
        producer = "CoffeMaker CO"
        waterTankSize = 1.5 //liter
        color = "black"
        powerConsumption = 220 // unit /coffee
    }
    
    init(factoryName : String, tankSize : Double, machineColor : String, powerConsumption : Double) {
        producer = factoryName
        waterTankSize = tankSize
        color = machineColor
        self.powerConsumption = powerConsumption
    }
    
    init(factoryName : String) {
        producer = factoryName
        waterTankSize = 1.5
        color = "grey"
        self.powerConsumption = 220
    }
    
    //class methods
//    Írd meg a CoffeeMaker (Kávéfőző) osztályban a makeCoffee függvényt, ami Power, Water, Capsule, és CoffeeSize paraméterekkel rendelkezik.
    func makeCoffe(powerParam : Power, waterParam : Water, capsuleParam : Capsule, coffeeSizeParam : CoffeeSize) -> Coffee? {
        
        if(self.powerConsumption > powerParam.availablePower) {
            debugPrint("Nincs áram a kávé készítéséhez")
            return nil
        }
        
        if((coffeeSizeParam == .small && waterParam.availableQuantity < 0.1)
            || (coffeeSizeParam == .large && waterParam.availableQuantity < 0.4)) {
            debugPrint("Nincs víz a kávé készítéséhez")
            return nil
        }
        

        let newCoffee = Coffee(coffeeMaker: self, water: waterParam, capsule: capsuleParam)
        
        switch coffeeSizeParam {
        case .small: newCoffee.quantity = 0.1
        case .large: newCoffee.quantity = 0.4
        }
        
        return newCoffee
    }

}

class Capsule {
    var taste : String = "finom"
    var capsuleColor : String = "barna"
    var producer : String = "Nespresso"
    var caffeine : Double = 4
    var isUsed : Bool = false
}

class Water {
    var quality : Int = 7   //víz minősége 10-es skálán
    var availableQuantity : Double = 2000.0 //Elérhető vízmennyiség literben
}

class Power {
    var availablePower : Double = 2200000.0 //felhasználható energia
    var currentPowerBill : Double = 0 //Aktuális energia számla
}

class Coffee {
    var taste : String?
    var quality : Double?
    var quantity : Double?
    var caffeine : Double?
    
    init(coffeeMaker : CoffeMaker, water : Water, capsule : Capsule) {
        self.taste = capsule.taste
        self.caffeine = capsule.caffeine
        self.quality = Double(water.quality)
    }
}

enum CoffeeSize {
    case small
    case large
}

//let machine1 = CoffeMaker() //paraméterek nélküli konstruktor
//let machine2 = CoffeMaker(factoryName: "Krups", tankSize: 0.5, machineColor: "red", powerConsumption : 30) //paraméterezett konstruktor
//let machine3 = CoffeMaker(factoryName: "DeLonghi")
//
//debugPrint(machine1.color)
//debugPrint(machine1.waterTankSize)
//debugPrint(machine1.producer)
//
//debugPrint(machine2.color)
//debugPrint(machine2.waterTankSize)
//debugPrint(machine2.producer)
//
//debugPrint(machine3.color)
//debugPrint(machine3.waterTankSize)
//debugPrint(machine3.producer)


//
//16. Hozz létre egy Water, és egy Power típusú változót (avagy példányosítsd ezeket az osztályokat). Paraméterek nélküli inicializáló függvényt (másik nevén KONSTRUKTOR-t) használj. (elvileg mást nem is tudsz)
let water = Water()
let power = Power()

//17. Hozz létre paraméterek nélküli konstruktorral egy kapszula típusú változót. Ennek módosítsd a taste és caffeine propertyjét (nem a konstruktorban)
let myCapsule = Capsule()
myCapsule.taste = "caramel"
myCapsule.caffeine = 7.8

//18. Példányosítsd a kávéfőző osztályodat paraméterezett konstruktorral
let coffeeMaker = CoffeMaker(factoryName: "Krups", tankSize: 0.5, machineColor: "red", powerConsumption : 30)

//19. A kávéfőző példányod segítségével, valamint a víz, kapszula, és áram példányok felhasználásával hozz létre egy Coffee objektumot.
let myCoffee = coffeeMaker.makeCoffe(powerParam: power, waterParam: water, capsuleParam: myCapsule, coffeeSizeParam: .large)



/*
 Feladatmegoldások vége
 */

