//
//  CustomView.swift
//  14-Tips
//
//  Created by Andras Somlai on 2018. 05. 08..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

enum CustomViewImagePosition {
    case up
    case down
}

class CustomView: UIView {
    private let squareView = UIView()
    private let imageView = UIImageView()
    var imagePosition : CustomViewImagePosition = .down
    
    
    //UIView osztály közvetlenül implementálja az init(frame:CGRect) inizializert, ezért ezt overridedal lehet csak megírni
    override init(frame : CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    //A paraméterek nélküli konstruktort nem implementálja UIView, ezért ide nem kell override
    init() {
        //A superen viszont a frame-es initializert kell meghívjuk, aminek CGRect.zero-t adok itt paraméterül.
        //CGRect.zero = CGRect(x:0, y:0, width:0, height:0)
        super.init(frame: CGRect.zero)
        setupUI()
    }
    
    //A required init olyan függvény, amit a leszármazottakban implementálni kell
    //A UIView-nak van ez a függvénye, ezért ezt ide mindig be kell írni, ha konstruktort csináltok. Autocorrect segít!
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //Mivel 2 konstruktor van, praktikus kiszevrezni egy külön függvénybe azt, amit a VC-ben viewDidLoadba írtunk
    func setupUI() {
        squareView.backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 1)
        squareView.isUserInteractionEnabled = false //Ezzel gaaratntálom, hogy a tap gesztust nem kapja el az imageView, és a CustomView fogja észlelni
        addSubview(squareView) //Mivel ez egy view leszármazott, ezért közvetlen meghívhatjuk az addSubView függvényt
        
        imageView.contentMode = .scaleAspectFill
        imageView.image = UIImage(named: "pic")
        imageView.clipsToBounds = true //Ezzel állítod be, hogy ha a kép kilógna az imageView framejéből, akkor vágja le a képet
        imageView.isUserInteractionEnabled = false //Ezzel gaaratntálom, hogy a tap gesztust nem kapja el az imageView, és a CustomView fogja észlelni
        addSubview(imageView)
        
        backgroundColor = #colorLiteral(red: 1, green: 1, blue: 0.7785915799, alpha: 1)
        //Ha garantálni akarjátok, hogy nem lóg ki semmi a customView-ból
        clipsToBounds = true
        
        //Tap gesztus hozzáadása
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(tapped))
        tap.addTarget(self, action: #selector(tap(sender:)))
        addGestureRecognizer(tap)
    }
    
    //Frameket itt állítsátok
    override func layoutSubviews() {
        super.layoutSubviews()
        //Ha az van beállítva, hogy a kép alul kell legyen, mások a framek, mintha za lenne beállítva, hogy felül van.
        if(imagePosition == .down) {
            squareView.frame = CGRect(x:frame.width * 0.05, y:frame.height * 0.05, width: frame.width * 0.9, height:frame.height * 0.1)
            imageView.frame = CGRect(x:frame.width * 0.05, y:frame.height * 0.2, width: frame.width * 0.9, height:frame.height * 0.75)
        }
        else {
            squareView.frame = CGRect(x:frame.width * 0.05, y:frame.height * 0.85, width: frame.width * 0.9, height:frame.height * 0.1)
            imageView.frame = CGRect(x:frame.width * 0.05, y:frame.height * 0.05, width: frame.width * 0.9, height:frame.height * 0.75)
        }
    }
    
    @objc func tap(sender: UITapGestureRecognizer) {
        
    }
    
    //Tap gesztusra ez hívódik meg
    @objc func tapped() {
        //Módosítom az imagePosition propertyt
        imagePosition = imagePosition == .up ? .down : .up
        
        /*
        if(imagePosition == .up) {
            imagePosition = .down
        }
        else {
            imagePosition = .up
        }
 */
        
        //Animálva frissítem a frameket
        //A layoutSubviews függvényt meghívhatom direktben
        /*
        self.setNeedsLayout() //Beállítom a viewn, hogy UI frissítés szükséges
        UIView.animate(withDuration: 0.4) {
            self.layoutIfNeeded() //Azonnal frissítem a UI-t animálva
        }
        */
        
        UIView.animate(withDuration: 0.4) {
            self.layoutSubviews()
        }
    }
}
