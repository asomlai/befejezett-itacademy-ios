//
//  ViewController.swift
//  14-Tips
//
//  Created by Andras Somlai on 2018. 05. 08..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate/*UITextFieldDelegate: Segítségével a textfieldeken történő veneteket elérjük*/{

    private let button = UIButton()
    private let label = UILabel()
    private let textField = UITextField()
    private let customView = CustomView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Itt adjátok hozzá a UIView-kat, és állítsatok be rajtuk méret független propertyket.
        //Méretfüggetlen propertyk pl: szín, text alignment, action
        
        //Példaképp hozzáadok egy UIButton-t, egy UITextFieldet, és egy UILabelt és egy customView-t
        //UIButton
        button.setTitle("Bejelentkezés", for: .normal) //Gombon alapból megjelenő szöveg
        button.setTitle("Nem tudsz bejelentkezni", for: .disabled) //"Nem tudsz bejelentkezni" -> Ez a szöveg jelenik meg a gombon, ha button.isEnabled = false
        button.setTitleColor(#colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1), for: .normal) //Gombon megjelenő szöveg színének beállítása alaphelyzetben
        button.setTitleColor(#colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1), for: .disabled) //Gombon megjelenő szöveg színének beállítása ha  button.isEnabled = false
        button.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1) //Gomb háttérszínének beállítása
        button.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside) //Gombyomásra a buttonPressed függvény hívódik meg
        button.titleLabel?.font = UIFont(name:"Avenir-Heavy", size:14) //Gombon megjelenő szöveg fontjának beállítása (tip:http://iosfonts.com)
        button.layer.cornerRadius = 5 //Lekerekítés hozzáadása a gomb sarkaihoz
        button.layer.borderColor = UIColor.black.cgColor //Gomb keretének beállítása
        button.layer.borderWidth = 1 //Gomb keretének vastagsága
        view.addSubview(button) //Gomb hozzáadása view-hoz
        
        //UILabel
        label.text = "Email címed:"
        label.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        label.font = UIFont(name:"BradleyHandITCTT-Bold", size:14)
        label.backgroundColor = UIColor.clear //Labelnek is lehet háttérszínt állítani. A UIColor.clear az átlátszó szín
        label.textAlignment = .center //Szöveg elrendezése. Leggyakrabban: jobbra/balra/középre
        
        //label.numberOfLines = 3 //Hány soros legyen a label
        
        //Ha azt akarjátok, hogy a szöveg hoisszától függjön, hogy hány rosoa a label:
        label.numberOfLines = 0 //Így jelezed a labelnek, hogy azt akarod, hogy ő tördelje magát
        label.lineBreakMode = .byWordWrapping //Szavanként tördeli új sorba a szöveget
        label.minimumScaleFactor = 0.5 //A font legfejlebb felére csökken annak érdekében, hogy kiférjen a szöveg
        
        view.addSubview(label)
        
        //UITextField
        textField.placeholder = "Ide írd be az email címed" //Placeholder. Ez jelenik meg halványan a textfieldben ha üres
        textField.textAlignment = .left //ugyanaz, mint uilabelnél
        //textField.isSecureTextEntry = true //Password texfieldeknél. Ezzel állítjátok be, hogy a karakterek helyett pöttyök jelenjenek meg
        textField.keyboardType = .emailAddress //Milyen billzet jelenjen meg a mezőre nyomva
        textField.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        textField.font = UIFont.systemFont(ofSize: 12)
        textField.borderStyle = .roundedRect //Milyen stílusú háttere legyen a textfieldnek
        textField.delegate = self //!!!!!!!!! Ezt be kell állítsátok, hogy a delegate függvények meghívódjanak
        view.addSubview(textField)
        
        //CustomView
        //Mindent megcsinál magában az osztály, nincsenek propertyk, amiket itt kellene állítani, ezért csak annyi dolgom van, h az addSubView függvényt meghívom
        customView.imagePosition = .up //A CustomView propertíket ugyanúgy módosítom, mint a tübbi view esetében
        view.addSubview(customView)
        
        //Ennek a sornak a segítségével tudom detektálni, amikor a billentyűzet megjelenik a kijelzőn
        //Ezzel a sorral állítom be, hogy keyboardWillAppear(notification: Notification) mindig meghívódjon
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Frameket itt állítsátok
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let exampleFrame = CGRect(x:25, y:30, width:200, height:100)
        print("""
        maxX : \(exampleFrame.maxX)
        minX : \(exampleFrame.minX)
        midX : \(exampleFrame.midX)
        maxY : \(exampleFrame.maxY)
        minY : \(exampleFrame.minY)
        midY : \(exampleFrame.midY)
        width : \(exampleFrame.width)
        height : \(exampleFrame.height)
        
        A CGRect(x: y: width: height:) függvénnyel ezeket a propertyket állítjátok be. A fent felsoroltak ezekből számolódnak
        origin.x : \(exampleFrame.origin.x)
        origin.y : \(exampleFrame.origin.y)
        size.width : \(exampleFrame.size.width)
        size.height : \(exampleFrame.size.height)
        """)
        
        
        textField.frame = CGRect(x:0.1*view.frame.width, y:view.frame.height - 200, width:0.8*view.frame.width, height:40)
        print("""
        textField.frame: \(textField.frame)
        textField.bounds: \(textField.bounds)
        Mindkettő CGRect típusú, de csak a frame tartalmaz x,y adatokat. A boundsnál ezek mindig 0-nak számítanak
        
        CGPoint típusú, a view közepét adja vissza, nem a bal felső sarkát
        textfield.center: \(textField.center)
        """)
        
        button.frame = CGRect(x:0.2 * view.frame.width, y:textField.frame.maxY + 20, width:view.frame.width * 0.6, height:40)
        //Ha azt akarjátok, hogy teljesen kerek legyen a gomb széle:
        //button.layer.cornerRadius = button.frame.height / 2
        
        label.frame = CGRect(x:0.1*view.frame.width, y:button.frame.maxY + 20, width:0.8*view.frame.width, height:30)
        //Ezzel a sorral lecsökkenti a label framejét akkorára, hogy épphogy kiférjen benne a szöveg. Az x,y pozíció változatlan, viszont a szélesség, és magasság is változhat
        //Gyakran használjuk, ha multiline labellel dolgozunk.
        //label.sizeToFit()
        
        customView.frame = CGRect(x:10, y:20, width:view.frame.width - 20, height:(view.frame.height / 2) - 30)
    }


    //Ez a függvény fog meghívódni, amikor a billentyűztet megjelenítő animáció elindul a képernyőn
    @objc func keyboardWillAppear(notification: Notification) {
        //Ez lesz a bilentyűzet frame-je
        let finalKeyboardFrame = notification.userInfo!["UIKeyboardFrameEndUserInfoKey"] as! CGRect
        //Ennek alapján kel módosítsd a frameket
        //2 módja van:
        //1: itt megírod a logikát
        //2: felveszel egy isKeyboardVisible változót, a viewDidLayoutSubviewsban elágazol és meghívod a view.setNeedsLayout() függvényt
        //view.setNeedsLayout() --> Beállítom, hogy frissítse a viewController view-ját amint lehetséges
        //INFO: NE HÍVD DIREKTBEN A viewDidLayoutSubviews függvényt!!
        
    }
    
    //Implementálok két fontos UITextFieldDelegate metódust
    //1. Ez hívódik meg, amikor return-t nyom a felhasználó
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder() //Ezzel a sorral csak az adott textfieldhez tartozó billzetet tüntetitek el
        //view.endEditing(true) //Ezzel eltűntetitek abillzetet a viewvontrollerről. Akkor használjuk, ha sok textfield van, és ismeretlen, h melyiket aktív épp
        return true
    }
    
    //Ez hívódik miután egy mező szerkesztése befejeződött
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    
    //Ez a függvény hívódik meg, ha a gomb meg lett nyomva
    @objc func buttonPressed(sender : UIButton) {
        //sender.isEnabled = false
        label.text = "Email címed: \(textField.text ?? "")"
        textField.text = nil
    }
}

