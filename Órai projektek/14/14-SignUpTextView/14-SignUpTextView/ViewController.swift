//
//  ViewController.swift
//  14-SignUpTextView
//
//  Created by Andras Somlai on 2018. 05. 08..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextViewDelegate {
    private let registrationLabel = UITextView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registrationLabel.font = UIFont(name:"KohinoorBangla-Regular", size:14)
        registrationLabel.delegate = self
        registrationLabel.isEditable = false
        registrationLabel.backgroundColor = UIColor.clear
        registrationLabel.textColor = UIColor.black
        
        let str = "Dont't have an account? Sign up"
        let attributedString = NSMutableAttributedString(string: str)
        let foundRange = attributedString.mutableString.range(of: "Sign up")
        attributedString.addAttribute(NSAttributedStringKey.link, value:"--", range: foundRange)
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value:UIColor.blue, range: foundRange)
        registrationLabel.attributedText = attributedString
        
        registrationLabel.textAlignment = .center
        
        view.addSubview(registrationLabel)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        registrationLabel.frame = CGRect(x:view.frame.size.width * 0.05, y:view.frame.size.height - 30, width:view.frame.size.width * 0.9, height:30)
    }
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        debugPrint("url: \(URL)")
        return true
    }
    
}

