//: Playground - noun: a place where people can play

import Foundation

/*
 1. FELADAT:
    Hozz létre, és adj tetszőleges értéket a következő típusú változóknak:
 */
//a
var name = "Dezső"
debugPrint(type(of: name)) //ellenőrzés

//b
var age = 18
debugPrint(type(of: age)) //ellenőrzés

//c
var isHappy = true
debugPrint(type(of: isHappy)) //ellenőrzés

//d
var weight = 54.3
debugPrint(type(of: weight)) //ellenőrzés

//e
var height : Float = 176.4
debugPrint(type(of: height)) //ellenőrzés

//f
var results : [Float] = [10.4, 11.1, 12.4, 13.2]
debugPrint(type(of: results)) //ellenőrzés

//g
var exchangeRates : [String : Double] = ["HUF" : 312.43, "USD" : 1.23]
debugPrint(type(of: exchangeRates)) //ellenőrzés


/*
 2. FELADAT:
    Hozz létre két változót. Az egyik Double típusú, értéke legyen 4.14, a másik Int típusú, értéke legyen 4. Egy if feltételben hasonlítsd össze a két változó értékét. Az if törzsében írd ki, hogy „4 kisebb, mint 4.14”. A kiírásnál a debugPrintben használd a változókat, ne begépeld a két számot.
 */
var integerVariable = 4
var doubleVariable = 4.14
if(Double(integerVariable) < doubleVariable) {
    //A lenti két sor közül mindkettő jó
    debugPrint(integerVariable, " kisebb, mint ", doubleVariable)
    debugPrint("\(integerVariable) kisebb, mint \(doubleVariable)")
}

/*
 3. FELADAT:
    Hozz létre két Bool típusú változót. Az egyik neve legyen ’a3’, értéke true, a másik neve legyen ’b3’, értéke pedig false.
    xLogold ki a következő műveletek eredményét
 */

var a3 = true
var b3 = false

//a
debugPrint(a3 && b3)

//b
debugPrint(a3 || b3)

//c
debugPrint(!a3 || b3)

//d
debugPrint(!b3)

//e
debugPrint(!b3 && a3)

/*
 4. FELADAT:
    Adott három Bool típusú változó: a4, b4, c4. Add meg ezeknek a változónak az értékét úgy, hogy a következő kifejezések igazak legyenek
 */

var a4 = true
var b4 = true
var c4 = true

//a
a4 = true
b4 = true
debugPrint(a4 && b4)

//b
a4 = true
b4 = false
debugPrint(a4 || b4)

//c
a4 = true
b4 = true
c4 = true
debugPrint(a4 && b4 && c4)

//d - 1. jó megoldás
a4 = true
b4 = false
c4 = true
debugPrint(a4 || b4 && c4)

//d - 2. jó megoldás
a4 = false
b4 = true
c4 = true
debugPrint(a4 || b4 && c4)

//d - 3. jó megoldás. Van még jó megoldás. Ha a debugprint true-t ír ki, akkor jó a megoldásod
a4 = true
b4 = true
c4 = false
debugPrint(a4 || b4 && c4)
//debugPrint(false || false && true || false)

//e - 1. jó megoldás
a4 = true
b4 = false
c4 = true
debugPrint(a4 || (b4 && c4))

//e - 2. jó megoldás
a4 = false
b4 = true
c4 = true
debugPrint(a4 || (b4 && c4))

//e - 3. jó megoldás. Van még jó megoldás. Ha a debugprint true-t ír ki, akkor jó a megoldásod
a4 = true
b4 = true
c4 = false
debugPrint(a4 || (b4 && c4))

//f - 1. jó megoldás
a4 = true
b4 = false
c4 = true
debugPrint((a4 || b4) && c4)

//f - 2. jó megoldás
a4 = false
b4 = true
c4 = true
debugPrint((a4 || b4) && c4)

//f - 3. jó megoldás - NINCS TÖBB jó megoldás
a4 = true
b4 = true
c4 = true
debugPrint((a4 || b4) && c4)

//g - 1. jó megoldás
a4 = false
b4 = true
c4 = true
debugPrint(!a4 && (b4 || !c4))

//g - 2. jó megoldás
a4 = false
b4 = true
c4 = false
debugPrint(!a4 && (b4 || !c4))

//g - 3. jó megoldás - NINCS TÖBB jó megoldás
a4 = false
b4 = false
c4 = false
debugPrint(!a4 && (b4 || !c4))

/*
 5. FELADAT:
    Hozz létre két Int típusú változót, a5 és b5 néven, és úgy adj nekik értéket, hogy a következő logikai kifejezések igazak legyenek:
 */

var a5 = 0
var b5 = 0

//a
a5 = 2
b5 = 2
debugPrint(a5 == b5)

//b
a5 = 1
b5 = 23
debugPrint(a5 < b5)

//c
a5 = 26
b5 = 13
debugPrint(a5 > b5)

//d
a5 = 5
b5 = 4
debugPrint(a5 != b5)

//e
a5 = 7
b5 = 7
debugPrint(a5 <= b5)

//f
a5 = 9
b5 = 6
debugPrint(a5 >= b5)

/*
 6. FELADAT:
    Hozz létre két Int típusú változót a6 és b6 néven, valamint egy Bool típusú változót c6 néven. Válaszd meg ezek értékeit úgy, hogy a következő kifejezések igazak legyenek:
 */
var a6 = 0
var b6 = 0
var c6 = true

//a
a6 = 83
b6 = 83
c6 = false
debugPrint((a6 == b6) && !c6)

//b
a6 = -201
b6 = -10
c6 = true
debugPrint((a6 < b6) || c6)

//c
a6 = 35
b6 = -12
c6 = true
debugPrint(!(a6 == b6) && c6)

//d
a6 = 3
b6 = 1
c6 = true
debugPrint(!c6 || !(a6 <= b6))

/*
 7. FELADAT:
    Hozz létre egy Int tömböt, és egy max7 nevű Int változót. Írj egy ciklust, ami max7 változóba teszi a tömbben lévő legnagyobb értéket
 */

var array7 = [3,5,7,3,5,7,8,10,34,2,5,7]
var max7 : Int = array7[0]
for loopIndex in stride(from: 0, to: array7.count, by: 1) {
    if(max7 < array7[loopIndex]) {
        max7 = array7[loopIndex]
    }
}
debugPrint(max7)

/*
 8. FELADAT:
    Hozz létre egy Int tömböt, és egy min8 nevű Int változót. Írj egy ciklust, ami min8 változóba teszi a tömbben lévő legkisebb értéket.
 */
var array8 = [3,5,7,3,5,7,8,10,34,2,5,7]
var min8 : Int = array8[0]
for loopIndex in stride(from: 0, to: array8.count, by: 1) {
    if(min8 > array8[loopIndex]) {
        min8 = array8[loopIndex]
    }
}
debugPrint(min8)

/*
 9. FELADAT:
    Írj egy programot, ami kiírja az első n darab páros számot, majd azok összegét a konzolra
 */
var sum9 = 0
var n9 = 12
var currentNumber9 = 0
for i in stride(from: 0, to: n9, by: 1) {
    debugPrint(currentNumber9)
    sum9 = sum9 + currentNumber9
    currentNumber9 = currentNumber9 + 2
}
debugPrint("Az első \(n9) páros szám összege: \(sum9)")

/*
 10. FELADAT:
    Hozz létre egy Double tömböt, és egy ciklus segítségével határozd meg az elemeinek átlagát
 */
var numbers10 = [3.22, 5.43, 65.2, 23.123, 4.234, 1.5]
var sum = 0.0
for i in stride(from: 0, to: numbers10.count, by: 1) {
    sum = sum + numbers10[i]
}
var avg10 = sum / Double(numbers10.count)
debugPrint(avg10)

/*
 11. FELADAT:
    Írj egy ciklust, ami kiírja az összes olyan kétjegyű számot, aminek mindkét számjegye azonos (11, 22, 33, stb. )
 */

for i in stride(from: 11, through: 99, by: 11) {
    debugPrint(i)
}

/*
 12. Feladat:
    Hozz létre Int tömböt, és egy Int változót element12 névvel. Írj egy ciklust, ami a futása végén megadja, hogy element12 hányadik indexen található (először) a tömbben. Ha nincs olyan érték a tömbben, azt is jelezze a program. Ha megtalálta a program a keresett indexet, ne fusson tovább a ciklus.
 */

var array12 = [1,2,3,2,4,5,6,7,9,10,11]
var element12 = 12
for i in stride(from: 0, to: array12.count, by: 1) {
    if (array12[i] == element12) {
        debugPrint("A keresett index: \(i)")
        break
    }
    
    if(i == array12.count - 1) {
        debugPrint("A keresett érték nincs a tömbben")
    }
}

/*
 13. Feladat
 Hozz létre egy Int tömböt, és egy max7 nevű Int változót. Írj egy ciklust, ami max13 változóba teszi a tömbben lévő legnagyobb értéket
 */
var array13 = [3,5,7,3,5,7,8,10,34,2,5,7]
var max13 : Int = array7[0]
var loopIndex13 = 0
while (loopIndex13 < array13.count) {
    if(max13 < array13[loopIndex13]) {
        max13 = array13[loopIndex13]
    }
    loopIndex13 = loopIndex13 + 1
}
debugPrint(max13)

/*
 14. Feladat
 Hozz létre egy Int tömböt, és egy min14 nevű Int változót. Írj egy ciklust, ami min8 változóba teszi a tömbben lévő legkisebb értéket.
 */
var array14 = [3,5,7,3,5,7,8,10,34,2,5,7]
var min14 : Int = array7[0]
var loopIndex14 = 0
while (loopIndex14 < array14.count) {
    if(min14 > array14[loopIndex14]) {
        min14 = array14[loopIndex14]
    }
    loopIndex14 = loopIndex14 + 1
}
debugPrint(min14)


/*
 15. Feladat
 Írj egy programot, ami kiírja az első n darab páros számot, majd azok összegét a konzolra
 */
var n15 = 5
var evenNumber = 0
var sumOfEvenNumbers = 0
while (n15 > 0) {
    sumOfEvenNumbers = sumOfEvenNumbers + evenNumber
    debugPrint(evenNumber)
    evenNumber = evenNumber + 2
    n15 = n15 - 1
}
debugPrint(sumOfEvenNumbers)

/*
 16. Feladat
 Hozz létre egy Double tömböt, és egy ciklus segítségével határozd meg az elemeinek átlagát
 */
var numbers16 = [3.22, 5.43, 65.2, 23.123, 4.234, 1.5]
var sum16 = 0.0
var loopIndex16 = 0
while (loopIndex16 < numbers16.count) {
    sum16 = sum16 + numbers16[loopIndex16]
    loopIndex16 = loopIndex16 + 1
}
var avg16 = sum / Double(numbers10.count)
debugPrint(avg16)


/*
 17. Feladat
 Írj egy ciklust, ami kiírja az összes olyan kétjegyű számot, aminek mindkét számjegye azonos (11, 22, 33, stb. )
 */
var loopIndex17 = 11
while (loopIndex17 <= 99) {
    debugPrint(loopIndex17)
    loopIndex17 = loopIndex17 + 11
}

/*
 18. Feladat
 Hozz létre Int tömböt, és egy Int változót element18 névvel. Írj egy ciklust, ami a futása végén megadja, hogy element18 hányadik indexen található (először) a tömbben. Ha nincs olyan érték a tömbben, azt is jelezze a program. Ha megtalálta a keresett értéket a program, ne fusson tovább a ciklus.
 Példa: var array18 = [1,3,5,4,7,8] és var element18 = 5 esetén azt kell kiírja a program, hogy „2”. Ha element18 értéke 10 lenne, ezt kell kiírja a program: „Nincs ilyen elem a tömbben”
 */
var array18 = [1,2,3,2,4,5,6,7,9,10,11]
var element18 = 2
var loopIndex18 = 0
while (loopIndex18 < array18.count) {
    if (array18[loopIndex18] == element18) {
        debugPrint("A keresett index: \(loopIndex18)")
        break
    }
    
    if(loopIndex18 == array18.count - 1) {
        debugPrint("A keresett érték nincs a tömbben")
    }

    loopIndex18 = loopIndex18 + 1
}

/*
 19. Feladat
 Adott a következő tömb: var array19 = [1,2,3,4,2,3,2,3,4,1,2,3,4,3,1,2,3,4,1,2,4]. Hozzátok létre  a következő dictionaryt: var dict19 = [1 : 0, 2 : 0, 3 : 0, 4 : 0] Egy ciklus segítségével töltsétek fel adatokkal ezt a dictionaryt úgy, hogy a kulcs értéke azt mutassa, hogy az adott kulcs (Int típusú szám) hányszor fordul elő array19-ben.
 */
var array19 = [1,2,3,4,2,3,2,3,4,1,2,3,4,3,1,2,3,4,1,2,4]
var dict19 = [1 : 0, 2 : 0, 3 : 0, 4 : 0]

for i in stride(from: 0, to: array19.count, by: 1) {
    var actualNumber = array19[i] //A tömb i-edik eleme
    //Mivel actualNumber egy teljesen valid (azaz helyes) kulcsa a szótárnak, ezért
    //A szótár actualNumber kulcs alatt lévő értékét növelem eggyel
    dict19[actualNumber] = dict19[actualNumber]! + 1
}
debugPrint(dict19)

/*
 20. Feladat
 Van két tömbötök. Az egyikben személyi számokat, a másikban neveket tároltok, az azonos indexű elemek összetratoznak. Tehát a 0 indexű személyi szám a 0 indexű névhez tartozik. Hozzatok létre egy dictionaryt a két tömbből, ahol a személyi számok a kulcsok, az értékeik pedig a nevek.
 */
var ids = [4535,4235,6243,4356]
var names = ["Tom", "Alex", "Bob", "Ray"]
var idsAndNames : [Int : String] = [:]
for i in stride(from: 0, to: ids.count, by: 1) {
    var idAtIndex = ids[i]  //Az indexedik személyiszám
    var nameAtIndex = names[i] //Az indexedik név
    idsAndNames[idAtIndex] = nameAtIndex //idAtIndex egy valid kulcs a szótárnak, és nameAtIndex egy valid érték neki így egyszerű értékadással lényeglben megoldható a feladat
}
debugPrint(idsAndNames)

/*
 21. Feladat
 Hozz létre egy olyan dictionaryt, aminek a kulcsa Int típusú, és a kulcs értéke azt mutatja, hogy az értékéül szolgáló tömbben 1-től kulcs-ig vannak a számok. A dictionary kulcsai 0-tól n-ig mennek. Ti adjátok meg n-t, a programnak bármilyen n>=0 értékre jó kimenetet kell adni.
 Példa:
 n = 3
 kimeneti dictionary: [0 : [], 1:[1], 2:[1,2], 3:[1,2,3]]
 n = 5
 kimeneti dictionary: [0 : [], 1:[1], 2:[1,2], 3:[1,2,3], 4:[1,2,3,4], 5:[1,2,3,4,5]]
 */

var resultDictionary : [Int : [Int]] = [:]
var n21 = 3 //Hányadik számig szeretnénk létrehozni a dictionaryt
while (n21 >= 0) { //Amíg n21 értéke el nem éri a nullát
    var arrayAtN : [Int] = [] //létrehozok egy üres tömböt
    for i in stride(from: 1, through: n21, by: 1) { //Amit feltöltök elemekkel nullától n21-ig. n21 értéke iterációnként változik (csökken!, nem nő)
        arrayAtN.append(i) //Hozzáadom arrayAtN-hez az adott számot. Sima tömb feltöltés ciklussal
    }
    resultDictionary[n21] = arrayAtN //A szótár n21 kulcsának értékül adom arrayAtN-t, ami 1 és n21 között tartalmazza az összes zsámot
    n21 = n21 - 1 //n21 értékét csökkentem
}
debugPrint(resultDictionary)

/*
 22. Feladat
 Hozzatok létre egy n*n-es 2D-s Int tömböt. (Azaz a tömbnek n eleme legyen, és az összes eleme legyen egy szintén n elemű tömb). Ciklus segítségével a következőket számoljátok ki:
 a. A tömbben lévő max, min érték
 b. A tömbben lévő értékek átlaga
 c. Dictionaryban adjátok vissza egy keresett elem indexét
 */
var array2D : [[Int]] = [[1,2,5,6,4], [5,4,1,2,3], [5,19,2,9,8,7,4]] //2D tömb létrehozása
var wantedNumber = 4    //Ezt keresem
var wantedIndexInArray : [Int : Int] = [:]
var maximum = array2D[0][0]     //első Int típusú elem, a 2D tömb abszolút első eleme
var minimum = array2D[0][0]     //első Int típusú elem, a 2D tömb abszolút első eleme
var sumOfArray = 0
var numberOfCheckedNumbers = 0
var avg : Double = 0

//Végigiterálok a tömböket tartalmazó tömbön
for i in stride(from: 0, to: array2D.count, by: 1) {
    let actualArray = array2D[i] //Ez mindig egy tömb lesz
    //Végigiterálok az aktuálus tömb összes elemén
    for j in stride(from: 0, to:actualArray.count , by: 1) {
        
        let actualNumber = actualArray[j]   //Ez mindig egy szám lesz
        numberOfCheckedNumbers = numberOfCheckedNumbers + 1 //Ebben számolom, hogy hány elemet vizsgáltam eddig. Átlagszámításhoz jól jön
        sumOfArray = sumOfArray + actualNumber //Ebbe gyűjtöm az elemek összegét
        
        //Minimum, és maximum keresés
        if(maximum < actualNumber) {
            maximum = actualNumber
        }
        if(minimum > actualNumber) {
            minimum = actualNumber
        }
        //Szótárba elem indexe, ha ez a keresett elem
        if(actualNumber == wantedNumber)  {
            wantedIndexInArray[i] = j
        }
        
        //Átlag számítása
        avg = Double(sumOfArray) / Double(numberOfCheckedNumbers)
        
        //Ha csak az utoló lépésnél akarod kiszámolni az átlagot, akkor ezt a feltételt használd
        //(i == array2D.count - 1) --> Az utolsó tömböt vizsgálod
        //(j == actualArray.count - 1) --> Az actualArray (ami az utolsó tömb) utolsó elemét vizsgálod
        //(i == array2D.count - 1) && (j == actualArray.count - 1) --> Az utolsó tömb utolsó elemét vizsgálod
        if((i == array2D.count - 1) && (j == actualArray.count - 1)) {
            avg = Double(sumOfArray) / Double(numberOfCheckedNumbers)
        }
    }
}

debugPrint("A legnagyobb szám:", maximum)
debugPrint("A legkisebb szám:", minimum)
debugPrint("Átlag:", avg)
debugPrint("A kulcs és az érték, a keresett számhoz:", wantedIndexInArray)


/*
 23. Feladat
 Készítsen egyszerű játékprogramot, amely során a játékos és a gép is dobókockával dob! A dobókockának 6 oldala van. Az nyer, aki előbb eléri a 25-t! Fontos, hogy a több, nem jobb: az eddigi dobásoknak összesen pontosan 25nek kell lennie!
 Nyilván az elején ez nem számít, tehát ha a játékosnak még csak 0 dobott értéke van, és hatost dob, akkor a dobott értékei 6-os értéket vesznek fel.
 De ha már 22 a dobott értékeinek összege, akkor például egy 4-est dobva az értékei 26-ra módosulnának: ezt nem szabad elfogadni, ilyenkor az értéket ne számolja bele!
 Aki előbb eléri a 25-t, az nyert!
 Felhasználói bemenet nincs a játékban, mindkét dobás véletlenszám-generátorral történjen!  (Köszi a feladatot Rudinak)
 Véletlenszámot így tudtok generálni 1 és 6 között:
 var randomNumber = Int(arc4random_uniform(6)) + 1
 */
var playerSum = 0       //Ebben számolom a játékos dobásainak összegét
var computerSum = 0     //Ebben számolom a gép dobásainak összegét
var numberOfRounds = 0  //Ebben számolom a játék köröket

//Amíg igaz az, hogy egyik érték sem 25, addig fut a ciklus
while((playerSum != 25) && (computerSum != 25)) {
    let randomNumber = Int(arc4random_uniform(6)) + 1
    
    if(numberOfRounds % 2 == 0) { //Páros kör --> Játékos dob
        if(playerSum + randomNumber <= 25) {    //Ha dobott szám, és a játékos eddigi dobott számainak összege kisebb, mint 25
            playerSum = playerSum + randomNumber    //Akkor adom hozzá a játékos eddig dobott számaihoz a most dobottat
        }
    }
    else {  //Páratlan kör --> Gép dob. MEGJ: elég az else, mert 2 eset van csak, és ha az első nem teljesült, akk biztos a másik igen
        if(computerSum + randomNumber <= 25) {
            computerSum = computerSum + randomNumber
        }
    }
    debugPrint("Scores: Játékos\(playerSum)---Gép\(computerSum)")
    numberOfRounds = numberOfRounds + 1 //Számolom a köröket, hogy tudjam, mikor ki jön
}

debugPrint("Játék vége, körök száma: \(numberOfRounds)")
if(playerSum == 25) {   //Ha a játkos dobott számainak az összege 25, akkor a játékos nyert
    debugPrint("Játékos nyert. (Scores: Játékos\(playerSum)---Gép\(computerSum))")
}
else {  //Egyébként biztos a gép nyert
    debugPrint("Gép nyert. (Scores: Játékos\(playerSum)---Gép\(computerSum))")
}

/*
24. Feladat
 Hozz létre egy Int típusú változót (legyen a neve sec). Ebbe adj meg egy másodperc értéket. Írj programot, ami átváltja a másodpercet órára, percre, másodperce, majd ezt kiírja a konzolra.
 például:
 var sec = 260 esetén ezt írd a kimenetre: 04:20
 var sec = 3940 esetén ezt írd a kimenetre: 01:05:40
 (ha az 1 számjeggyel jelenítitek meg az óra/perc értéket, az is ok (Pl.: 1:5:40), de próbáljátok meg 2 száámjeggyel megjeleníteni)
 */

var sec = 26248

let hours = sec / 3600 //Az órákat az határozza meg, hogy hányszor van meg maradék nélkül 3600 a sec-ben. (1 óra = 3600 másodper)
sec = sec - hours*3600 //sec értékét csökkentem az órák száma * 3600-zal --> sec értéke már csak a pefc-másodperc infót tartalmazza
    
let minutes = sec / 60 //A paerceket az határozza meg, hogy hányszor van meg maradék nélkül 60 a sec-ben. (1 perc = 60 másodperc)
sec = sec - minutes*60 //sec értékét csükkentem a percek száma * 60-nal --> sec értéke már csak a másodperceket fogja tartalmazni
    
/*
2 digiten ábrázoláshoz Google keresés kulcssszó: swift show numbers min on 2 digits
link: https://stackoverflow.com/questions/25566581/leading-zeros-for-int-in-swift
*/
var timeString = ""
if(hours == 0) {    //Ha nincs óra a megjelenítendő időben
    timeString = String(format: "%02d:%02d", minutes, sec)
}
else {  //Ha órát is megjelenítünk
    timeString = String(format: "%02d:%02d:%02d", hours, minutes, sec)
}


debugPrint("\(sec) másodperc ábrázolva: \(timeString)")





