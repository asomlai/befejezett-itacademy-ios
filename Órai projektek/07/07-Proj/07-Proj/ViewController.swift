//
//  ViewController.swift
//  07-Proj
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        label.text = "Number of elements in array: ?"
        textView.text = "Ide írasd majd ki a json-ben lévő elemeket szövegesen"
        textView.isEditable = false
        getCoordinateData(completionHandler: dataReadyToUse(data:))
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func dataReadyToUse(data : [[String : Any]]) {
        
    }
    
    @IBAction func loadDattaButtonTouched(_ sender: Any) {
        
    }
    
    //Ezzel ne foglalkozzatok
    func getCoordinateData(completionHandler : (_ data : [[String : Any]]) -> Void) {
        if let filePath = Bundle.main.path(forResource: "chartData", ofType: "json") {
            let path=URL(fileURLWithPath: filePath)
            do {
                let fileContent = try Data(contentsOf: path)
                if let jsonArray = try JSONSerialization.jsonObject(with: fileContent, options : .allowFragments) as? [[String : Any]] {
                    completionHandler(jsonArray)
                } else {
                    print("bad json")
                }
            }
            catch {
                debugPrint(error.localizedDescription)
            }
        }
    }
    
}

