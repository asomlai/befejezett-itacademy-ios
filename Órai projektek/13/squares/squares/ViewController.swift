//
//  ViewController.swift
//  squares
//
//  Created by Andras Somlai on 2018. 05. 03..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let view1 = UIView()
    let view2 = UIView()
    let view3 = UIView()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.addSubview(view1)
        self.view.addSubview(view2)
        self.view.addSubview(view3)
        
        view1.backgroundColor = #colorLiteral(red: 0.2392156869, green: 0.6745098233, blue: 0.9686274529, alpha: 1)
        view2.backgroundColor = UIColor.orange
        view3.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        view1.frame = CGRect(x:0, y:0, width: self.view.frame.width*0.3, height: self.view.frame.height*0.3)
        
        view2.frame = CGRect(x:view1.frame.width, y:0, width: view1.frame.width*0.5, height: view1.frame.height*0.5)
        view3.frame = CGRect(x:0, y:view1.frame.height, width: view1.frame.width*0.5, height: view1.frame.height*0.5)
        
        
    }

}

