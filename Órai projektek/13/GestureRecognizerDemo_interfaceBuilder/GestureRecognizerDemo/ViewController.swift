//
//  ViewController.swift
//  GestureRecognizerDemo
//
//  Created by Andras Somlai on 2018. 05. 01..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var pinchLabel: UILabel!
    @IBOutlet weak var panLabel: UILabel!
    
    var isLongPressInProgress : Bool = false
    
    @IBAction func rotate(_ sender: UIRotationGestureRecognizer) {
        debugPrint("rotate: \(sender.rotation)")
    }
    @IBAction func pan(_ sender: UIPanGestureRecognizer) {
        debugPrint("pan in parent: \(sender.location(in: sender.view))")
        debugPrint("pan in VC: \(sender.location(in: self.view))")
        
        debugPrint("Pan state: \(sender.state.rawValue)")
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Tagban jegyzem meg az utoljára használt fontméretet
        pinchLabel.tag = 16
        
        //UIScreenEdgePanGestureRecognizer-t nem sikerült hozzáadnom storyboardból.
        //Ha nektek megy, írjátok meg, hogyan
        let rightEdgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        rightEdgePan.edges = .right
        view.addGestureRecognizer(rightEdgePan)
        
        let leftEdgePan = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(screenEdgeSwiped))
        leftEdgePan.edges = .left
        view.addGestureRecognizer(leftEdgePan)
    }

    
    @objc func screenEdgeSwiped(_ recognizer: UIScreenEdgePanGestureRecognizer) {
        print("Screen edge swiped!")
        switch recognizer.edges {
        case .left: view.backgroundColor = UIColor.white
        case .right: view.backgroundColor = UIColor.black
        default: break
        }
    }

    @IBAction func tap(_ sender: UITapGestureRecognizer) {
        if (sender.view!.tag == 0) {
            sender.view?.backgroundColor = UIColor.groupTableViewBackground
            sender.view!.tag = 1
        }
        else {
            sender.view?.backgroundColor = UIColor.lightGray
            sender.view!.tag = 0
        }
    }
    
    
    @IBAction func pinch(_ sender: UIPinchGestureRecognizer) {
        let lastUsedFontSize = CGFloat(pinchLabel.tag)
        var newFontSize = lastUsedFontSize
        debugPrint("scale:\(sender.scale)")
        if(sender.scale > 1) {
            newFontSize = lastUsedFontSize+5*sender.scale
        }
        else if(sender.scale < 1) {
            newFontSize = lastUsedFontSize - 5/sender.scale
        }
        pinchLabel.font = UIFont.systemFont(ofSize: newFontSize)
        
        //Ha befejeződött a gesztus
        if (sender.state == .ended) {
            pinchLabel.tag = Int(newFontSize)
            return
        }
    }
    
    /*
    @IBAction func rotation(_ sender: UIRotationGestureRecognizer) {
        debugPrint("rotation: \(sender.rotation)")
        let lastAngle = CGFloat(sender.view!.tag)/1000
        if (sender.state == .ended) {
            sender.view!.tag = Int((lastAngle + sender.rotation)*1000)
            return
        }
        debugPrint("rotation: \(sender.rotation)---\(lastAngle)")
        sender.view!.transform = CGAffineTransform(rotationAngle: sender.rotation + lastAngle)

    }
    
    @IBAction func pan(_ sender: UIPanGestureRecognizer) {
        if(sender.state == .ended) {
            panLabel.center = CGPoint(x:sender.view!.frame.width / 2, y: sender.view!.frame.height/2)
            return
        }
        panLabel.center = sender.location(in: sender.view!)
        sender.view!.layoutIfNeeded()
    }
    */
 
    @IBAction func swipe(_ sender: UISwipeGestureRecognizer) {
        switch sender.direction {
        case .left: sender.view!.backgroundColor = UIColor.red
        case .right: sender.view!.backgroundColor = UIColor.blue
        case .up: sender.view!.backgroundColor = UIColor.green
        case .down: sender.view!.backgroundColor = UIColor.lightGray
        default: sender.view!.backgroundColor = UIColor.black
        }
    }
    
    @IBAction func longPress(_ sender: UILongPressGestureRecognizer) {
        debugPrint("longPress: \(sender.location(in: sender.view))")
        if(sender.state == .began) {
            sender.view?.backgroundColor = UIColor.groupTableViewBackground
            isLongPressInProgress = true
            
            
            
            Timer.scheduledTimer(timeInterval: 0.05,
                                 target: self,
                                 selector: #selector(timer(sender:)),
                                 userInfo: ["red" : CGFloat(0.0), "actionView" : sender.view!],
                                 repeats: false)
        }
        else if(sender.state == .ended) {
            sender.view?.backgroundColor = UIColor.lightGray
            isLongPressInProgress = false
        }
    }

    @objc func timer(sender : Timer) {
        
        if(isLongPressInProgress == false) {
            sender.invalidate()
            debugPrint("Elengedtem")
            return
        }
        
        let userInfoDict = sender.userInfo as! [String : Any]
        let redComponents = userInfoDict["red"] as! CGFloat
        let parentView = userInfoDict["actionView"] as! UIView

        parentView.backgroundColor = UIColor(red:redComponents ,green:0, blue:0, alpha:1.0)
        debugPrint(redComponents)
        var newUserInfo : [String : Any] = [:]
        newUserInfo["actionView"] = parentView
        newUserInfo["red"] = min(1, redComponents + 0.01)
        
        Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(timer(sender:)), userInfo: newUserInfo, repeats: false)
        debugPrint("Fogom")
        
        
    }
}

