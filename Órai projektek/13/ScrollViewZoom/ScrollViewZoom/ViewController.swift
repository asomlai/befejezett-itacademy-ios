//
//  ViewController.swift
//  ScrollViewZoom
//
//  Created by Andras Somlai on 2018. 05. 01..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {

    @IBOutlet weak var upperScrollView: UIScrollView!
    @IBOutlet weak var lowerScrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Kép nagyítása
        upperScrollView.delegate = self;
        upperScrollView.minimumZoomScale = 0.5
        upperScrollView.maximumZoomScale = 10
        
        let image = UIImage(named: "AppleHeadquarter")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = #colorLiteral(red: 0.8078431487, green: 0.02745098062, blue: 0.3333333433, alpha: 1)
        upperScrollView.addSubview(imageView)
        
        
        //View nagyítása, amikhez van hozzáadva subview
        lowerScrollView.delegate = self;
        lowerScrollView.minimumZoomScale = 1
        lowerScrollView.maximumZoomScale = 10
        
        let containerView = UIView()
        let label = UILabel()
        label.text = "Hello, type your name"
        label.tag = 1
        containerView.addSubview(label)
        
        let textField = UITextField()
        textField.placeholder = "here"
        textField.tag = 2
        containerView.addSubview(textField)
        
        lowerScrollView.addSubview(containerView)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        //kép frame beállítása
        upperScrollView.subviews[0].frame = upperScrollView.bounds
        
        //View, és a hozzá tartozó subview framek beálítása
        lowerScrollView.subviews[0].frame = lowerScrollView.bounds
        let label = lowerScrollView.subviews[0].viewWithTag(1)!
        let textField = lowerScrollView.subviews[0].viewWithTag(2)!
        label.frame = CGRect(x:0,y:0,width:label.superview!.frame.width,height:30)
        textField.frame = CGRect(x:0,y:40 ,width:textField.superview!.frame.width,height:30)
    }

    //Zoomoláshoz szükséges ezt a függvényt implementálni
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return scrollView.subviews[0]
    }
    
    //Billzet eltüntetése
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

