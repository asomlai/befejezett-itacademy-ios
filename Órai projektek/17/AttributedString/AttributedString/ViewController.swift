//
//  ViewController.swift
//  AttributedString
//
//  Created by Andras Somlai on 2018. 05. 17..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var attributedLabel: UILabel!
    @IBOutlet weak var simpleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /*
         SETUP LABEL WITH ATTRIBUTED STRING
         */
        attributedLabel.backgroundColor = UIColor.brown
        attributedLabel.textColor = UIColor.cyan
        attributedLabel.font = UIFont(name: "AlNile-Bold", size:20)
        
        let myString = "My Swift Attributed String"
        let myAttrString = NSMutableAttributedString(string: myString, attributes: nil)
        
        //3. karaktertől 5 karakter színe legyen kék
        myAttrString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.blue, range: NSMakeRange(3, 5))
        
        //0. karaktertől 8 karakter háttérszíne legyen sárga
        myAttrString.addAttribute(NSAttributedStringKey.backgroundColor, value: UIColor.yellow, range: NSMakeRange(0, 8))
        
        //Az "Attributed" szövegrész legyen aláhúzva
        let attributedRange = (myAttrString.string as NSString).range(of: "SŁ")
        myAttrString.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: attributedRange)
        
        //A "String" szövegrésznek legyen árnyéka
        let myShadow = NSShadow()
        myShadow.shadowBlurRadius = 3
        myShadow.shadowOffset = CGSize(width: 3, height: 3)
        myShadow.shadowColor = UIColor.gray
        let stringTextPartRange = (myAttrString.string as NSString).range(of: "String")
        myAttrString.addAttribute(NSAttributedStringKey.shadow, value: myShadow, range: stringTextPartRange)
        
        //A 14. karaktertől 5 karakter legyen Chalkduster fonttal megjelenítve
        myAttrString.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "Chalkduster", size: 18.0)!, range: NSMakeRange(14, 4))
        attributedLabel.attributedText = myAttrString
        
        
        /*
         SETUP LABEL WITH SIMPLE STRING
         */
        let simpleString = "My Swift Simple String"
        simpleLabel.backgroundColor = UIColor.darkGray
        simpleLabel.textColor = UIColor.orange
        simpleLabel.font = UIFont(name: "AlNile-Bold", size:16)
        simpleLabel.text = simpleString
        
        
        /*
         MEGJEGYZÉS: UGYANÍGY MŰKÖDIK UITEXTVIEW ESETÉBEN IS
         */
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

