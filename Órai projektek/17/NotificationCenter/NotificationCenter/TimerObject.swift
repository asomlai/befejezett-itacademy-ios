//
//  TimerObject.swift
//  NotificationCenter
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class TimerObject {
    //1 mód a singleton objektum lekérésére
    static let sharedInstance = TimerObject()
    
//    //másik mód
//    static let sharedInstance: TimerObject = {
//        let instance = TimerObject()
//        // setup code
//        //Pl: instance.setupDefaultData()
//        return instance
//    }()
    
    var timer : Timer!
    private var milliseconds : Int = 0
    var isActive : Bool = false {
        didSet {
            var notificationObject = Notification(name: Notification.Name("timerStateChanged"))
            notificationObject.userInfo = ["hello" : "hello"]
            
            NotificationCenter.default.post(notificationObject)
            //NotificationCenter.default.post(name: Notification.Name("timerStateChanged"), object: nil)
        }
    }
    
    var secondsPart : Int {
        return (milliseconds - minutesPart*60000) / 1000
    }
    
    var minutesPart : Int {
        return milliseconds / 60000
    }
    
    var millisecondsPart : Int {
        return milliseconds % 1000
    }
    
    private init() {    }
    
    func start() {
        if(isActive) {
            return
        }
        isActive = true
        timer = Timer.scheduledTimer(timeInterval: 0.001, target: self, selector: #selector(timerFired(sender:)), userInfo: nil, repeats: true)
    }
    
    func pause() {
        isActive = false
    }
    
    func stop() {
        milliseconds = 0
        isActive = false
    }
    
    func restart() {
        stop()
        start()
    }
    
    @objc func timerFired(sender: Timer) {
        if(isActive == false) {
            sender.invalidate()
            return
        }
        
        milliseconds += 1
        NotificationCenter.default.post(name: Notification.Name("milisecondsChanged"), object: nil)
        
        if(milliseconds % 1000 == 0) {
            NotificationCenter.default.post(name: Notification.Name("secondsChanged"), object: nil)
        }
        
        if(milliseconds % 60000 == 0) {
            NotificationCenter.default.post(name: Notification.Name("minutesChanged"), object: nil)
        }
        
    }
}
