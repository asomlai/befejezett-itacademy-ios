//
//  ViewController.swift
//  NotificationCenter
//
//  Created by Andras Somlai on 2018. 05. 15..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var millisecondsLabel : UILabel!
    @IBOutlet weak var secondsLabel : UILabel!
    @IBOutlet weak var minutesLabel : UILabel!
    
    @IBOutlet weak var restartButton : UIButton!
    @IBOutlet weak var pauseButton : UIButton!
    @IBOutlet weak var stopButton : UIButton!
    @IBOutlet weak var startButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateButtons()
        
        NotificationCenter.default.addObserver(self, selector: #selector(timerStateChanged(notification:)), name: Notification.Name("timerStateChanged"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(minutesChanged(notification:)), name: Notification.Name("minutesChanged"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(secondsChanged(notification:)), name: Notification.Name("secondsChanged"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(millisecsChanged(notification:)), name: Notification.Name("milisecondsChanged"), object: nil)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }

    @IBAction func buttonPressed(sender: UIButton) {
        switch sender.tag {
        case 1: TimerObject.sharedInstance.start()
        case 2: TimerObject.sharedInstance.pause()
        case 3: TimerObject.sharedInstance.stop()
        case 4: TimerObject.sharedInstance.restart()
        default: break
        }
    }

    @objc func timerStateChanged(notification: Notification) {
        let userInfo = notification.userInfo
        updateLabels()
        updateButtons()
    }
    
    @objc func minutesChanged(notification: Notification) {
        let userInfo = notification.userInfo
        highlightLabel(label: minutesLabel)
    }
    
    @objc func secondsChanged(notification: Notification) {
        highlightLabel(label: secondsLabel)
    }
    
    @objc func millisecsChanged(notification: Notification) {
        updateLabels()
    }
    
    func highlightLabel(label: UILabel) {
        DispatchQueue.main.async {
            label.backgroundColor = UIColor.red
            Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(self.uiUpdateTimerFired(sender:)), userInfo: label.tag, repeats: false)
        }
    }
    
    func updateLabels() {
        DispatchQueue.main.async {
            self.millisecondsLabel.text = String(TimerObject.sharedInstance.millisecondsPart)
            self.secondsLabel.text = String(TimerObject.sharedInstance.secondsPart)
            self.minutesLabel.text = String(TimerObject.sharedInstance.minutesPart)
        }
    }

    func updateButtons() {
        DispatchQueue.main.async {
            self.stopButton.isEnabled = TimerObject.sharedInstance.isActive
            self.pauseButton.isEnabled = TimerObject.sharedInstance.isActive
            self.restartButton.isEnabled = TimerObject.sharedInstance.isActive
            self.startButton.isEnabled = !TimerObject.sharedInstance.isActive
        }
    }
    
    @objc func uiUpdateTimerFired(sender: Timer) {
        let viewTagToUpdate = sender.userInfo as! Int
        DispatchQueue.main.async {
            switch viewTagToUpdate {
            case 1: self.secondsLabel.backgroundColor = UIColor.clear
            case 2: self.minutesLabel.backgroundColor = UIColor.clear
            default : break
            }
            //self.view.viewWithTag()!.backgroundColor = UIColor.clear
        }
    }

}
