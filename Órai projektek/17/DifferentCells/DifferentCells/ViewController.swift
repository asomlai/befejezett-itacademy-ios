//
//  ViewController.swift
//  DifferentCells
//
//  Created by Andras Somlai on 2018. 05. 17..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    private let actionNames = ["Start", "Pause", "Stop", "Restart"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 + actionNames.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //0. sorban van a timer cell
        if(indexPath.row == 0) {
            return getTimerCell(indexPath: indexPath)
        }
        //A többi sorban vannak a gombok
        else {
            return getActionCell(indexPath: indexPath)
        }
    }
    
    func getTimerCell(indexPath: IndexPath) -> UITableViewCell{
        var cell = tableView.dequeueReusableCell(withIdentifier: "timerCell") as? TimerCell
        if(cell == nil) {
            cell = TimerCell(style: .default, reuseIdentifier: "timerCell")
        }
        return cell!
    }

    func getActionCell(indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "actionCell") as? ActionCell
        if(cell == nil) {
            cell = ActionCell(style: .default, reuseIdentifier: "actionCell")
        }
        
        //A cella sorának megfelelő action string beállítása
        //indexPath.row-1-ik elem kell, mert a tableView elején van egy másik sor.
        cell!.actionName = actionNames[indexPath.row-1]
        return cell!
    }
}

