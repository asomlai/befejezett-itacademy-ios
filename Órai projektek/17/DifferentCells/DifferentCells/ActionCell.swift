//
//  ActionCell.swift
//  DifferentCells
//
//  Created by Andras Somlai on 2018. 05. 17..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ActionCell: UITableViewCell {
    var actionName : String! {
        didSet {
            actionButton.setTitle(actionName, for: .normal)
        }
    }
    
    @IBOutlet private weak var actionButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
