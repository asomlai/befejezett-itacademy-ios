//
//  KeyboardViewController.swift
//  MyKeyboard
//
//  Created by Andras Somlai on 2018. 06. 07..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class KeyboardViewController: UIInputViewController {

    let nextKeyboardButton = UIButton(type: .system)
    let inputButton = UIButton(type: .system)
    let backspaceButton = UIButton(type: .system)
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        // Add custom view sizing constraints here
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Perform custom UI setup here
        nextKeyboardButton.setTitle("Next Keyboard", for: .normal)
        nextKeyboardButton.addTarget(self, action: #selector(handleInputModeList(from:with:)), for: .touchUpInside)
        view.addSubview(nextKeyboardButton)
        
        inputButton.setTitle("Type 'A'", for: .normal)
        inputButton.addTarget(self, action: #selector(type(sender:)), for: .touchUpInside)
        view.addSubview(inputButton)
        
        backspaceButton.setTitle("Backspace", for: .normal)
        backspaceButton.addTarget(self, action: #selector(backspace(sender:)), for: .touchUpInside)
        view.addSubview(backspaceButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        nextKeyboardButton.frame = CGRect(x:5, y:view.frame.height-35, width:view.frame.width/4, height:30)
        inputButton.frame = CGRect(x:view.frame.width / 2 - view.frame.width/8, y:view.frame.height-35, width:view.frame.width/4, height:30)
        backspaceButton.frame = CGRect(x:view.frame.width*3/4 - 5, y:view.frame.height-35, width:view.frame.width/4, height:30)
    }
    
    override func textWillChange(_ textInput: UITextInput?) {
        debugPrint("textWillChange")
        // The app is about to change the document's contents. Perform any preparation here.
    }
    
    override func textDidChange(_ textInput: UITextInput?) {
        // The app has just changed the document's contents, the document context has been updated.
        debugPrint("textDidChange")
        
        var textColor: UIColor
        let proxy = self.textDocumentProxy
        if proxy.keyboardAppearance == UIKeyboardAppearance.dark {
            textColor = UIColor.white
        } else {
            textColor = UIColor.black
        }
        self.nextKeyboardButton.setTitleColor(textColor, for: [])
    }
    
    @objc func type(sender: UIButton) {
        textDocumentProxy.insertText("A")
    }
    
    @objc func backspace(sender: UIButton) {
        textDocumentProxy.deleteBackward()
    }

}
