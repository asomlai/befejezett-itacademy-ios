//
//  Stopper.swift
//  UnitTestDemo
//
//  Created by Andras Somlai on 2018. 06. 12..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class Stopper {
    var timer : Timer?
    var counter = 0
    
    func start() {
        timer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: timerFired)
    }
    
    func stop() {
        if(timer != nil) {
            timer!.invalidate()
            timer = nil
            counter = 0
        }
    }
    
    func timerFired(sender : Timer) {
        counter += 1
    }
    
    func getPrettyTime() -> String {
        return getPrettyTime(number: counter)
    }
    
    func getPrettyTime(number: Int) -> String {
        if(number < 10) {
            return "0\(number)"
        }
        return ""
    }
}
