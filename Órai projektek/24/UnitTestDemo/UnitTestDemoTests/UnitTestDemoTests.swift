//
//  UnitTestDemoTests.swift
//  UnitTestDemoTests
//
//  Created by Andras Somlai on 2018. 06. 12..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import XCTest
@testable import UnitTestDemo

class UnitTestDemoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testStopperGetPrettyTime() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let stopper = Stopper()
        XCTAssertEqual("01", stopper.getPrettyTime(number: 1))
        XCTAssertNotEqual("00", stopper.getPrettyTime(number: 1))
        
       
    }
    
    func testStopperState() {
        let stopper = Stopper()
        
        XCTAssertNil(stopper.timer)
        XCTAssertEqual(0, stopper.counter)
        
        stopper.start()
    
        XCTAssertNotNil(stopper.timer)
       
        
        stopper.stop()
        XCTAssertNil(stopper.timer)
        XCTAssertEqual(0, stopper.counter)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
