//
//  ViewController.swift
//  UITestDemo
//
//  Created by Andras Somlai on 2018. 06. 12..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var buttonTappedLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonTappedLabel.isHidden = true
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func pressMePressed(_ sender: UIButton) {
        sender.isHidden = true
        buttonTappedLabel.isHidden = false
        
    }
    
}

