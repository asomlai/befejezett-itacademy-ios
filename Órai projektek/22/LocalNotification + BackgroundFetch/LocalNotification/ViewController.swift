//
//  ViewController.swift
//  LocalNotification
//
//  Created by Andras Somlai on 2018. 06. 05..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI //framework to customize the notification

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Request Authorization
        UNUserNotificationCenter.current().requestAuthorization (options: [.alert, .sound, .badge]) { accepted, error in
            if (!accepted) {
                print("Notification access denied.")
            }
        }

        //Create notificaation object
        let content = UNMutableNotificationContent()
        content.title = "Title"
        content.subtitle = "Subtitle"
        content.body = "A Lorem Ipsum egy egyszerû szövegrészlete, szövegutánzata a betûszedõ és nyomdaiparnak. A Lorem Ipsum az 1500-as évek óta standard szövegrészletként szolgált az iparban; mikor egy ismeretlen nyomdász összeállította a betûkészletét és egy példa-könyvet vagy szöveget nyomott papírra, ezt használta. Nem csak 5 évszázadot élt túl, de az elektronikus betûkészleteknél is változatlanul megmaradt. Az 1960-as években népszerûsítették a Lorem Ipsum részleteket magukbafoglaló Letraset lapokkal, és legutóbb softwarekkel mint például az Aldus Pagemaker."
        content.sound = UNNotificationSound.default()
        content.badge = 1
        
        
        //To Present image in notification
        if let path = Bundle.main.path(forResource: "monkey", ofType: "png") {
            let url = URL(fileURLWithPath: path)
            do {
                let attachment = try UNNotificationAttachment(identifier: "sampleImage", url: url, options: nil)
                content.attachments = [attachment]
            }
            catch {
                print("attachment not found.")
            }
        }
        
        //Gomb hozzáadása
        let snoozeAction = UNNotificationAction(
            identifier: "buttonAction",
            title: "Magical button",
            options: [.foreground])
        
        let snoozeCategory = UNNotificationCategory(
            identifier: "myNotificationCategory",
            actions: [snoozeAction],
            intentIdentifiers: [],
            options: [])
        content.categoryIdentifier = "myNotificationCategory"
        
        UNUserNotificationCenter.current().setNotificationCategories([snoozeCategory])
        
        //Set notification time
        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5.0, repeats: false) //5 seconds from now
        
        //Add notification to system
        let request = UNNotificationRequest(identifier:"requestIdentifier", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request)
        
    }
}



