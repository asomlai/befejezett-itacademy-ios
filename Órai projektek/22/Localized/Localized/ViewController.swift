//
//  ViewController.swift
//  Localized
//
//  Created by Andras Somlai on 2018. 06. 05..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func changeLanguage(_ sender: UIButton) {
        sender.tag += 1
        UserDefaults.standard.removeObject(forKey: "AppleLanguages")
        
        var language = "en"
        switch sender.tag % 3 {
        case 0: language = "en"
        case 1: language = "de"
        case 2: language = "hu"
        default: break
        }
        
        //language = "kr"
        UserDefaults.standard.set([language], forKey: "AppleLanguages")
        UserDefaults.standard.synchronize()
        
        let alertController = UIAlertController(title: "New language", message:language, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
}

