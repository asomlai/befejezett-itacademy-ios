//
//  Extensions.swift
//  Localized
//
//  Created by Andras Somlai on 2018. 06. 05..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

extension String {
    
    func localized(lang : String) -> String? {
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        let bundle = Bundle(path: path!)
        return bundle?.localizedString(forKey: self, value: nil, table: nil)
    }
    
}
