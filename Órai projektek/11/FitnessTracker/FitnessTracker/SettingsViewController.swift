//
//  SettingsViewController.swift
//  FitnessTracker
//
//  Created by Andras Somlai on 2018. 04. 26..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    var bodyData : BodyData!
    
    @IBOutlet weak var genderSelector: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }

    func updateUI() {
        if (bodyData.gender != nil) {
            genderSelector.selectedSegmentIndex = bodyData.gender == .female ? 0 : 1
        }
        else {
            genderSelector.selectedSegmentIndex = -1
        }
    }

    @IBAction func genderSelected(_ sender: UISegmentedControl) {
        bodyData.gender = sender.selectedSegmentIndex == 0 ? .female : .male
    }
    
    @IBAction func clearAppData(_ sender: UIButton) {
        UserDefaults.standard.removeObject(forKey: "savedData")
        bodyData.clear()
        updateUI()
    }
    
    @IBAction func rateApp(_ sender: UIButton) {
        let ratePanel = Bundle.main.loadNibNamed("RatePanel", owner: nil, options: nil)![0] as! RatePanel
        ratePanel.frame = CGRect(x:0, y:0, width:300, height:250)
        ratePanel.center = view.center
        view.addSubview(ratePanel)
    }
    
}
