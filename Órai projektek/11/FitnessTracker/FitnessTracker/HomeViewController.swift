//
//  ViewController.swift
//  FitnessTracker
//
//  Created by Andras Somlai on 2018. 04. 26..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    @IBOutlet weak var bodyHeight: UITextField!
    @IBOutlet weak var bodyWeight: UITextField!
    @IBOutlet weak var chest: UITextField!
    @IBOutlet weak var stomach: UITextField!
    @IBOutlet weak var rightThigh: UITextField!
    @IBOutlet weak var leftThigh: UITextField!
    @IBOutlet weak var rightBiceps: UITextField!
    @IBOutlet weak var leftBiceps: UITextField!
    
    @IBOutlet weak var lastUpdatedLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    
    var bodyData : BodyData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (UserDefaults.standard.dictionary(forKey: "savedData") != nil) {
            bodyData = BodyData(dictionary: UserDefaults.standard.dictionary(forKey: "savedData")!)
        }
        else {
            bodyData = BodyData()
        }
        
        let tap = UITapGestureRecognizer()
        tap.addTarget(self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUIData()
    }

    func updateUIData() {
        (bodyData.bodyHeight != nil) ? (bodyHeight.text = String(bodyData.bodyHeight!)) : (bodyHeight.text = "")
        (bodyData.bodyWeight != nil) ? (bodyWeight.text = String(bodyData.bodyWeight!)) : (bodyWeight.text = "")
        (bodyData.chest != nil) ? (chest.text = String(bodyData.chest!)) : (chest.text = "")
        (bodyData.stomach != nil) ? (stomach.text = String(bodyData.stomach!)) : (stomach.text = "")
        (bodyData.rightThigh != nil) ? (rightThigh.text = String(bodyData.rightThigh!)) : (rightThigh.text = "")
        (bodyData.leftThigh != nil) ? (leftThigh.text = String(bodyData.leftThigh!)) : (leftThigh.text = "")
        (bodyData.rightBiceps != nil) ? (rightBiceps.text = String(bodyData.rightBiceps!)) : (rightBiceps.text = "")
        (bodyData.leftBiceps != nil) ? (leftBiceps.text = String(bodyData.leftBiceps!)) : (leftBiceps.text = "")
        
        (bodyData.gender != nil) ? (genderLabel.text = bodyData.gender!.rawValue) : (genderLabel.text = "UNKNOWN")
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy, MMM d, HH:mm:ss"
        (bodyData.lastUpdated != nil) ? (lastUpdatedLabel.text = dateFormatter.string(from: bodyData.lastUpdated!)) : (lastUpdatedLabel.text = "-")
    }
    
    @IBAction func saveData(_ sender: UIButton) {
        
        bodyData.bodyHeight = Double(bodyHeight.text ?? "")
        bodyData.bodyWeight = Double(bodyWeight.text ?? "")
        
        bodyData.chest = Double(chest.text ?? "")
        bodyData.stomach = Double(stomach.text ?? "")
        bodyData.rightThigh = Double(rightThigh.text ?? "")
        bodyData.leftThigh = Double(leftThigh.text ?? "")
        bodyData.rightBiceps = Double(rightBiceps.text ?? "")
        bodyData.leftBiceps = Double(leftBiceps.text ?? "")
        
        bodyData.lastUpdated = Date()
        
        UserDefaults.standard.set(bodyData.getAsDictionary(), forKey: "savedData")
        updateUIData()
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "openSettings") {
            (segue.destination as! SettingsViewController).bodyData = self.bodyData
        }
    }
}

