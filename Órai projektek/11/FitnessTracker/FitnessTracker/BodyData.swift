//
//  BodyData.swift
//  FitnessTracker
//
//  Created by Andras Somlai on 2018. 04. 26..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

enum Gender : String {
    case male = "MALE"
    case female = "FEMALE"
}

class BodyData: NSObject {
    var bodyHeight: Double?
    var bodyWeight: Double?
    var chest: Double?
    var stomach: Double?
    var rightThigh: Double?
    var leftThigh: Double?
    var rightBiceps: Double?
    var leftBiceps: Double?
    
    var lastUpdated : Date?
    var gender : Gender?
    
    override init() {
        super.init()
    }
    
    init(dictionary : [String : Any?]) {
        super.init()
        bodyHeight = dictionary["bodyHeight"] as? Double
        bodyWeight = dictionary["bodyWeight"] as? Double
        
        chest = dictionary["chest"] as? Double
        stomach = dictionary["stomach"] as? Double
        rightThigh = dictionary["rightThigh"] as? Double
        leftThigh = dictionary["leftThigh"] as? Double
        rightBiceps = dictionary["rightBiceps"] as? Double
        leftBiceps = dictionary["leftBiceps"] as? Double
        
        lastUpdated = dictionary["lastUpdated"] as? Date
        
        if let genderString = dictionary["gender"] as? String {
            gender = Gender(rawValue: genderString)
        }
    }
    
    func getAsDictionary() -> [String : Any?] {
        var dictionary : [String : Any?] = [:]
        dictionary["bodyHeight"] = bodyHeight
        dictionary["bodyWeight"] = bodyWeight
        
        dictionary["chest"] = chest
        dictionary["stomach"] = stomach
        dictionary["rightThigh"] = rightThigh
        dictionary["leftThigh"] = leftThigh
        dictionary["rightBiceps"] = rightBiceps
        dictionary["leftBiceps"] = leftBiceps
        
        dictionary["lastUpdated"] = lastUpdated
        dictionary["gender"] = gender?.rawValue
        
        return dictionary
    }
    
    func clear() {
        bodyHeight = nil
        bodyWeight = nil
        chest = nil
        stomach = nil
        rightThigh = nil
        leftThigh = nil
        rightBiceps = nil
        leftBiceps = nil
        lastUpdated = nil
        gender = nil
    }
}
