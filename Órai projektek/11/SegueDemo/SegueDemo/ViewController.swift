//
//  ViewController.swift
//  SegueDemo
//
//  Created by Andras Somlai on 2018. 04. 24..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textField: UITextField!
    var data = Data()
    
    @IBOutlet weak var numberDataLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if(self.data.numberData != nil) {
            self.numberDataLabel.text = String(self.data.numberData!)
        }
        else {
            self.numberDataLabel.text = "Nincs numberData"
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
        if(segue.identifier == "showSecondViewController") {
            if let secondVC = segue.destination as? SecondViewController {
                //CRASH, mert ekkor még nem töltöt be a UI
                //secondVC.label.text = textField.text
                
                //Elválasztom a modelt a view-tól, és nem UI-t müdosítok!
                secondVC.textToShow = textField.text
            }
        }
        else if(segue.identifier == "blueSegueId") {
            if let thirdVC = segue.destination as? ThirdViewController {
                //Elválasztom a modelt a view-tól, és nem UI-t müdosítok!
                self.data.stringData = textField.text
                //thirdVC.parentVC = self
                thirdVC.data = self.data
            }
        }
     }
 
}

