//
//  ThirdViewController.swift
//  SegueDemo
//
//  Created by Andras Somlai on 2018. 04. 24..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    var data : Data?
    var parentVC: UIViewController?
    @IBOutlet weak var dataLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(data?.stringData != nil) {
            dataLabel.text = data?.stringData
        }
        
        if let parentVc = navigationController?.viewControllers[0] as? ViewController {
            parentVc.textField.text = "Blue"
        }
        
        if(data != nil) {
            data!.numberData = 12
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
