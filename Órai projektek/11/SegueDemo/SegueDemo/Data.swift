//
//  Data.swift
//  SegueDemo
//
//  Created by Andras Somlai on 2018. 04. 24..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class Data {
    var stringData : String?
    var numberData : Int?
}
