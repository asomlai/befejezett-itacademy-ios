//
//  SecondViewController.swift
//  SegueDemo
//
//  Created by Andras Somlai on 2018. 04. 24..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    var textToShow : String?
    @IBOutlet weak var label : UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(textToShow != nil) {
            label.text = textToShow
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    

}
