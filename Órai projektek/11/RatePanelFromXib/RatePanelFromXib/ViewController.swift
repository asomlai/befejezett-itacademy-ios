//
//  ViewController.swift
//  RatePanelFromXib
//
//  Created by Andras Somlai on 2018. 04. 24..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ratePanelContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        if let ratePanel = Bundle.main.loadNibNamed("RatePanel", owner: nil, options: nil)![0] as? RatePanel {
            ratePanelContainer.addSubview(ratePanel)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillLayoutSubviews() {
        ratePanelContainer.subviews[0].frame = ratePanelContainer.bounds
    }

}

