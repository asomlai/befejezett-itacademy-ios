//
//  RatePanel.swift
//  RatePanelFromXib
//
//  Created by Andras Somlai on 2018. 04. 24..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class RatePanel: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
     
     */
    @IBAction func rated(_ sender: UIButton) {
        switch sender.tag {
        case 1: debugPrint("LIKE")
        case 2: debugPrint("NEUTRAL")
        case 3: debugPrint("DISLIKE")
        default: debugPrint("UNKNOWN TAG")
        }
        
        self.removeFromSuperview()
    }
}
