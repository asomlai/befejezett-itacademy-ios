//
//  ViewController.swift
//  UserDefaultsAndDateFormatter
//
//  Created by Andras Somlai on 2018. 04. 24..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var dateformatTextField: UITextField!
    @IBOutlet weak var dateLabel: UILabel!
    
    /*
     Example date formats
     EEEE, MMM d, yyyy ------ Tuesday, Apr 24, 2018
     MM/dd/yyyy ------ 04/24/2018
     MM-dd-yyyy HH:mm ------ 04-24-2018 12:12
     MMM d, h:mm a ------ Apr 24, 12:12 PM
     MMMM yyyy ------ April 2018
     MMM d, yyyy ------ Apr 24, 2018
     E, d MMM yyyy HH:mm:ss Z ------ Tue, 24 Apr 2018 12:12:01 +0000
     yyyy-MM-dd'T'HH:mm:ssZ ------ 2018-04-24T12:12:01+0000
     dd.MM.yy ------ 24.04.18
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func saveToDefaults(_ sender: UIButton) {
        if(dateformatTextField.text != nil && dateformatTextField.text!.isEmpty == false) {
            UserDefaults.standard.set(Date(), forKey: "SavedDateFormat")
        }
    }
    
    @IBAction func loadFromDefaults(_ sender: UIButton) {
        if let loadedDateFormat = UserDefaults.standard.object(forKey: "SavedDateFormat") as? String {
            dateformatTextField.text = loadedDateFormat
        }
    }
    
    
    @IBAction func updateDateLabel(_ sender: UIButton) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateformatTextField.text
        
        let dateString = dateFormatter.string(from: Date())
        if(dateString.isEmpty == true) {
            dateLabel.text = "\(dateformatTextField.text!) nem értelmezhető"
        }
        else {
            dateLabel.text = dateString
        }
        
//        debugPrint(Date().timeIntervalSince1970)
//        debugPrint(Date.)
    }
}

