//
//  Operation.swift
//  08-CalculatorPlus
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

//A művelettípusokat praktikus enumban tárolni.
//Sokat egyszerűsít a dolgonkun, ha az enumoknak String típusú rawValue-t biztsítunk, mert a kiírásnál nem kell bajlódnunk vele.
enum OperationType : String {
    case addition = "+"
    case substraction = "-"
    case multiplication = "*"
    case division = "/"
    case modulo = "%"
    case unknown = "unknown"
    
    //Enumokhoz hozzá lehet adni computed propertyket
    //https://medium.com/@igroomgrim/swift-trick-how-to-add-a-computed-property-to-enumeration-cc90f8502e40
    //https://stackoverflow.com/questions/24007461/how-to-enumerate-an-enum-with-string-type
    static let validOperations = [OperationType.addition,
                            OperationType.substraction,
                            OperationType.multiplication,
                            OperationType.division,
                            OperationType.modulo]
}

class Operation : NSObject {
    var operand1 : Double   //Egyik operandus
    var operand2 : Double   //Másik operandus
    var operationType : OperationType   //Milyen műveletet végzünk
    var calculator : Calculator //A számológép, ami elvégezte a műveletet
    
    init(operands: (operand1: Double, operand2: Double), operationType : OperationType, calculator : Calculator) {
        self.operand1 = operands.operand1
        self.operand2 = operands.operand2
        self.operationType = operationType
        self.calculator = calculator
    }
    
    //"5 + 4" formában adva vissza az eredményt
    func getAsString() -> String {
        return "\(operand1) \(operationType.rawValue) \(operand2)"
    }
}
