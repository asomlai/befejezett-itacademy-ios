//
//  Product.swift
//  08-ProductStock
//
//  Created by Andras Somlai on 2018. 04. 12..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import Foundation

class StockItem {
    var name : String?
    var price : Double?
    var priceCurrency : Currency?
    var canBuyUnder18 : Bool = true
    var quantity : Int = 0
    
    //Visszaadja az árát a paraméterként kapott valutában
    func getPrice(in currency: Currency) -> Double {
        return CurrencyCalculator.exchange(sourceCurrency: priceCurrency!, destinationCurrency: currency, money: price!)
    }
    
    //Megmondja, hogy az összes property meg van-e adva
    func allDetailsProvided() -> Bool {
        return name != nil && price != nil && priceCurrency != nil && !name!.isEmpty
    }
    
    //Visszaadja stringként az objektumot
    func getAsString() -> String {
        //Hibakezelés, hogy tuti ne szálljon el a program "unexpectedly found nil" hibával
        if(!allDetailsProvided()) {
            return "incomplete item"
        }
        return "\(name!) - quantity:\(quantity) - price:\(String(price!))\(priceCurrency!.rawValue)"
    }
}
