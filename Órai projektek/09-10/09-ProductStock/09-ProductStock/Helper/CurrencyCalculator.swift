//
//  CurrencyCalculator.swift
//  08-ProductStock
//
//  Created by Andras Somlai on 2018. 04. 16..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

//Enum a valutáknak
enum Currency : String {
    case eur = "EUR"
    case huf = "HUF"
    case usd = "USD"
}


class CurrencyCalculator {
    //feladat szöveg specifikációnak megfelelő árfolyamok
    private static let exchangeRates : [String : Double] = ["EUR/HUF" : 300, "EUR/USD" : 1.2, "EUR/EUR" : 1]
    
    //Árfolyam átszámítás
    static func exchange(sourceCurrency: Currency, destinationCurrency: Currency, money: Double) -> Double {
        //Ha közvetlenül meg van adva az árfolyam
        if(exchangeRates[sourceCurrency.rawValue + "/" + destinationCurrency.rawValue] != nil) {
            return exchangeRates[sourceCurrency.rawValue + "/" + destinationCurrency.rawValue]! * money
        }
        //Ha a source és destination currency megegyezik
        if(sourceCurrency == destinationCurrency) {
            return money
        }
        
        //Egyéb esetekben
        let sourceEurEchangeRate = exchangeRates["EUR/" + sourceCurrency.rawValue]
        let destinationEurExchangeRate = exchangeRates["EUR/" + destinationCurrency.rawValue]
        if(sourceEurEchangeRate == nil || destinationEurExchangeRate == nil) {
            return 0
        }
        return (destinationEurExchangeRate!/sourceEurEchangeRate!) * money
    }
}
