//
//  ProductListViewController.swift
//  08-ProductStock
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ProductListViewController: UIViewController {
    @IBOutlet weak var quantitySelectorSlider: UISlider!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var productListTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //ÍGY TUDJÁTOK BEÁLLÍTANI, HOGY A SLIDER.VALUE-NAK MI LEGYEN A MINIMUM ÉS MAXIMUM ÉRTÉKE
        quantitySelectorSlider.minimumValue = 0
        //Maximum értéke a slidernek az itemek számával megegyezik
        quantitySelectorSlider.maximumValue = Float(Stock.sharedInstance.getAllItem().count)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Ez a függvény hívódik meg, amikor mozog a csúszka
    @IBAction func quantitySelected(_ sender: UISlider) {
        
        //Beállítom a label értékét a slider szerint
        quantityLabel.text = String(Int(sender.value+0.5))
        
        //ürítem a textView-t
        productListTextView.text = ""
        
        //For ciklus segítségével kiírom az összes itemet a Stockból.
        //stride-ot kell használnom, hiszen nem feltétlenül megyek végig az összes itemen, hanem a slider szerint írom ki őket
        for i in stride(from: 0, to: Int(sender.value+0.5), by: 1) {
            let item = Stock.sharedInstance.getAllItem()[i]
            productListTextView.text = productListTextView.text + item.getAsString() + "\n"
        }
    }
}
