//
//  ViewController.swift
//  08-ProductStock
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet weak var totalValueLabel: UILabel!
    @IBOutlet weak var numberOfProductsLabel: UILabel!
    @IBOutlet weak var currencySelector: UISegmentedControl!
    
    //Ez a függvény csak akkor hívodik meg, amikor a képernyő betöltődött
    //Nem hívódik meg, amikor a back gomb hatására láthatóvá válik
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    //Ez a függvény mindig meghívódik mielőtt a képernyő látható lesz
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //Frissítem a megjelenített össz árat
        updatePriceLabel()
        //Frissítem a termékek össz mennyiségét
        numberOfProductsLabel.text = String(Stock.sharedInstance.totalNumberOfProducts())
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //Ez a függvény hívódik meg, amikor a valutát választott a felhasználó
    @IBAction func currencySelected(_ sender: UISegmentedControl) {
        updatePriceLabel()
    }
    
    //Frissíti az ár label értékét
    private func updatePriceLabel() {
        //Ha van kiválasztva valuta
        if(currencySelector.selectedSegmentIndex != -1) {
            //Lekérem a kiválasztott valuta nevét
            let selectedCurrencyName = currencySelector.titleForSegment(at: currencySelector.selectedSegmentIndex)!.uppercased()
            //Létrehozok bellőle egy enumot
            let selectedCurrency = Currency(rawValue: selectedCurrencyName)!
            //Lekérdezem a termékek össz értékét a kiválasztott valuta szerint
            let totalPrice = Stock.sharedInstance.totalPriceOfProducts(currency: selectedCurrency)
            //Megjelenítem a kijelzőn a termékek össz árát
            totalValueLabel.text = String(totalPrice)
        }
    }
}

