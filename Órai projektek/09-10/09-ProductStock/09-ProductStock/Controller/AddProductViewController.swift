//
//  AddProductViewController.swift
//  08-ProductStock
//
//  Created by Andras Somlai on 2018. 04. 11..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class AddProductViewController: UIViewController {
    
    @IBOutlet weak var productNameInputField: UITextField!  //termék neve text input property
    @IBOutlet weak var productPriceTextField: UITextField!  //termék ára text input property
    @IBOutlet weak var currencySelector: UISegmentedControl!//valut kiválasztó property
    @IBOutlet weak var quantiyFeedbackLabel: UILabel!       //Mennyiség label property
    @IBOutlet weak var quantityStepper: UIStepper!          //A +/- kontroll propertyje
    @IBOutlet weak var canBuySwitchOutlet: UISwitch!        //18 alatt meg lehet-e venni a terméket switch property
    
    var itemToAdd = StockItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Beállítom a UI-t alaphelyzetbe
        resetUI()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //Visszaállítja a felhasználói felületet az eredeti állapotába
    private func resetUI() {
        canBuySwitchOutlet.isOn = itemToAdd.canBuyUnder18
        quantityStepper.value = Double(itemToAdd.quantity)
        quantiyFeedbackLabel.text = String(itemToAdd.quantity)
        productNameInputField.text = ""
        productPriceTextField.text = ""
        currencySelector.selectedSegmentIndex = -1
        view.endEditing(true)
    }
    
    //Ez a függvény hívódik meg, ha valutát választ a user
    @IBAction func currencySelected(_ sender: UISegmentedControl) {
        //Lekérdezem, hogy melyik valutát választotta ki a felhasználó
        //Uppercaselem a biztonság kedvéért, mert az enum rawValue értékei mind nagybetűsök
        let selectedCurrencyName = currencySelector.titleForSegment(at: currencySelector.selectedSegmentIndex)!.uppercased()
        
        //Beállítom a currency enumot, amit a string segítségével hoztam létre.
        itemToAdd.priceCurrency = Currency(rawValue: selectedCurrencyName)
    }
    
    //Ez a függvény hívódik meg, amikor a +/- konrollt használja a user
    @IBAction func quantityChanged(_ sender: UIStepper) {
        //updaetelem az objektumot
        itemToAdd.quantity = Int(sender.value)
        //updatelem a felhasználói felületet az objektumnak megfelelően
        quantiyFeedbackLabel.text = String(itemToAdd.quantity)
    }
    
    //Ez a függvény hívódik meg, ha a switc-et megnyomta a user
    @IBAction func canBuyProductSwitchValueChanged(_ sender: UISwitch) {
        //updatelem az objektumot.
        //a felasználói felületen nem kell módosítanom, hiszen a user már megtette
        itemToAdd.canBuyUnder18 = sender.isOn
    }
    
    //Ez a függvény hívódikmeg, ha a Termék hozzáadása gombra nyom a user
    @IBAction func addProductButtonPressed(_ sender: UIButton) {
        //Termék nevének beállítása
        itemToAdd.name = productNameInputField.text
        //Termék árának beállítása
        itemToAdd.price = Double(productPriceTextField.text!.replacingOccurrences(of: ",", with: "."))
        
        //ennek a változónak az értékét fogom megjeleníteni a felhasználói felületen
        var alertTitle = ""
        
        //Ha nincs megadva minden az itemhez
        if(!itemToAdd.allDetailsProvided()) {
            alertTitle = "Can't add product. Please provide all details"
        }
        //Ha meg van adva minden az itemhez
        else {
            alertTitle = "Product added"
            //Hozzáadom a stockhoz
            Stock.sharedInstance.add(item: itemToAdd)
            
            //Létrehozok egy új, üres itemet
            itemToAdd = StockItem()
            
            //Visszaállítom alaphelyzetbe a felhasználói felületet
            resetUI()
        }
        
        //feldobom az alertet
        let alert = UIAlertController(title: alertTitle, message:nil , preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }

}
