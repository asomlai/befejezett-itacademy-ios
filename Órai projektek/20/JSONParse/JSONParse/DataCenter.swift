//
//  DataCenter.swift
//  JSONParse
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class DataCenter: NSObject {

    static var sharedInstance : DataCenter = {
        let instance = DataCenter()
        return instance
    }()
    
    override private init() {
        super.init()
    }
    
    func getHumans() {
        NetworkManager.getPeople { (jsonData, error) in
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            do {
                let humans = try decoder.decode([Human].self, from: jsonData!)
                print(humans)
            } catch {
                print(error.localizedDescription)
            }
        }
        
    }
}
