//
//  People.swift
//  JSONParse
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

/*
 JSON object:
 {
     "id": 1,
     "first_name": "Jeanette",
     "last_name": "Penddreth",
     "email": "jpenddreth0@census.gov",
     "gender": "Female",
     "ip_address": "26.58.193.2"
 }
 */

struct Human: Codable {
    private(set) var id : Int
    private(set) var firstName : String
    private(set) var lastName : String
    private(set) var email : String?
    private(set) var gender : String?
    private(set) var ipAddress : String?
}
