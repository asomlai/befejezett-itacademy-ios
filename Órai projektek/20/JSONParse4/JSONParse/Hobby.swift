//
//  Hobby.swift
//  JSONParse
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

struct Hobby: Codable {
    private(set) var id : Int
    private(set) var name : String
    
    func getAsString() -> String {
        return "\(id) - \(name)"
    }
}
