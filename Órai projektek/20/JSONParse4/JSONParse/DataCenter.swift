//
//  DataCenter.swift
//  JSONParse
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class DataCenter: NSObject {
    var humans : [Human]!

    static var sharedInstance : DataCenter = {
        let instance = DataCenter()
        return instance
    }()
    override private init() {
        super.init()
    }
    
    func getHumans(completion: @escaping (Bool) -> Void) {
        NetworkManager.getPeople { (jsonData, error) in
            let decoder = JSONDecoder()
            do {
                self.humans = try decoder.decode([Human].self, from: jsonData!)
                for human in self.humans {
                    human.prettyPrint()
                }
                completion(true)
            } catch {
                print(error.localizedDescription)
                completion(false)
            }
        }
    }
    
    func getHumansAsJSON() -> String? {
        let encoder = JSONEncoder()
        do {
            let jsonData = try encoder.encode(humans)
            let jsonString = String(data: jsonData, encoding: .utf8)
            return jsonString
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
