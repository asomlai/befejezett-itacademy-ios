//
//  People.swift
//  JSONParse
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

enum Gender : String, Codable {
    case male = "Male"
    case female =  "Female"
}

struct Human: Codable {
    private(set) var id : Int
    private(set) var firstName : String
    private(set) var lastName : String
    private(set) var email : String?
    private(set) var gender : Gender?
    private(set) var ipAddress : String?
    private(set) var hobbies : [Hobby]?
    
    private enum CodingKeys : String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case gender
        case ipAddress = "ip_address"
        case hobbies
    }
    
    
    func prettyPrint() {
        var hobbiesString = "no hobby"
        if(hobbies != nil) {
            hobbiesString = ""
            for hobby in hobbies!.map({$0.getAsString()}) {
                hobbiesString += "\(hobby), "
            }
        }
        
        print(
        """
            ---------------
            \(id)
            \(firstName)
            \(lastName)
            \(email ?? "no email")
            \(gender?.rawValue ?? "no gender")
            \(ipAddress ?? "no ip")
            hobbies: \(hobbiesString)
        """
        )
    }
}
