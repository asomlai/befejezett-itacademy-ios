//
//  Friend.swift
//  TryCatch
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import UIKit

enum ObjectIdentifierLogicalError : Error {
    case negativeValueError
    case zeroValueError
    case maximumValueLimitError(reason: String)
}

enum ObjectIdentifierCastError : Error, LocalizedError {
    case integerCastError
    
    public var errorDescription: String? {
        return NSLocalizedString("Az azonosító nem integer típusú", comment: "User-friendly error message")
    }
}

class MyObject {
    var identifier : Int!
    
    init(param : Any) throws {
        guard let integer = param as? Int else {
            throw ObjectIdentifierCastError.integerCastError
        }
        
        if(integer < 0) {
            throw ObjectIdentifierLogicalError.negativeValueError
        }
        
        if(integer == 0) {
            throw ObjectIdentifierLogicalError.zeroValueError
        }
        
        if(integer > 100000) {
            throw ObjectIdentifierLogicalError.maximumValueLimitError(reason: "limit is 100000")
        }
        
        identifier = integer
    }
}
