//
//  CustomView.swift
//  CustomViewInStoryboard
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

class CustomView: UIView {
    let lineView = UIView()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        backgroundColor = UIColor.yellow
        addSubview(lineView)
        lineView.backgroundColor = UIColor.red
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        lineView.frame = CGRect(x:frame.width/2 - 1, y:0, width:2, height: frame.height)
    }

}
