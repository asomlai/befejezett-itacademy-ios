//
//  People.swift
//  JSONParse
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

enum Gender : String {
    case male = "Male"
    case female =  "Female"
}

struct Human: Codable {
    private(set) var id : Int
    private(set) var firstName : String
    private(set) var lastName : String
    private(set) var email : String?
    private(set) var gender : Gender?
    private(set) var ipAddress : String?
    
    private enum CodingKeys : String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case gender
        case ipAddress = "ip_address"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(Int.self, forKey: .id)
        
        firstName = try values.decode(String.self, forKey: .firstName)
        
        lastName = try values.decode(String.self, forKey: .lastName)
        
        email = try values.decode(String.self, forKey: .email)
        
        ipAddress = try values.decode(String.self, forKey: .ipAddress)
        
        let genderString = try values.decode(String.self, forKey: .gender)
        gender = Gender(rawValue: genderString)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(id, forKey: .id)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(email, forKey: .email)
        try container.encode(gender?.rawValue, forKey: .gender)
        try container.encode(ipAddress, forKey: .ipAddress)
    }
}
