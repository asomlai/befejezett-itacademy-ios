//
//  Hobby.swift
//  JSONParse
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

struct Hobby: Codable {
    private(set) var id : Int
    private(set) var hobbyName : String
    
    
    private enum CodingKeys : String, CodingKey {
        case id
        case hobbyName = "name"
    }
    /*
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
    }
    */
    
    func getAsString() -> String {
        return "\(id) - \(hobbyName)"
    }
}
