
//
//  NetworkManager.swift
//  JSONParse
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit
import Alamofire

class NetworkManager: NSObject {
    
    static func getPeople(completion : @escaping (Data?, Error?) -> Void) {
        Alamofire.request("http://138.197.187.213/itacademy/peopleWithHobbies").responseData { (response) in
                let statusCode = response.response?.statusCode
                if(statusCode == 200) {
                    completion(response.result.value, nil)
                }
                else {
                    completion(nil, response.error)
                }
        }
    }
    
}
