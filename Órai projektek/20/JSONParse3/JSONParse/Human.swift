//
//  People.swift
//  JSONParse
//
//  Created by Andras Somlai on 2018. 05. 29..
//  Copyright © 2018. ITAcademy. All rights reserved.
//

import UIKit

enum Gender : String, Codable {
    case male = "Male"
    case female =  "Female"
}

struct Human: Codable {
    private(set) var id : Int
    private(set) var firstName : String
    private(set) var lastName : String
    private(set) var email : String?
    private(set) var gender : Gender?
    private(set) var ipAddress : String?
    private(set) var hobbies : [Hobby]?
    
    private enum CodingKeys : String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case email
        case gender
        case ipAddress = "ip_address"
        case hobbies
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        firstName = try values.decode(String.self, forKey: .firstName)
        lastName = try values.decode(String.self, forKey: .lastName)
        email = try values.decode(String.self, forKey: .email)
        //let genderString = try values.decode(String.self, forKey: .gender)
        gender = try values.decode(Gender.self, forKey: .gender)//Gender(rawValue: genderString)
        ipAddress = try values.decode(String.self, forKey: .ipAddress)
        hobbies = try values.decodeIfPresent([Hobby].self, forKey: .hobbies)
        
        /*if let decodedHobbies = try values.decodeIfPresent([Hobby].self, forKey: .hobbies) {
            hobbies = decodedHobbies
        }*/
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(email, forKey: .email)
        try container.encode(gender, forKey: .gender)
        try container.encode(ipAddress, forKey: .ipAddress)
        try container.encodeIfPresent(hobbies, forKey: .hobbies)
    }
    
    func prettyPrint() {
        var hobbiesString = "no hobby"
        if(hobbies != nil) {
            hobbiesString = ""
            for hobby in hobbies!.map({$0.getAsString()}) {
                hobbiesString += "\(hobby), "
            }
        }
        
        print(
        """
            ---------------
            \(id)
            \(firstName)
            \(lastName)
            \(email ?? "no email")
            \(gender?.rawValue ?? "no gender")
            \(ipAddress ?? "no ip")
            hobbies: \(hobbiesString)
        """
        )
    }
}
