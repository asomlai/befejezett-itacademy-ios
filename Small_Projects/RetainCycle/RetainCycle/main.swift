//
//  main.swift
//  RetainCycle
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import Foundation

//var person : Person? = Person(name: "Henrik")
//
//var computer : Computer? = Computer(memory: 8)
//
//person = nil
//computer = nil
//
//person = Person(name: "Gyula")
//computer = Computer(memory: 16)
//
//person!.computer = computer
//computer!.owner = person
//
//person = nil
//computer = nil
//
//let annaComp = Computer(memory: 4)
//var anna = Person(name:"Anna")
//anna.computer = annaComp
//
//let person2 = Person(name: "Jeno")
//debugPrint("person2:", person2)
//

weak var person3 : Person?// = Person(name: "Bea")
debugPrint("person3:", person3)

func testPeople() {
    let petra = Person(name: "Petra")
    debugPrint(petra.name)
}

testPeople()
