//
//  Person.swift
//  RetainCycle
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import Cocoa

class Person {
    weak var computer : Computer?
    var name : String
    
    init(name : String) {
        self.name = name
    }
    
    deinit {
        debugPrint("deinit called on Person class")
    }
}
