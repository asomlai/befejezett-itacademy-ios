//
//  Computer.swift
//  RetainCycle
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import Cocoa

class Computer {
    weak var owner : Person?
    var memory : Int
    
    init(memory : Int) {
        self.memory = memory
    }
    
    deinit {
        debugPrint("deinit called on Computer class")
    }
}
