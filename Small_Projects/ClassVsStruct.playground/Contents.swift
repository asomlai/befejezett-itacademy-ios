//: Playground - noun: a place where people can play

import UIKit

var str = "Hello, playground"

let data : [[String : Any]] = [["YE" : "YO", "YO" : "YE"]]

if let dict = data[0]["YEE"] as? String {
    debugPrint("OK")
}

struct PointsStruct {
    var x : Double = 0
    var y : Double = 0
    
    func printData() {
        debugPrint("Struct--x:\(x),y:\(y)")
    }
}

class PointsClass {
    var x : Double = 0
    var y : Double = 0
    
    func printData() {
        debugPrint("Class--x:\(x),y:\(y)")
    }
}

class Container {
    var pointsStruct : PointsStruct
    var pointsClass : PointsClass
    init(pointsClass : PointsClass, pointsStruct : PointsStruct) {
        self.pointsClass = pointsClass
        self.pointsStruct = pointsStruct
    }
    
    func printData() {
        pointsStruct.printData()
        pointsClass.printData()
    }
}

class Test {
    static func chanceCoordinates(container : Container) {
        let newX : Double = 10
        let newY : Double = 5
        
        var pointsStruct = container.pointsStruct
        let pointsClass = container.pointsClass
        
        pointsStruct.x = newX //Ha pointsStruct nem var, hanem let --> ERROR
        pointsStruct.y = newY //Ha pointsStruct nem var, hanem let --> ERROR
        
        pointsClass.x = newX
        pointsClass.y = newY
        
        container.printData()
        
    }
}

let container = Container(pointsClass: PointsClass(), pointsStruct: PointsStruct())
Test.chanceCoordinates(container: container)
