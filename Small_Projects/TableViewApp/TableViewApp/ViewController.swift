//
//  ViewController.swift
//  TableViewApp
//
//  Created by Andras Somlai on 2018. 04. 04..
//  Copyright © 2018. Astron Informatikai Kft. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let tableView = UITableView()
    var data : [String] = ["yo", "Yo2"]
    @IBOutlet var reloadFromDefaultsButton : UIButton?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        tableView.frame = CGRect(x:0, y:reloadFromDefaultsButton!.frame.maxY+20, width: view.frame.width, height: view.frame.height - 20 - reloadFromDefaultsButton!.frame.maxY)
    }

    @IBAction func reloadFromMemory(_ sender: UIButton) {
        data = generateContentToMemory()
        tableView.reloadData()
    }
    
    @IBAction func reloadFromDefaults(_ sender: UIButton) {
        generateContentToDefaults()
        data = UserDefaults.standard.object(forKey: "data") as! [String]
        tableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var tableViewCell = tableView.dequeueReusableCell(withIdentifier: "cell")
        if(tableViewCell == nil) {
            debugPrint("New tableViewCell")
            tableViewCell = UITableViewCell(style: .default, reuseIdentifier:"cell")
            
        }
        else {
            debugPrint("Reused tableViewCell")
        }
        
        tableViewCell?.textLabel?.text = data[indexPath.row]
        return tableViewCell!
    }

    func generateContentToDefaults() {
        var dataToDefaults : [String] = []
        for i in stride(from: 0, through: 100000, by: 1) {
            dataToDefaults.append("ReloadedFromDefaults: \(i)")
        }
        UserDefaults.standard.set(dataToDefaults, forKey: "data")
    }
    
    func generateContentToMemory() -> [String] {
        var dataToDefaults : [String] = []
        for i in stride(from: 0, through: 100000, by: 1) {
            dataToDefaults.append("ReloadedFromMemory: \(i)")
        }
        return dataToDefaults
    }
    
}

