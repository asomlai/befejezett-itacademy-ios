//
//  ViewController.swift
//  Navigation
//
//  Created by Andras Somlai on 2018. 04. 17..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        debugPrint("prepare: \(segue.identifier ?? "nincs")")
//        debugPrint(segue.destination is UIViewController)
    }
}

