//
//  PurpleViewController.swift
//  Navigation
//
//  Created by Andras Somlai on 2018. 04. 19..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class PurpleViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Vissza 1 szintet
    @IBAction func simpleBack(_ sender: UIButton) {
        self.navigationController!.popViewController(animated: true)
    }
    
    //Vissza a rootba
    @IBAction func backToRoot(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
