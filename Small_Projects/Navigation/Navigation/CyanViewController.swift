//
//  CyanViewController.swift
//  Navigation
//
//  Created by Andras Somlai on 2018. 04. 17..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class CyanViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func back(sender : UIButton) {
        dismiss(animated: true) {
            //nothing here
        }
    }

}
