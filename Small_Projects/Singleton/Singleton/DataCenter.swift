//
//  DataCenter.swift
//  Singleton
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import Cocoa

class DataCenter {
    //properties
    var peopleNames : [String] = ["Joe", "Bob", "Amy"]
    var workToDo : String?
    var timeLeft : Double?
    
    static let sharedInstance: DataCenter = {
        let instance = DataCenter()
        return instance
    }()
    
    //Ezzel a sorral biztosítom, hogy nem lehet példányosítani kívülről
    private init() {
        debugPrint("Datacenter init called")
    }
    
    func printData() {
        debugPrint("names: \(peopleNames), work:\(workToDo ?? "none"), timeLeft:\(String(timeLeft ?? 0))")
    }
}
