//
//  main.swift
//  Singleton
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import Foundation

//Így is elérem a sinletont
let dataCenter = DataCenter.sharedInstance
dataCenter.workToDo = "cook dinner"
dataCenter.timeLeft = 3600
dataCenter.printData()

//És így is
DataCenter.sharedInstance.workToDo = "cook dinner1"
DataCenter.sharedInstance.timeLeft = 1800
DataCenter.sharedInstance.printData()

//Osztályon belül is akárhol
class Random {
    var text : String? {
        didSet {
            DataCenter.sharedInstance.printData()
        }
    }
}

let random = Random()
random.text = "Hey"
dataCenter.workToDo = "cook dinner3"
dataCenter.timeLeft = 4800
random.text = "hi"

//DataCenter.sharedInstance = nil --> ERROR
