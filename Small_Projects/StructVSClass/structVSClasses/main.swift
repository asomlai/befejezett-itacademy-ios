//
//  main.swift
//  structVSClasses
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. Andras Somlai. All rights reserved.
//

import Foundation

//: Playground - noun: a place where people can play


var str = "Hello, playground"

let data : [[String : Any]] = [["YE" : "YO", "YO" : "YE"]]

if let dict = data[0]["YEE"] as? String {
    debugPrint("OK")
}

struct PointsStruct {
    var x : Double
    var y : Double
    
    func printData() {
        debugPrint("Struct--x:\(x),y:\(y)")
    }
}

class PointsClass {
    var x : Double = 0
    var y : Double = 0
    
    func printData() {
        debugPrint("Class--x:\(x),y:\(y)")
    }
}

class Container {
    var pointsStruct : PointsStruct
    var pointsClass : PointsClass
    init(pointsClass : PointsClass, pointsStruct : PointsStruct) {
        self.pointsClass = pointsClass
        self.pointsStruct = pointsStruct
    }
    
    func printData() {
        pointsStruct.printData()
        pointsClass.printData()
    }
}

final class Test {
    static func chanceCoordinates(container : Container) {
        let newX : Double = 10
        let newY : Double = 5
        
        var pointsStruct = container.pointsStruct
        let pointsClass = container.pointsClass
        
        pointsStruct.x = newX //Ha pointsStruct nem var, hanem let --> ERROR
        pointsStruct.y = newY //Ha pointsStruct nem var, hanem let --> ERROR
        
        pointsClass.x = newX
        pointsClass.y = newY
        
        container.printData()
        
    }
}

class KaveGep {

    static func daral(classKave : ClassKave) {
        let classKaveCopy = classKave.getCopy()
        ClassKave.daraltStatic = true
        classKaveCopy.daralt = true
    }
    
    static func daral(structKave : StructKave) {
        var structKaveCopy = structKave
        structKaveCopy.daralt = true
    }
}

struct StructKave {
    var daralt = false
}

class ClassKave {
    var daralt = false
    static var daraltStatic = false
    
    func getCopy() -> ClassKave {
        let classKaveCopy = ClassKave()
        classKaveCopy.daralt = self.daralt
        return classKaveCopy
    }
}

let structure = PointsStruct(x : 0, y : 0)
let container = Container(pointsClass: PointsClass(), pointsStruct: structure)
Test.chanceCoordinates(container: container)

var classKave = ClassKave()
var structKave = StructKave()

debugPrint("\(classKave.daralt)")
debugPrint("\(structKave.daralt)")

KaveGep.daral(classKave: classKave)
KaveGep.daral(structKave: structKave)

debugPrint("\(classKave.daralt)")
debugPrint("\(structKave.daralt)")

