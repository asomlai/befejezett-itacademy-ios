//
//  DataModel.swift
//  ModelViewController
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class DataModel {
    var enteredText : String? {
        didSet {
            NotificationCenter.default.post(name: Notification.Name("modelUpdated"), object: nil)
        }
    }
}
