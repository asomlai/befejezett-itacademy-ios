//
//  CustomView.swift
//  Storyboard
//
//  Created by Andras Somlai on 2018. 04. 17..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class CustomView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    @IBAction func buttonTocuhed(_ sender: UIButton) {
        if(sender.tag == 1) {
            debugPrint("LIKE")
        }
        else if(sender.tag == 2) {
            debugPrint("DISLIKE")
        }
        else {
            debugPrint("UNKNOWN")
        }
    }
    
    static func fromNib() -> CustomView {
        return Bundle.main.loadNibNamed("CustomView", owner: nil, options: nil)![0] as! CustomView
    }
}
