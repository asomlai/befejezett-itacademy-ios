//
//  ViewController.swift
//  Storyboard
//
//  Created by Andras Somlai on 2018. 04. 15..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var rateViewContainer: UIView!
    let rateView = CustomView.fromNib()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        rateViewContainer.addSubview(rateView)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        rateView.frame = rateViewContainer.bounds
    }

}

