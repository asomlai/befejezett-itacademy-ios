//
//  LabelView.swift
//  ModelViewPresenter
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class LabelView: UILabel {
    override var text: String? {
        didSet {
            if(text == nil || text!.isEmpty) {
                text = "No entered Text"
            }
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
