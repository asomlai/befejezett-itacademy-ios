//
//  ViewController.swift
//  ModelViewPresenter
//
//  Created by Andras Somlai on 2018. 04. 10..
//  Copyright © 2018. IT Academy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var textInputField: UITextField!
    @IBOutlet weak var feedbackLabel: LabelView!
    let dataModel = DataModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateUI() {
        view.endEditing(true) //billzet elrejtése
        textInputField.text = nil
        feedbackLabel.text = dataModel.enteredText
    }

    //This function is called when done button pressed
    @IBAction func doneButtonPressed(_ sender: Any) {
        dataModel.enteredText = textInputField.text
        updateUI()
    }
}

